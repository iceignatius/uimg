#include <string.h>
#include <gif_lib.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimggif_dec.h"

struct read_status
{
    const uint8_t *raw;
    size_t raw_size;
    size_t read_pos;
    size_t max_pos;
};

struct uimggif_dec
{
    GifFileType *gif;
    int default_duration;
    int default_transparent_index;
};

uimggif_dec_t* uimggif_dec_open_file(const char *filename)
{
    /**
     * Create a decode handler by parse a file
     *
     * @param filename Name of the file to be parsed
     * @return  The created decode handler on success;
     *          otherwise, NULL will be returned.
     */
    if(!filename)
        return NULL;

    size_t size;
    void *buf = filebuf_load(filename, &size);
    if(!buf)
        return NULL;

    uimggif_dec_t *dec = uimggif_dec_open_mem(buf, size, NULL);
    free(buf);

    return dec;
}

static
int gif_read_raw(GifFileType *gif, GifByteType *buf, int size)
{
    struct read_status *file = gif->UserData;

    if( file->read_pos + size > file->raw_size )
        return -1;

    memcpy(buf, file->raw + file->read_pos, size);
    file->read_pos += size;

    if( file->max_pos < file->read_pos )
        file->max_pos = file->read_pos;

    return size;
}

static
bool get_gcb_info(
    const ExtensionBlock *blklist, int blknum,
    int *duration, int *transparent_index)
{
    for(int i = 0; i < blknum; ++i)
    {
        const ExtensionBlock *blk = &blklist[i];

        GraphicsControlBlock gcb;
        if( blk->Function == GRAPHICS_EXT_FUNC_CODE &&
            GIF_OK == DGifExtensionToGCB(blk->ByteCount, blk->Bytes, &gcb) )
        {
            *duration = gcb.DelayTime * 10;
            *transparent_index = gcb.TransparentColor;
            return true;
        }
    }

    return false;
}

uimggif_dec_t* uimggif_dec_open_mem(
    const void *raw, size_t raw_size, size_t *read_size)
{
    /**
     * Create a decode handler by parse data from memory
     *
     * @param raw       The raw GIF format data to be parsed
     * @param raw_size  Size of the input data
     * @param read_size Return the size of data be read from the input data.
     *                  This parameter can be NULL if not needed.
     * @return  The created decode handler on success;
     *          otherwise, NULL will be returned.
     */
    uimggif_dec_t *dec = NULL;

    bool succ = false;
    do
    {
        if( !raw || !raw_size )
            break;

        if( !( dec = calloc(1, sizeof(*dec)) ) )
            break;

        struct read_status file =
        {
            .raw = raw,
            .raw_size = raw_size,
        };

        int err;
        if( !( dec->gif = DGifOpen(&file, gif_read_raw, &err) ) )
            break;

        if( GIF_OK != DGifSlurp(dec->gif) )
            break;

        if(!get_gcb_info(
            dec->gif->ExtensionBlocks,
            dec->gif->ExtensionBlockCount,
            &dec->default_duration,
            &dec->default_transparent_index))
        {
            dec->default_duration = 10;
            dec->default_transparent_index = -1;
        }

        if(read_size)
            *read_size = file.max_pos;

        succ = true;
    } while(false);

    if( !succ && dec )
    {
        uimggif_dec_close(dec);
        dec = NULL;
    }

    return dec;
}

void uimggif_dec_close(uimggif_dec_t *dec)
{
    /**
     * End use of the decode handler and release its resources
     *
     * @param dec The decode handler
     */
    if(dec)
    {
        if(dec->gif)
            DGifCloseFile(dec->gif, NULL);

        free(dec);
    }
}

int uimggif_dec_get_width(const uimggif_dec_t *dec)
{
    /**
     * Get image width
     *
     * @param dec The decode handler
     * @return The image width if available; otherwise, -1 will be returned.
     */
    return dec ? dec->gif->SWidth : -1;
}

int uimggif_dec_get_height(const uimggif_dec_t *dec)
{
    /**
     * Get image height
     *
     * @param dec The decode handler
     * @return The image height if available; otherwise, -1 will be returned.
     */
    return dec ? dec->gif->SHeight : -1;
}

int uimggif_dec_count_colour(const uimggif_dec_t *dec)
{
    /**
     * Count number of colours in the colour table
     *
     * @param dec The decode handler
     * @return  Number of colours in the colour table if available;
     *          otherwise, -1 will be returned.
     */
    return dec ? 1 << dec->gif->SColorResolution : -1;
}

int uimggif_dec_count_image(const uimggif_dec_t *dec)
{
    /**
     * Count images
     *
     * @param dec The decode handler
     * @return  Number of images in this file if available;
     *          otherwise, -1 will be returned.
     */
    return dec ? dec->gif->ImageCount : -1;
}

static
uint32_t colour_from_map(ColorMapObject *map, int index)
{
    if( index < 0 || map->ColorCount <= index )
        return UIMG_COLOUR_BLANK;

    GifColorType c = map->Colors[index];
    return UIMG_MAKE_COLOUR(c.Blue, c.Green, c.Red, 255);
}

int uimggif_dec_get_image(
    uimggif_dec_t *dec, int idx, uimg_t *img, int *dura)
{
    /**
     * Read image
     *
     * @param dec   The decode handler
     * @param idx   Index of the image to get
     * @param img   Return the read out image
     * @param dura  Return duration of the image in milliseconds.
     *              This parameter can be NULL if not needed.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !dec || idx < 0 || idx >= dec->gif->ImageCount || !img )
        return -1;

    GifFileType *gif = dec->gif;
    SavedImage *img_data = &gif->SavedImages[idx];
    GifImageDesc *img_desc = &img_data->ImageDesc;
    ColorMapObject *colour_map =
        img_desc->ColorMap ? img_desc->ColorMap : gif->SColorMap;

    int duration, transparent_index;
    if(!get_gcb_info(
        img_data->ExtensionBlocks, img_data->ExtensionBlockCount,
        &duration, &transparent_index))
    {
        duration = dec->default_duration;
        transparent_index = dec->default_transparent_index;
    }

    if(dura)
        *dura = duration;

    if(uimg_resize(img, gif->SWidth, gif->SHeight))
        return -1;

    if( img_desc->Left != 0 ||
        img_desc->Top != 0 ||
        img_desc->Width != gif->SWidth ||
        img_desc->Height != gif->SHeight )
    {
        uimg_fill(img, colour_from_map(colour_map, gif->SBackGroundColor));
    }

    if( img_desc->Left < 0 ||
        img_desc->Top < 0 ||
        img_desc->Width <= 0 ||
        img_desc->Height <= 0 )
    {
        return 0;
    }

    uimg_begin_buf_access(img);
    for(int src_y = 0; src_y < img_desc->Height; ++src_y)
    {
        GifByteType *src_row = &img_data->RasterBits[ img_desc->Width * src_y ];
        uint32_t *dst_row = uimg_get_row_buf(img, img_desc->Top + src_y);

        for(int src_x = 0; src_x < img_desc->Width; ++src_x)
        {
            int src_idx = src_row[src_x];
            dst_row[ img_desc->Left + src_x ] = src_idx == transparent_index ?
                UIMG_COLOUR_BLANK : colour_from_map(colour_map, src_idx);
        }
    }
    uimg_end_buf_access(img);

    return 0;
}

int uimggif_hlpr_load_file(uimg_t *img, const char *filename)
{
    /**
     * @brief Load image from file
     *
     * This is a helper function to simplify the use case of
     * just need to read only one image from a file.
     *
     * @param img       The image object to be loaded
     * @param filename  Name of the file to be parsed
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename )
        return -1;

    uimggif_dec_t *dec = NULL;

    int err = -1;
    do
    {
        if( !( dec = uimggif_dec_open_file(filename) ) )
            break;

        if(uimggif_dec_get_image(dec, 0, img, NULL))
            break;

        err = 0;
    } while(false);

    if(dec)
        uimggif_dec_close(dec);

    return err;
}
