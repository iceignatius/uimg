#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "uimg_config.h"
#include "uimgcolour.h"
#include "uimgdrv.h"

unsigned uimgdrv_get_magic(void)
{
    return UIMG_DRVID_SOFT;
}

struct uimg* uimgdrv_alloc_imgslot(void)
{
    return malloc(sizeof(struct uimg));
}

void uimgdrv_free_imgslot(struct uimg *slot)
{
    free(slot);
}

void* uimgdrv_alloc_imgbuf(unsigned w, unsigned h)
{
    size_t size = w * h * sizeof(uint32_t);
    return malloc(size);
}

void* uimgdrv_clone_imgbuf(void *drvbuf, unsigned w, unsigned h)
{
    size_t size = w * h * sizeof(uint32_t);

    uint8_t *dst = malloc(size);
    if(dst)
        memcpy(dst, drvbuf, size);

    return dst;
}

void uimgdrv_free_imgbuf(void *drvbuf)
{
    free(drvbuf);
}

int uimgdrv_begin_buf_access(void *drvbuf)
{
    return 0;
}

int uimgdrv_end_buf_access(void *drvbuf)
{
    return 0;
}

uint32_t* uimgdrv_get_row_buf(void *drvbuf, unsigned w, unsigned h, unsigned y)
{
    return (uint32_t*) drvbuf + y * w;
}

static
void fill_rect(
    uint32_t *buf, unsigned w, unsigned h, uint32_t val, const struct uimg_rect *rect)
{
    uint32_t *row = buf + w * rect->y;
    for(unsigned j = 0; j < rect->h; ++j)
    {
        for(unsigned i = 0; i < rect->w; ++i)
            row[ rect->x + i ] = val;
        row += w;
    }
}

static
void fill_whole(uint32_t *buf, unsigned w, unsigned h, uint32_t val)
{
    for(size_t n = w * h; n--;)
        *buf++ = val;
}

int uimgdrv_fill(
    void *drvbuf, unsigned w, unsigned h, uint32_t val, const struct uimg_rect *rect)
{
    if(rect)
        fill_rect(drvbuf, w, h, val, rect);
    else
        fill_whole(drvbuf, w, h, val);

    return 0;
}

static
void flip_h(uint32_t *dst, uint32_t *src, unsigned w, unsigned h)
{
    for(unsigned y = 0; y < h; ++y, src += w, dst += w)
    {
        for(unsigned x = 0; x < w; ++x)
            dst[x] = src[ w - x - 1 ];
    }
}

static
void flip_v(uint32_t *dst, uint32_t *src, unsigned w, unsigned h)
{
    src += w * ( h - 1 );
    for(unsigned y = 0; y < h; ++y, dst += w, src -= w)
        memcpy(dst, src, w * sizeof(src[0]));
}

int uimgdrv_flip(void *dst_buf, void *src_buf, unsigned w, unsigned h, int dir)
{
    int err = 0;
    switch(dir)
    {
    case UIMG_FLIP_H:
        flip_h(dst_buf, src_buf, w, h);
        break;

    case UIMG_FLIP_V:
        flip_v(dst_buf, src_buf, w, h);
        break;

    default:
        err = -1;
        break;
    }

    return err;
}

static
void rotate_180(uint32_t *dst, uint32_t *src, unsigned src_w, unsigned src_h)
{
    unsigned n = src_w * src_h;

    src += n -1 ;
    while(n--)
        *dst++ = *src--;
}

static
void rotate_90(uint32_t *dst, uint32_t *src, unsigned src_w, unsigned src_h)
{
    unsigned dst_w = src_h;
    unsigned dst_h = src_w;
    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        unsigned src_row_offset = ( dst_w - 1 ) * src_w;
        unsigned src_x = dst_y;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            *dst++ = src[ src_row_offset + src_x ];
            src_row_offset -= src_w;
        }
    }
}

static
void rotate_270(uint32_t *dst, uint32_t *src, unsigned src_w, unsigned src_h)
{
    unsigned dst_w = src_h;
    unsigned dst_h = src_w;
    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        unsigned src_row_offset = 0;
        unsigned src_x = dst_h - dst_y - 1;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            *dst++ = src[ src_row_offset + src_x ];
            src_row_offset += src_w;
        }
    }
}

int uimgdrv_rotate(
    void *dst_buf, void *src_buf, unsigned src_w, unsigned src_h, int ang)
{
    int err = 0;
    switch(ang)
    {
    case UIMG_ROTATE_0:
        break;

    case UIMG_ROTATE_90:
        rotate_90(dst_buf, src_buf, src_w, src_h);
        break;

    case UIMG_ROTATE_180:
        rotate_180(dst_buf, src_buf, src_w, src_h);
        break;

    case UIMG_ROTATE_270:
        rotate_270(dst_buf, src_buf, src_w, src_h);
        break;

    default:
        err = -1;
        break;
    }

    return err;
}

static
void stretch_none(
    uint32_t *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    uint32_t *src_buf,
    unsigned src_w,
    unsigned src_h)
{
    for(unsigned y = 0; y < dst_h; ++y)
    {
        for(unsigned x = 0; x < dst_w; ++x)
            *dst_buf++ = x < src_w && y < src_h ? src_buf[ src_w * y + x ] : 0;
    }
}

static
void stretch_nearby(
    uint32_t *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    uint32_t *src_buf,
    unsigned src_w,
    unsigned src_h)
{
    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        unsigned src_y = dst_y * src_h / dst_h;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            unsigned src_x = dst_x * src_w / dst_w;
            *dst_buf++ = src_buf[ src_w * src_y + src_x ];
        }
    }
}

static
uint32_t interpolate_image_value(
    uint32_t *buf, unsigned w, unsigned h, float x, float y)
{
    int x_left = floor(x);
    int x_right = ceil(x);
    int y_top = floor(y);
    int y_bottom = ceil(y);
    x_left = x_left >= 0 ? x_left : 0;
    x_right = x_right < w ? x_right : w - 1;
    y_top = y_top >= 0 ? y_top : 0;
    y_bottom = y_bottom < h ? y_bottom : h - 1;

    uint32_t v_lt = buf[ w * y_top + x_left ];
    uint32_t v_rt = buf[ w * y_top + x_right ];
    uint32_t v_lb = buf[ w * y_bottom + x_left ];
    uint32_t v_rb = buf[ w * y_bottom + x_right ];

    float dl = x - x_left;
    float dr = 1 - dl;
    float dt = y - y_top;
    float db = 1 - dt;

    unsigned b =
        UIMG_COLOUR_GET_B(v_lt) * dr * db +
        UIMG_COLOUR_GET_B(v_rt) * dl * db +
        UIMG_COLOUR_GET_B(v_lb) * dr * dt +
        UIMG_COLOUR_GET_B(v_rb) * dl * dt;
    unsigned g =
        UIMG_COLOUR_GET_G(v_lt) * dr * db +
        UIMG_COLOUR_GET_G(v_rt) * dl * db +
        UIMG_COLOUR_GET_G(v_lb) * dr * dt +
        UIMG_COLOUR_GET_G(v_rb) * dl * dt;
    unsigned r =
        UIMG_COLOUR_GET_R(v_lt) * dr * db +
        UIMG_COLOUR_GET_R(v_rt) * dl * db +
        UIMG_COLOUR_GET_R(v_lb) * dr * dt +
        UIMG_COLOUR_GET_R(v_rb) * dl * dt;
    unsigned a =
        UIMG_COLOUR_GET_A(v_lt) * dr * db +
        UIMG_COLOUR_GET_A(v_rt) * dl * db +
        UIMG_COLOUR_GET_A(v_lb) * dr * dt +
        UIMG_COLOUR_GET_A(v_rb) * dl * dt;

    return UIMG_MAKE_COLOUR(b, g, r, a);
}

static
void stretch_linear(
    uint32_t *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    uint32_t *src_buf,
    unsigned src_w,
    unsigned src_h)
{
    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        float src_y = (float) dst_y * ( src_h - 1 ) / ( dst_h - 1 );
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            float src_x = (float) dst_x * ( src_w - 1 ) / ( dst_w - 1 );
            *dst_buf++ =
                interpolate_image_value(src_buf, src_w, src_h, src_x, src_y);
        }
    }
}

int uimgdrv_stretch(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    int scale_mode)
{
    int err = 0;
    switch(scale_mode)
    {
    case UIMG_SCALE_NONE:
        stretch_none(dst_buf, dst_w, dst_h, src_buf, src_w, src_h);
        break;

    case UIMG_SCALE_NEARBY:
        stretch_nearby(dst_buf, dst_w, dst_h, src_buf, src_w, src_h);
        break;

    case UIMG_SCALE_LINEAR:
    case UIMG_SCALE_SPLINE:
        stretch_linear(dst_buf, dst_w, dst_h, src_buf, src_w, src_h);
        break;

    default:
        err = -1;
    }

    return err;
}

static
int blend_overlay(
    uint32_t *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    uint32_t *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect)
{
    for(int j = 0; j < dst_rect->h; ++j)
    {
        int dst_y = dst_rect->y + j;
        int src_y = src_rect->y + j;

        for(int i = 0; i < dst_rect->w; ++i)
        {
            int dst_x = dst_rect->x + i;
            int src_x = src_rect->x + i;

            dst_buf[ dst_w * dst_y + dst_x ] = src_buf[ src_w * src_y + src_x ];
        }
    }

    return 0;
}

static
uint32_t blend_alpha_pixel(uint32_t dst, uint32_t src)
{
    unsigned dst_b = UIMG_COLOUR_GET_B(dst);
    unsigned dst_g = UIMG_COLOUR_GET_G(dst);
    unsigned dst_r = UIMG_COLOUR_GET_R(dst);
    unsigned dst_a = UIMG_COLOUR_GET_A(dst);

    unsigned src_b = UIMG_COLOUR_GET_B(src);
    unsigned src_g = UIMG_COLOUR_GET_G(src);
    unsigned src_r = UIMG_COLOUR_GET_R(src);
    unsigned src_a = UIMG_COLOUR_GET_A(src);

    unsigned colour_base = (255*255) - ( 255 - src_a )*( 255 - dst_a );
    if(colour_base)
    {
        unsigned mix_a = colour_base / 255;
        unsigned mix_r = ( 255*src_r*src_a + dst_r*dst_a*( 255 - src_a ) ) / colour_base;
        unsigned mix_g = ( 255*src_g*src_a + dst_g*dst_a*( 255 - src_a ) ) / colour_base;
        unsigned mix_b = ( 255*src_b*src_a + dst_b*dst_a*( 255 - src_a ) ) / colour_base;

        return UIMG_MAKE_COLOUR(mix_b, mix_g, mix_r, mix_a);
    }
    else
    {
        return UIMG_COLOUR_EMPTY;
    }
}

static
int blend_alpha(
    uint32_t *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    uint32_t *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect)
{
    for(int j = 0; j < dst_rect->h; ++j)
    {
        int dst_y = dst_rect->y + j;
        int src_y = src_rect->y + j;

        for(int i = 0; i < dst_rect->w; ++i)
        {
            int dst_x = dst_rect->x + i;
            int src_x = src_rect->x + i;

            uint32_t *dst_v = &dst_buf[ dst_w * dst_y + dst_x ];
            uint32_t src_v = src_buf[ src_w * src_y + src_x ];

            *dst_v = blend_alpha_pixel(*dst_v, src_v);
        }
    }

    return 0;
}

int uimgdrv_blend(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect,
    int blend_mode)
{
    switch(blend_mode)
    {
    case UIMG_BLEND_OVERLAY:
        return blend_overlay(
            dst_buf, dst_w, dst_h, dst_rect, src_buf, src_w, src_h, src_rect);

    case UIMG_BLEND_ALPHA:
        return blend_alpha(
            dst_buf, dst_w, dst_h, dst_rect, src_buf, src_w, src_h, src_rect);

    default:
        return -1;
    }
}
