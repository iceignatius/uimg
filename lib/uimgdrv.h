#ifndef _UIMG_DRIVER_INTERFACE_H_
#define _UIMG_DRIVER_INTERFACE_H_

#include <stdint.h>
#include <stdatomic.h>
#include "drvid.h"
#include "uimgblitdef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Image object data
 */
struct uimg
{
    unsigned magic;     // A magic number to identify
                        // which driver be used to create the image buffer
    atomic_int refcnt;  // Object reference count
    unsigned w;         // Width of image in pixels
    unsigned h;         // Height of image in pixels
    void *buf;          // The driver defined image buffer
    int lockcnt;        // The directly buffer access lock count (mutable)
};

unsigned uimgdrv_get_magic(void);

/*
 * Object buffer allocation
 *
 * Need to allocate memory buffer only,
 * no need to initialise its contents.
 */
struct uimg* uimgdrv_alloc_imgslot(void);
void uimgdrv_free_imgslot(struct uimg *slot);

/*
 * Image buffer allocation
 */
void* uimgdrv_alloc_imgbuf(unsigned w, unsigned h);
void* uimgdrv_clone_imgbuf(void *drvbuf, unsigned w, unsigned h);
void uimgdrv_free_imgbuf(void *drvbuf);

/*
 * Image buffer access
 */
int uimgdrv_begin_buf_access(void *drvbuf);
int uimgdrv_end_buf_access(void *drvbuf);
uint32_t* uimgdrv_get_row_buf(void *drvbuf, unsigned w, unsigned h, unsigned y);

/*
 * Basic accelerated blit function
 */
int uimgdrv_fill(
    void *drvbuf, unsigned w, unsigned h, uint32_t val, const struct uimg_rect /*optional*/ *rect);
int uimgdrv_flip(void *dst_buf, void *src_buf, unsigned w, unsigned h, int dir);
int uimgdrv_rotate(
    void *dst_buf, void *src_buf, unsigned src_w, unsigned src_h, int ang);
int uimgdrv_stretch(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    int scale_mode);
int uimgdrv_blend(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect,
    int blend_mode);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
