#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <jpeglib.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgjpg.h"

struct error_handler
{
    struct jpeg_error_mgr super;

    jmp_buf jmpbuf;
};

#ifdef UIMG_ENABLE_JPEG_ENCODE
struct dest_mgr
{
    struct jpeg_destination_mgr super;

    uint8_t *buf;
    size_t bufsize;
    size_t fillsize;
};
#endif

struct src_mgr
{
    struct jpeg_source_mgr super;

    const uint8_t *data;
    size_t datasize;
    size_t readsize;
};

static
void error_handler_exit(j_common_ptr jinfo)
{
    jinfo->err->output_message(jinfo);

    struct error_handler *err = (struct error_handler*) jinfo->err;
    longjmp(err->jmpbuf, 1);
}

static
struct jpeg_error_mgr* error_handler_init(struct error_handler *err)
{
    struct jpeg_error_mgr *mgr = jpeg_std_error(&err->super);

    err->super.error_exit = error_handler_exit;

    return mgr;
};

#ifdef UIMG_ENABLE_JPEG_ENCODE
static
void dest_mgr_init_destination(j_compress_ptr cinfo)
{
    struct dest_mgr *mgr = (struct dest_mgr*) cinfo->dest;

    mgr->super.next_output_byte = mgr->buf;
    mgr->super.free_in_buffer = mgr->bufsize;
}
#endif

#ifdef UIMG_ENABLE_JPEG_ENCODE
static
boolean dest_mgr_empty_output_buffer(j_compress_ptr cinfo)
{
    // We had prepared a large buffer to hold entire data,
    // so the buffer should not be full up.
    jpeg_abort_compress(cinfo);

    return true;
}
#endif

#ifdef UIMG_ENABLE_JPEG_ENCODE
static
void dest_mgr_term_destination(j_compress_ptr cinfo)
{
    struct dest_mgr *mgr = (struct dest_mgr*) cinfo->dest;

    mgr->fillsize = mgr->bufsize - mgr->super.free_in_buffer;
}
#endif

#ifdef UIMG_ENABLE_JPEG_ENCODE
static
struct jpeg_destination_mgr* dest_mgr_init(
    struct dest_mgr *mgr, void *buf, size_t size)
{
    mgr->buf = buf;
    mgr->bufsize = size;
    mgr->fillsize = 0;

    mgr->super.next_output_byte = NULL;
    mgr->super.free_in_buffer = 0;
    mgr->super.init_destination = dest_mgr_init_destination;
    mgr->super.empty_output_buffer = dest_mgr_empty_output_buffer;
    mgr->super.term_destination = dest_mgr_term_destination;

    return &mgr->super;
}
#endif

static
void src_mgr_init_source(j_decompress_ptr dinfo)
{
    struct src_mgr *mgr = (struct src_mgr*) dinfo->src;

    mgr->super.next_input_byte = mgr->data;
    mgr->super.bytes_in_buffer = mgr->datasize;
}

static
boolean src_mgr_fill_input_buffer(j_decompress_ptr dinfo)
{
    return true;
}

static
void src_mgr_skip_input_data(j_decompress_ptr dinfo, long num_bytes)
{
    struct src_mgr *mgr = (struct src_mgr*) dinfo->src;

    mgr->super.next_input_byte += num_bytes;
    mgr->super.bytes_in_buffer -= num_bytes;
}

static
void src_mgr_term_source(j_decompress_ptr dinfo)
{
    struct src_mgr *mgr = (struct src_mgr*) dinfo->src;

    mgr->readsize = mgr->datasize - mgr->super.bytes_in_buffer;
}

static
struct jpeg_source_mgr* src_mgr_init(
    struct src_mgr *mgr, const void *data, size_t size)
{
    mgr->data = data;
    mgr->datasize = size;
    mgr->readsize = 0;

    mgr->super.next_input_byte = NULL;
    mgr->super.bytes_in_buffer = 0;
    mgr->super.init_source = src_mgr_init_source;
    mgr->super.fill_input_buffer = src_mgr_fill_input_buffer;
    mgr->super.skip_input_data = src_mgr_skip_input_data;
    mgr->super.resync_to_restart = jpeg_resync_to_restart;
    mgr->super.term_source = src_mgr_term_source;

    return &mgr->super;
}

#ifdef UIMG_ENABLE_JPEG_ENCODE
static
void uimg_row_to_jpeg_row(JSAMPLE jrow[], const uint32_t urow[], unsigned width)
{
    JSAMPLE *jpix = jrow;
    const uint32_t *upix = urow;

    for(; width--; ++upix, jpix += 3)
    {
        jpix[2] = UIMG_COLOUR_GET_B(*upix);
        jpix[1] = UIMG_COLOUR_GET_G(*upix);
        jpix[0] = UIMG_COLOUR_GET_R(*upix);
    }
}
#endif

static
void jpeg_row_to_uimg_row(
    uint32_t urow[],
    const JSAMPLE jrow[],
    unsigned width,
    unsigned colour_components)
{
    uint32_t *upix = urow;
    const JSAMPLE *jpix = jrow;

    if( colour_components == 1 )
    {
        for(; width--; jpix += colour_components)
            *upix++ = UIMG_MAKE_COLOUR(jpix[0], jpix[0], jpix[0], 255);
    }
    else if( colour_components == 3 )
    {
        for(; width--; jpix += colour_components)
            *upix++ = UIMG_MAKE_COLOUR(jpix[2], jpix[1], jpix[0], 255);
    }
    else
    {
        // Actually the code should not come here,
        // because we should have check the parameter earlier.
        while(width--)
            *upix++ = 0;
    }
}

size_t uimgjpg_encode(const uimg_t *img, void *buf, size_t size, int quality)
{
    /**
     * Encode to the JPEG format data
     *
     * @param img       The image object to be encoded.
     * @param buf       The buffer to receive the encoded data.
     * @param size      Size of the output buffer.
     * @param quality   Encode quality from 0 to 100.
     *                  Predefined values in ::uimgjpg_quality can be used
     *                  if have no idea what value should be used,
     *                  or set it to -1 to auto select.
     * @return Total size of data has been fill to the buffer; or ZERO if failed.
     */
#ifdef UIMG_ENABLE_JPEG_ENCODE
    if( !img || !img->buf || !img->w || !img->h ) return 0;
    if( !buf || !size ) return 0;

    if( quality < 0 )
        quality = UIMGJPG_QUALITY_MEDIUM;

    struct jpeg_compress_struct cinfo;
    memset(&cinfo, 0, sizeof(cinfo));

    // Error handler must be set before compressor creation.
    struct error_handler err;
    cinfo.err = error_handler_init(&err);

    jpeg_create_compress(&cinfo);

    // Destination must be set after compressor creation.
    struct dest_mgr dest_mgr;
    cinfo.dest = dest_mgr_init(&dest_mgr, buf, size);

    size_t res = 0;
    if( 0 == setjmp(err.jmpbuf) )
    {
        cinfo.image_width = img->w;
        cinfo.image_height = img->h;
        cinfo.input_components = 3; // Colour components per pixel.
                                    // 1: grey scale; 3: colourful.
        cinfo.in_color_space = JCS_RGB; // Can be JCS_GRAYSCALE or JCS_RGB.
        jpeg_set_defaults(&cinfo);
        jpeg_set_quality(&cinfo, quality, false);   // Quality vary from 0 ~ 100.

        jpeg_start_compress(&cinfo, true);

        int row_stride = cinfo.image_width * cinfo.input_components;
        for(unsigned y = 0; ( y = cinfo.next_scanline ) < cinfo.image_height;)
        {
            JSAMPLE row[row_stride];
            uimg_begin_buf_access(img);
            uimg_row_to_jpeg_row(row, uimg_get_row_buf_c(img, y), img->w);
            uimg_end_buf_access(img);

            JSAMPROW rows[1] = { row };
            if( 1 != jpeg_write_scanlines(&cinfo, rows, 1) )
                jpeg_abort_compress(&cinfo);
        }

        jpeg_finish_compress(&cinfo);

        res = dest_mgr.fillsize;
    }
    else
    {
        res = 0;
    }

    jpeg_destroy_compress(&cinfo);

    return res;
#else
    return 0;
#endif
}

size_t uimgjpg_decode(uimg_t *img, const void *data, size_t size)
{
    /**
     * Decode from the JPEG format data
     *
     * @param img   The image object to receive the decoded result.
     * @param data  The JPEG data to be decoded.
     * @param size  Size of the input data.
     * @return Total size of data has been read to decode; or ZERO if failed.
     */
    if( !img || !data || !size )
        return 0;

    struct jpeg_decompress_struct dinfo;
    memset(&dinfo, 0, sizeof(dinfo));

    // Error handler must be set before de-compressor creation.
    struct error_handler err;
    dinfo.err = error_handler_init(&err);

    jpeg_create_decompress(&dinfo);

    // Source must be set after de-compressor creation.
    struct src_mgr src_mgr;
    dinfo.src = src_mgr_init(&src_mgr, data, size);

    size_t res = 0;
    if( 0 == setjmp(err.jmpbuf) )
    {
        if( JPEG_HEADER_OK != jpeg_read_header(&dinfo, true) )
            jpeg_abort_decompress(&dinfo);

        if(!jpeg_start_decompress(&dinfo))
            jpeg_abort_decompress(&dinfo);

        if( dinfo.output_components != 1 && dinfo.output_components != 3 )
            jpeg_abort_decompress(&dinfo);  // Unsupported format!

        if(uimg_resize(img, dinfo.output_width, dinfo.output_height))
            jpeg_abort_decompress(&dinfo);

        int row_stride = dinfo.output_width * dinfo.output_components;
        for(unsigned y = 0; ( y = dinfo.output_scanline ) < dinfo.output_height;)
        {
            JSAMPLE row[row_stride];
            JSAMPROW rows[1] = { row };
            if( 1 != jpeg_read_scanlines(&dinfo, rows, 1) )
                jpeg_abort_decompress(&dinfo);

            uimg_begin_buf_access(img);
            jpeg_row_to_uimg_row(
                uimg_get_row_buf(img, y), row, img->w, dinfo.output_components);
            uimg_end_buf_access(img);
        }

        if( !jpeg_finish_decompress(&dinfo) )
            jpeg_abort_decompress(&dinfo);

        res = src_mgr.readsize;
    }
    else
    {
        res = 0;
    }

    jpeg_destroy_decompress(&dinfo);

    return res;
}

int uimgjpg_save_file(const uimg_t *img, const char *filename, int quality)
{
    /**
     * Save image to file
     *
     * @param img       The image object to be saved.
     * @param filename  Name of the file.
     * @param quality   Encode quality from 0 to 100.
     *                  Predefined values in ::uimgjpg_quality can be used
     *                  if have no idea what value should be used,
     *                  or set it to -1 to auto select.
     * @return ZERO if success; or other values indicate error occurred.
     */
#ifdef UIMG_ENABLE_JPEG_ENCODE
    if( !img || !filename ) return false;

    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if(!img->buf) break;

        static const size_t redundancy_size = 1024;
        size_t estimate_size = img->w * img->h * sizeof(uint32_t) + redundancy_size;
        if( !( buf = malloc(estimate_size) ) ) break;

        size_t encode_size = uimgjpg_encode(img, buf, estimate_size, quality);
        if(!encode_size) break;

        filebuf_save(filename, buf, encode_size);

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
#else
    return -1;
#endif
}

int uimgjpg_load_file(uimg_t *img, const char *filename)
{
    /**
     * Load image from file
     *
     * @param img       The image object to be loaded.
     * @param filename  Name of the file.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename ) return -1;

    size_t size = 0;
    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if( !( buf = filebuf_load(filename, &size) ) ) break;
        if(!uimgjpg_decode(img, buf, size)) break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}
