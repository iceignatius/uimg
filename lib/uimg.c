#include <assert.h>
#include <stdbool.h>
#include "uimgdrv.h"
#include "uimg.h"

unsigned uimg_get_drv_id(void)
{
    /**
     * Get the driver ID.
     *
     * This function usually be used to identify
     * which driver be used at runtime.
     * For the identification of which driver be bundled at compile time,
     * use the @p UIMG_USE_* macros instead.
     */
    return uimgdrv_get_magic();
}

uimg_t* uimg_create(int w, int h)
{
    /**
     * Create an image object
     *
     * @param w     Width of the new image,
     *              can set to ZERO to create a buffer-not-ready image object.
     * @param h     Height of the new image,
     *              can set to ZERO to create a buffer-not-ready image object.
     * @return  A new created image object will be returned if success,
     *          or NULL if error occurred
     *          (error may be induced by invalid input or failed allocation).
     */
    struct uimg *img = NULL;

    bool succ = false;
    do
    {
        if( w <= 0 || h <= 0 )
            w = h = 0;

        if( !( img = uimgdrv_alloc_imgslot() ) )
            break;

        img->magic = uimgdrv_get_magic();
        atomic_init(&img->refcnt, 1);
        img->w = w;
        img->h = h;
        img->buf = NULL;
        img->lockcnt = 0;

        if( w && h && !( img->buf = uimgdrv_alloc_imgbuf(w, h) ) )
            break;

        succ = true;
    } while(false);

    if( !succ && img )
    {
        uimgdrv_free_imgslot(img);
        img = NULL;
    }

    return img;
}

uimg_t* uimg_clone(const uimg_t *src)
{
    /**
     * Create an image object by clone contents from another
     *
     * @param src The source image object.
     * @return  A new created image object will be returned if success,
     *          or NULL if error occurred.
     */
    struct uimg *img = NULL;

    bool succ = false;
    do
    {
        if( !src || src->magic != uimgdrv_get_magic() )
            break;

        if( !( img = uimgdrv_alloc_imgslot() ) )
            break;

        img->magic = uimgdrv_get_magic();
        atomic_init(&img->refcnt, 1);
        img->w = src->w;
        img->h = src->h;
        img->buf = src->buf ? uimgdrv_clone_imgbuf(src->buf, src->w, src->h) : NULL;

        if( src->buf && !img->buf )
            break;

        succ = true;
    } while(false);

    if( !succ && img )
    {
        uimgdrv_free_imgslot(img);
        img = NULL;
    }

    return img;
}

uimg_t* uimg_shadow(const uimg_t *src)
{
    /**
     * Create an image object by share contents from another
     *
     * @param src The source image object.
     * @return  A new created image object will be returned if success,
     *          or NULL if error occurred.
     */
    uimg_t *img = (uimg_t*) src;

    if(img)
        atomic_fetch_add(&img->refcnt, 1);

    return img;
}

void uimg_release(uimg_t *img)
{
    /**
     * Release an image object
     *
     * @param img The image object which is not using anymore.
     *
     * @remarks
     * The image object may not be released in actually
     * because of that the other user my still using the object
     * (by the shadow reference).
     * The image object may be still usable and not released
     * until the last user release it.
     */
    if( !img || 1 != atomic_fetch_sub(&img->refcnt, 1) )
        return;

    assert( img->magic == uimgdrv_get_magic() );
    if( img->lockcnt > 0 )
        uimgdrv_end_buf_access(img->buf);
    if(img->buf)
        uimgdrv_free_imgbuf(img->buf);

    uimgdrv_free_imgslot(img);
}

int uimg_get_refcnt(const uimg_t *img)
{
    return img ? atomic_load(&img->refcnt) : 0;
}

int uimg_is_valid(const uimg_t *img)
{
    /**
     * Check if the image object is good to use
     *
     * @param img The image object to be query.
     * @return  A NON-ZERO value will be returned if the image object is good to use,
     *          it means that the object have the usable contents buffer and
     *          the non-zero resolution value;
     *          otherwise, ZERO will be returned.
     */
    return img && img->buf && img->w && img->h;
}

int uimg_get_width(const uimg_t *img)
{
    /**
     * Get width
     *
     * @param img The image object to be query.
     * @return Width of the image.
     */
    return img ? img->w : 0;
}

int uimg_get_height(const uimg_t *img)
{
    /**
     * Get height
     *
     * @param img The image object to be query.
     * @return Height of the image.
     */
    return img ? img->h : 0;
}

uint32_t uimg_get_pixel(uimg_t *img, int x, int y)
{
    /**
     * Get single pixel value
     *
     * @param img   The image object to be operated.
     * @param x     X position of the pixel.
     * @param y     Y position of the pixel.
     * @return  The pixel value on the specific position of the image; or
     *          ZERO will be returned if the input are invalid.
     */
    if( !img || img->magic != uimgdrv_get_magic() )
        return 0;
    if( !img->buf || x < 0 || img->w <= x || y < 0 || img->h <= y )
        return 0;

    uint32_t *row = uimgdrv_get_row_buf(img->buf, img->w, img->h, y);
    return row ? row[x] : 0;
}

int uimg_set_pixel(uimg_t *img, int x, int y, uint32_t val)
{
    /**
     * Set single pixel value
     *
     * @param img   The image object to be operated.
     * @param x     X position of the pixel.
     * @param y     Y position of the pixel.
     * @param val   The new pixel value.
     * @return ZERO if success; or other values if the input are invalid!
     */
    if( !img || img->magic != uimgdrv_get_magic() )
        return -1;
    if( !img->buf || x < 0 || img->w <= x || y < 0 || img->h <= y )
        return -1;

    uint32_t *row = uimgdrv_get_row_buf(img->buf, img->w, img->h, y);
    if(!row) return -1;

    row[x] = val;
    return 0;
}

int uimg_begin_buf_access(const uimg_t *img)
{
    /**
     * Begin directly buffer access
     *
     * Must be call this function before directly buffer read or write,
     * such like uimg_get_row_buf() and uimg_get_row_buf_c().
     *
     * @param img The image object to be operated.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     *
     * @remarks Must call uimg_end_buf_access()
     *          when the directly operation was done.
     *          Function uimg_begin_buf_access() and uimg_end_buf_access()
     *          may be called recursively or not,
     *          it is depend on the driver implementation.
     */
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;

    assert( img->lockcnt >= 0 );
    if( ((uimg_t*)img)->lockcnt ++ != 0 )
        return 0;

    int err = uimgdrv_begin_buf_access(img->buf);
    if(err)
        -- ((uimg_t*)img)->lockcnt;

    return err;
}

int uimg_end_buf_access(const uimg_t *img)
{
    /**
     * End directly buffer access
     *
     * Must be call this function when the directly buffer read or write is done.
     *
     * @param img The image object to be operated.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     */
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;

    assert( img->lockcnt > 0 );
    if( -- ((uimg_t*)img)->lockcnt != 0 )
        return 0;

    int err = uimgdrv_end_buf_access(img->buf);
    if(err)
        ++ ((uimg_t*)img)->lockcnt;

    return err;
}

uint32_t* uimg_get_row_buf(uimg_t *img, int y)
{
    /**
     * Get buffer of a specific row of image
     *
     * @param img   The image object to be operated.
     * @param y     Index of the row.
     * @return The specific row buffer; or NULL if the input are invalid.
     *
     * @remarks Must call uimg_begin_buf_access() before calling this function.
     */
    if( !img || img->magic != uimgdrv_get_magic() )
        return NULL;

    return img->buf && 0 <= y && y < img->h ?
        uimgdrv_get_row_buf(img->buf, img->w, img->h, y) : NULL;
}

const uint32_t* uimg_get_row_buf_c(const uimg_t *img, int y)
{
    /**
     * Get buffer of a specific row of image
     *
     * @param img   The image object to be operated.
     * @param y     Index of the row.
     * @return The specific row buffer; or NULL if the input are invalid.
     *
     * @remarks Must call uimg_begin_buf_access() before calling this function.
     */
    return uimg_get_row_buf((uimg_t*)img, y);
}

void* uimg_get_drv_buf(uimg_t *img)
{
    /**
     * Get the driver defined image buffer
     *
     * @param img The image object to be operated.
     * @return The driver defined image buffer.
     */
    return img ? img->buf : NULL;
}

int uimg_resize(uimg_t *img, int w, int h)
{
    /**
     * Resize image
     *
     * @param img   The image object to be operated.
     * @param w     The new width.
     * @param h     The new height.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     *
     * @remarks
     * The image object may not keep its contents,
     * it is depend on the driver implementation.
     * It means that the image data may be undefined
     * after a successfully buffer resize.
     */
    if( w <= 0 || h <= 0 )
        w = h = 0;

    if( !img || img->magic != uimgdrv_get_magic() )
        return -1;
    if( img && img->w == w && img->h == h )
        return 0;

    void *newbuf = NULL;
    if( w && h && !( newbuf = uimgdrv_alloc_imgbuf(w, h) ) )
        return -1;

    if( img->lockcnt > 0 )
        uimgdrv_end_buf_access(img->buf);
    if(img->buf)
        uimgdrv_free_imgbuf(img->buf);

    img->buf = newbuf;
    img->lockcnt = 0;

    img->w = w;
    img->h = h;

    return 0;
}

int uimg_freebuf(uimg_t *img)
{
    /**
     * Deallocate image buffer
     *
     * @param img The image object to be operated.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     */
    if( !img || img->magic != uimgdrv_get_magic() )
        return -1;

    if(img->buf)
    {
        uimgdrv_free_imgbuf(img->buf);
        img->buf = NULL;
        img->w = 0;
        img->h = 0;
    }

    return 0;
}

int uimg_fill(uimg_t *img, uint32_t val)
{
    /**
     * Fill image with a specific colour
     *
     * @param img The image object to be operated.
     * @param val The colour to be filled to the image.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     */
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;

    return uimgdrv_fill(img->buf, img->w, img->h, val, NULL);
}
