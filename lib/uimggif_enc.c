#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gif_lib.h>
#include "uimgdrv.h"
#include "uimggif_palette.h"
#include "uimggif_enc.h"

#define TERMINATOR_INTRODUCER 0x3B

#define CEIL4096(x) ( ( (x) + 4096 - 1 ) & ~( 4096 - 1 ) )

struct output_pack
{
    uint8_t *buf;
    size_t buf_size;
    size_t fill_size;
    bool is_user_buf;
    FILE *file;
};

struct uimggif_enc
{
    struct output_pack output;
    struct uimggif_palette *palette;
    ColorMapObject *colour_map;
    GifFileType *gif;
};

static int EGifSpewNoClose(GifFileType *fileout);

static
int calc_colour_resolution(uint8_t colour_num)
{
    for(int i = 0; i < 8; ++i)
    {
        if( 1 << i >= colour_num )
            return i;
    }

    return 0;
}

uimggif_enc_t* uimggif_enc_open_file(const char *filename, int palette_id)
{
    /**
     * Create an encode handler to write a file
     *
     * @param filename      Name of the file to output
     * @param palette_id    The palette ID (::uimggif_palette_id) to be used to
     *                      encode images
     * @return  The created encode handler on success;
     *          otherwise, NULL will be returned.
     */
    if(!filename)
        return NULL;

    uimggif_enc_t *enc = NULL;

    bool succ = false;
    do
    {
        if( !( enc = uimggif_enc_open_mem(NULL, 0, palette_id) ) )
            break;

        if( !( enc->output.file = fopen(filename, "wb") ) )
            break;

        succ = true;
    } while(false);

    if( !succ && enc )
    {
        uimggif_enc_close(enc);
        enc = NULL;
    }

    return enc;
}

static
int gif_write_raw(GifFileType *gif, const GifByteType *data, int size)
{
    uimggif_enc_t *enc = gif->UserData;

    size_t req_size = enc->output.fill_size + size + (1/*terminator*/);
    req_size = CEIL4096(req_size);

    if( req_size > enc->output.buf_size )
    {
        if(enc->output.is_user_buf)
            return -1;

        uint8_t *newbuf = realloc(enc->output.buf, req_size);
        if(!newbuf)
            return -1;

        enc->output.buf = newbuf;
        enc->output.buf_size = req_size;
    }

    if(!enc->output.buf)
        return -1;

    memcpy(enc->output.buf + enc->output.fill_size, data, size);
    enc->output.fill_size += size;

    return size;
}

uimggif_enc_t* uimggif_enc_open_mem(void *buf, size_t size, int palette_id)
{
    /**
     * Create an encode handler to encode data to a memory buffer
     *
     * @param buf       A buffer to be filled with the GIF format data.
     *                  This parameter can be NULL, and if so,
     *                  encode will allocate and maintain
     *                  an internal buffer for output,
     *                  which can be accessed by uimggif_enc_get_rawdata()
     *                  and uimggif_enc_get_rawsize().
     * @param size      Size of the output buffer.
     *                  This parameter will be ignored if @a buf is NULL.
     * @param palette_id    The palette ID (::uimggif_palette_id) to be used to
     *                      encode images
     * @return  The created encode handler on success;
     *          otherwise, NULL will be returned.
     */
    if( palette_id <= 0 )
        palette_id = UIMGGIF_PALETTE_VGA256;

    uimggif_enc_t *enc = NULL;

    bool succ = false;
    do
    {
        if( !( enc = calloc(1, sizeof(*enc)) ) )
            break;

        if(buf)
        {
            if(!size)
                break;

            enc->output.buf = buf;
            enc->output.buf_size = size;
            enc->output.is_user_buf = true;
        }

        if( !( enc->palette = uimggif_palette_find(palette_id) ) )
            break;

        const GifColorType *table = enc->palette->get_table(enc->palette);
        unsigned colour_num = enc->palette->count_colours(enc->palette);

        if( !( enc->colour_map = GifMakeMapObject(colour_num, table) ) )
            break;

        int err;
        if( !( enc->gif = EGifOpen(enc, gif_write_raw, &err) ) )
            break;

        EGifSetGifVersion(enc->gif, true);

        enc->gif->SWidth = 0;
        enc->gif->SHeight = 0;
        enc->gif->SColorResolution = calc_colour_resolution(colour_num);
        enc->gif->SBackGroundColor = 0;
        enc->gif->AspectByte = 0;
        enc->gif->SColorMap = enc->colour_map;
        enc->gif->ImageCount = 0;
        enc->gif->SavedImages = NULL;
        enc->gif->ExtensionBlockCount = 0;
        enc->gif->ExtensionBlocks = NULL;
        enc->gif->Error = 0;

        succ = true;
    } while(false);

    if( !succ && enc )
    {
        uimggif_enc_close(enc);
        enc = NULL;
    }

    return enc;
}

void uimggif_enc_close(uimggif_enc_t *enc)
{
    /**
     * End use of the encode handler and release its resources
     *
     * @param enc The encode handler
     */
    if(enc)
    {
        int img_num = 0;
        SavedImage *img_list = NULL;

        if(enc->gif)
        {
            img_num = enc->gif->ImageCount;
            img_list = enc->gif->SavedImages;

            enc->gif->SColorMap = NULL; // To avoid the GIF library's bug
            EGifCloseFile(enc->gif, NULL);
        }

        for(int i = 0; i < img_num; ++i)
        {
            SavedImage *img = &img_list[i];

            GifFreeExtensions(&img->ExtensionBlockCount, &img->ExtensionBlocks);
            if(img->RasterBits)
                free(img->RasterBits);
        }

        if(img_list)
            free(img_list);

        if(enc->colour_map)
            GifFreeMapObject(enc->colour_map);

        if(enc->output.file)
            fclose(enc->output.file);
        if( enc->output.buf && !enc->output.is_user_buf )
            free(enc->output.buf);

        free(enc);
    }
}

int uimggif_enc_get_width(const uimggif_enc_t *enc)
{
    /**
     * Get image width
     *
     * @param enc The encode handler
     * @return The image width if available; otherwise, -1 will be returned.
     *
     * @remarks
     * The width value may not be available before that
     * the first image be added into the encoder.
     */
    return enc && enc->gif ? enc->gif->SWidth : -1;
}

int uimggif_enc_get_height(const uimggif_enc_t *enc)
{
    /**
     * Get image height
     *
     * @param enc The encode handler
     * @return The image height if available; otherwise, -1 will be returned.
     *
     * @remarks
     * The height value may not be available before that
     * the first image be added into the encoder.
     */
    return enc && enc->gif ? enc->gif->SHeight : -1;
}

int uimggif_enc_count_colour(const uimggif_enc_t *enc)
{
    /**
     * Get number of colours
     *
     * @param enc The encoder handler
     * @return Number of colours if available; otherwise -1 will be returned.
     *
     * @remarks
     * This GIF encoder only support the Web Safe Colours sofar, so that
     * this function always returns 256 if the encode handler is available.
     * In actually, the Web Safe Colours only have 216 available colours, then
     * we padding some dummy data to make it grow to be 256 because of that
     * the GIF format request the colour number be power of 2.
     */
    return enc && enc->palette ? enc->palette->count_colours(enc->palette) : -1;
}

int uimggif_enc_count_image(const uimggif_enc_t *enc)
{
    /**
     * Count images
     *
     * @param enc The encode handler
     * @return  Number of images of this file if available;
     *          otherwise, -1 will be returned.
     */
    return enc && enc->gif ? enc->gif->ImageCount : -1;
}

int uimggif_enc_add_image(
    uimggif_enc_t *enc, const uimg_t *img, int dura)
{
    /**
     * Add one image
     *
     * @param enc   The encode handler
     * @param img   The image to be added.
     *              Dimension (width and height) of this image must be
     *              the same as the previous added image except that
     *              this is the first image be added to the encoder, or
     *              this call will return an error and
     *              the image will not be added.
     * @param dura  Duration of this image in milliseconds
     * @return ZERO if success; otherwise, -1 will be returned.
     */
    int blknum = 0;
    ExtensionBlock *blklist = NULL;
    GifByteType *imgbuf = NULL;

    bool succ = false;
    do
    {
        /*
         * Check parameters
         */

        if(!enc)
            break;
        if( !img || !img->buf || img->w <= 0 || img->h <= 0 )
            break;
        if( dura < 0 )
            dura = 10;

        bool is_first_img = enc->gif->ImageCount == 0;
        if( !is_first_img &&
            ( img->w != enc->gif->SWidth || img->h != enc->gif->SHeight ) )
        {
            break;
        }

        /*
         * Store special extension blocks for the first image
         */

        if( is_first_img && GIF_OK != GifAddExtensionBlock(
            &blknum, &blklist, APPLICATION_EXT_FUNC_CODE, 11,
            (unsigned char*) "NETSCAPE2.0") )
        {
            break;
        }

        if( is_first_img && GIF_OK != GifAddExtensionBlock(
            &blknum, &blklist, CONTINUE_EXT_FUNC_CODE, 3,
            (unsigned char*) "\x01\x00\x00") )
        {
            break;
        }

        /*
         * Build up image data
         */

        if( !( imgbuf = malloc( img->w * img->h ) ) )
            break;

        uimg_begin_buf_access(img);
        for(int y = 0; y < img->h; ++y)
        {
            const uint32_t *src_row = uimg_get_row_buf_c(img, y);
            GifByteType *dst_row = &imgbuf[ img->w * y ];

            for(int x = 0; x < img->w; ++x)
                dst_row[x] = enc->palette->calc_index(enc->palette, src_row[x]);
        }
        uimg_end_buf_access(img);

        /*
         * Append GIF image list
         */

        SavedImage *g_img = GifMakeSavedImage(enc->gif, NULL);
        if(!g_img)
            break;

        if(is_first_img)
        {
            enc->gif->SWidth = img->w;
            enc->gif->SHeight = img->h;
        }

        g_img->ImageDesc.Left = 0;
        g_img->ImageDesc.Top = 0;
        g_img->ImageDesc.Width = img->w;
        g_img->ImageDesc.Height = img->h;
        g_img->RasterBits = imgbuf;
        g_img->ExtensionBlockCount = blknum;
        g_img->ExtensionBlocks = blklist;

        imgbuf = NULL;
        blknum = 0;
        blklist = NULL;

        /*
         * Add the GCB extension block
         */

        GraphicsControlBlock gcb =
        {
            .DisposalMode = DISPOSE_DO_NOT,
            .UserInputFlag = false,
            .DelayTime = dura / 10, // pre-display delay in 0.01 sec units
            .TransparentColor = enc->palette->get_transparent(enc->palette),
        };

        EGifGCBToSavedExtension(&gcb, enc->gif, enc->gif->ImageCount - 1);  // Ignore error

        succ = true;
    } while(false);

    if(imgbuf)
        free(imgbuf);
    if(blklist)
        GifFreeExtensions(&blknum, &blklist);

    return succ ? 0 : -1;
}

size_t uimggif_enc_write(uimggif_enc_t *enc)
{
    /**
     * Write file contents
     *
     * @param enc The encoder handler
     * @return  How many bytes be filled to the output buffer if success;
     *          otherwise, ZERO will be returned.
     */
    if(!enc)
        return 0;

    enc->output.fill_size = 0;
    if(enc->output.file)
        rewind(enc->output.file);

    bool succ = GIF_OK == EGifSpewNoClose(enc->gif);

    // Because the weird behaviour of GIF library!
    if( enc->gif->SColorMap != enc->colour_map )
    {
        GifFreeMapObject(enc->gif->SColorMap);
        enc->gif->SColorMap = enc->colour_map;
    }

    /*
     * Pre-append the file terminator!
     *
     * The file terminator will be wrote on close GIF instance, but
     * that will be bother to get the total encoded raw data
     * before close and release buffers.
     * That is why we pre-append the terminator byte here!
     */
    enc->output.buf[enc->output.fill_size] = TERMINATOR_INTRODUCER;

    if(enc->output.file)
    {
        size_t write_size = enc->output.fill_size + (1/*terminator*/);
        succ = succ && write_size == fwrite(
            enc->output.buf, 1, write_size, enc->output.file);
    }

    return succ ? enc->output.fill_size + (1/*terminator*/) : 0;
}

const void* uimggif_enc_get_rawdata(const uimggif_enc_t *enc)
{
    /**
     * Get the encoder output buffer
     *
     * @param enc The encode handler
     * @return The output buffer which contains the encoded raw data
     */
    return enc ? enc->output.buf : NULL;
}

size_t uimggif_enc_get_rawsize(const uimggif_enc_t *enc)
{
    /**
     * Get the encoded data size
     *
     * @param enc The encode handler
     * @return Size of the encoded raw data be filled into the output buffer
     */
    return enc ? enc->output.fill_size + (1/*terminator*/) : 0;
}

int uimggif_hlpr_save_file(
    const uimg_t *img, const char *filename, int palette_id)
{
    /**
     * @brief Save image to file
     *
     * This is a helper function to simplify the use case of
     * just need to write only one image to a file.
     *
     * @param img           The image object to be saved
     * @param filename      Name of the file to be write
     * @param palette_id    The palette ID (::uimggif_palette_id) to be used to
     *                      encode images
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !img->buf || img->w <= 0 || img->h <= 0 )
        return -1;
    if(!filename)
        return -1;

    uimggif_enc_t *enc = NULL;

    int err = -1;
    do
    {
        if( palette_id <= 0 )
            palette_id = UIMGGIF_PALETTE_VGA256;

        if( !( enc = uimggif_enc_open_file(filename, palette_id) ) )
            break;

        if(uimggif_enc_add_image(enc, img, -1))
            break;

        if(!uimggif_enc_write(enc))
            break;

        err = 0;
    } while(false);

    if(enc)
        uimggif_enc_close(enc);

    return err;
}

/*
 * Below are private functions (and modifications) from "egif_lib.c"
 */

static
int EGifWriteExtensions(GifFileType *fileout, ExtensionBlock *blocks, int count)
{
    if(blocks)
    {
        for(int i = 0; i < count; ++i)
        {
            ExtensionBlock *ep = &blocks[i];

            if( ep->Function != CONTINUE_EXT_FUNC_CODE &&
                EGifPutExtensionLeader(fileout, ep->Function) == GIF_ERROR )
            {
                return GIF_ERROR;
            }

            if( EGifPutExtensionBlock(fileout, ep->ByteCount, ep->Bytes) == GIF_ERROR )
                return GIF_ERROR;

            if( i == count - 1 || ( ep + 1 )->Function != CONTINUE_EXT_FUNC_CODE)
            {
                if( EGifPutExtensionTrailer(fileout) == GIF_ERROR )
                    return GIF_ERROR;
            }
        }
    }

    return GIF_OK;
}

static
int EGifSpewNoClose(GifFileType *fileout)
{
    if( EGifPutScreenDesc(
        fileout,
        fileout->SWidth,
        fileout->SHeight,
        fileout->SColorResolution,
        fileout->SBackGroundColor,
        fileout->SColorMap) == GIF_ERROR )
    {
        return GIF_ERROR;
    }

    for(int i = 0; i < fileout->ImageCount; ++i)
    {
        SavedImage *img = &fileout->SavedImages[i];
        int width = img->ImageDesc.Width;
        int height = img->ImageDesc.Height;

        // this allows us to delete images by nuking their rasters
        if(!img->RasterBits)
            continue;

        if( EGifWriteExtensions(
            fileout, img->ExtensionBlocks, img->ExtensionBlockCount) == GIF_ERROR )
        {
            return GIF_ERROR;
        }

        if( EGifPutImageDesc(
            fileout,
            img->ImageDesc.Left,
            img->ImageDesc.Top,
            width,
            height,
            img->ImageDesc.Interlace,
            img->ImageDesc.ColorMap) == GIF_ERROR )
        {
            return GIF_ERROR;
        }

        if(img->ImageDesc.Interlace)
        {
            /*
             * The way an interlaced image should be written -
             * offsets and jumps...
             */
            static const int InterlacedOffset[] = { 0, 4, 2, 1 };
            static const int InterlacedJumps[] = { 8, 8, 4, 2 };
            // Need to perform 4 passes on the images:
            for(int j = 0; j < 4; ++j)
            {
                for(int k = InterlacedOffset[j]; k < height; k += InterlacedJumps[j])
                {
                    if( EGifPutLine(
                        fileout, img->RasterBits + k * width, width)	== GIF_ERROR )
                    {
                        return GIF_ERROR;
                    }
                }
            }
        }
        else
        {
            for(int j = 0; j < height; ++j)
            {
                if( EGifPutLine(
                    fileout, img->RasterBits + j * width, width) == GIF_ERROR )
                {
                    return GIF_ERROR;
                }
            }
        }
    }

    if( EGifWriteExtensions(
        fileout, fileout->ExtensionBlocks, fileout->ExtensionBlockCount) == GIF_ERROR )
    {
        return GIF_ERROR;
    }

    return GIF_OK;
}
