#ifndef _GIF_PALETTE_VGA16_H_
#define _GIF_PALETTE_VGA16_H_

#include <limits.h>
#include <gif_lib.h>
#include "uimgcolour.h"

static const GifColorType vga16_table[] =
{
    { 0, 0, 0 },
    { 0, 2, 170 },
    { 20, 170, 0 },
    { 0, 170, 170 },
    { 170, 0, 3 },
    { 170, 0, 170 },
    { 170, 85, 0 },
    { 170, 170, 170 },
    { 85, 85, 85 },
    { 85, 85, 255 },
    { 85, 255, 85 },
    { 85, 255, 255 },
    { 255, 85, 85 },
    { 253, 85, 255 },
    { 255, 255, 85 },
    { 255, 255, 255 },
};

static
const GifColorType* get_vga16_table(void *host)
{
    return vga16_table;
}

static
unsigned count_vga16_colours(void *host)
{
    return sizeof(vga16_table) / sizeof(vga16_table[0]);
}

static
int calc_vga16_fast_abs(int x)
{
    int mask = x >> ( sizeof(int) * CHAR_BIT - 1 );
    return ( ( x + mask ) ^ mask );
}

static
uint8_t calc_vga16_index(void *host, uint32_t colour)
{
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned b = UIMG_COLOUR_GET_B(colour);

    int n = sizeof(vga16_table) / sizeof(vga16_table[0]);
    const GifColorType *table = vga16_table;

    int min_diff = INT_MAX;
    int min_index = -1;

    for(int i = 0; i < n; ++i)
    {
        int diff =
            calc_vga16_fast_abs( r - table[i].Red ) +
            calc_vga16_fast_abs( g - table[i].Green ) +
            calc_vga16_fast_abs( b - table[i].Blue );
        if( min_diff > diff )
        {
            min_diff = diff;
            min_index = i;
        }
    }

    return min_index;
}

static
int get_vga16_transparent(void *host)
{
    return NO_TRANSPARENT_COLOR;
}

#endif
