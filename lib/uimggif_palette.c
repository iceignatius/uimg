#include <pthread.h>
#include "gif_palette_bw.h"
#include "gif_palette_vga16.h"
#include "gif_palette_vga256.h"
#include "gif_palette_fast332.h"
#include "gif_palette_websafe.h"
#include "gif_palette_grey.h"
#include "uimggif_palette.h"

#define CUSTOM_MIN UIMGGIF_PALETTE_CUSTOM_MIN
#define CUSTOM_MAX UIMGGIF_PALETTE_CUSTOM_MAX
#define CUSTOM_NUM ( CUSTOM_MAX - CUSTOM_MIN + 1 )

static struct uimggif_palette *customised_list[CUSTOM_NUM] = {0};
static pthread_mutex_t customised_mutex = PTHREAD_MUTEX_INITIALIZER;

static struct uimggif_palette palette_bw =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_bw_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_bw_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_bw_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_bw_transparent,
};

static struct uimggif_palette palette_vga16 =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_vga16_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_vga16_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_vga16_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_vga16_transparent,
};

static struct uimggif_palette palette_vga256 =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_vga256_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_vga256_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_vga256_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_vga256_transparent,
};

static struct uimggif_palette palette_fast332 =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_fast332_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_fast332_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_fast332_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_fast332_transparent,
};

static struct uimggif_palette palette_websafe =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_websafe_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_websafe_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_websafe_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_websafe_transparent,
};

static struct uimggif_palette palette_grey =
{
    .get_table = (const GifColorType*(*)(struct uimggif_palette*)) get_grey_table,
    .count_colours = (unsigned(*)(struct uimggif_palette*)) count_grey_colours,
    .calc_index = (uint8_t(*)(struct uimggif_palette*, uint32_t)) calc_grey_index,
    .get_transparent = (int(*)(struct uimggif_palette*)) get_grey_transparent,
};

int uimggif_palette_acquire_id(struct uimggif_palette *palette)
{
    if(!palette)
        return -1;

    pthread_mutex_lock(&customised_mutex);

    int id = -1;
    for(int index = 0; id < 0 && index < CUSTOM_NUM; ++index)
    {
        if(!customised_list[index])
        {
            customised_list[index] = palette;
            id = CUSTOM_MIN + index;
        }
    }

    pthread_mutex_unlock(&customised_mutex);

    return id;
}

int uimggif_palette_release_id(int id)
{
    if( id < CUSTOM_MIN || CUSTOM_MAX < id )
        return -1;

    pthread_mutex_lock(&customised_mutex);

    int index = id - CUSTOM_MIN;
    int err = customised_list[index] ? 0 : -1;
    customised_list[index] = NULL;

    pthread_mutex_unlock(&customised_mutex);

    return err;
}

struct uimggif_palette* uimggif_palette_find(int id)
{
    switch(id)
    {
    case UIMGGIF_PALETTE_BW:
        return &palette_bw;

    case UIMGGIF_PALETTE_VGA16:
        return &palette_vga16;

    case UIMGGIF_PALETTE_VGA256:
        return &palette_vga256;

    case UIMGGIF_PALETTE_FAST332:
        return &palette_fast332;

    case UIMGGIF_PALETTE_WEBSAFE:
        return &palette_websafe;

    case UIMGGIF_PALETTE_GREY:
        return &palette_grey;
    }

    if( id < CUSTOM_MIN || CUSTOM_MAX < id )
        return NULL;

    pthread_mutex_lock(&customised_mutex);

    int index = id - CUSTOM_MIN;
    struct uimggif_palette *palette = customised_list[index];

    pthread_mutex_unlock(&customised_mutex);

    return palette;
}
