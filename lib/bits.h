#ifndef _BITS_UTILITY_H_
#define _BITS_UTILITY_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

static inline
int bits_get_bit(const void *bits, unsigned index)
{
    return ( ((uint8_t*)bits)[ index >> 3 ] & ( 0x80 >> ( index & 0x07 ) ) ) ?
        1 : 0;
}

static inline
void bits_set_bit(void *bits, unsigned index)
{
    ((uint8_t*)bits)[ index >> 3 ] |= ( 0x80 >> ( index & 0x07 ) );
}

static inline
void bits_clear_bit(void *bits, unsigned index)
{
    ((uint8_t*)bits)[ index >> 3 ] &= ~( 0x80 >> ( index & 0x07 ) );
}

static inline
void bits_switch_bit(void *bits, unsigned index, int value)
{
    value ? bits_set_bit(bits, index) : bits_clear_bit(bits, index);
}

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
