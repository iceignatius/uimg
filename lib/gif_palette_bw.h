#ifndef _GIF_PALETTE_BW_H_
#define _GIF_PALETTE_BW_H_

#include <gif_lib.h>
#include "uimgcolour.h"

static
const GifColorType* get_bw_table(void *host)
{
    static const GifColorType palette[2] =
    {
        { 0, 0, 0 },
        { 255, 255, 255 },
    };

    return palette;
}

static
unsigned count_bw_colours(void *host)
{
    return 2;
}

static
uint8_t calc_bw_index(void *host, uint32_t colour)
{
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned b = UIMG_COLOUR_GET_B(colour);
    unsigned grey = ( r*19595 + g*38469 + b*7472 ) >> 16;

    return grey > 127 ? 1 : 0;
}

static
int get_bw_transparent(void *host)
{
    return NO_TRANSPARENT_COLOR;
}

#endif
