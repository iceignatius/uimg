#ifndef _GIF_PALETTE_FAST332_H_
#define _GIF_PALETTE_FAST332_H_

#include <gif_lib.h>
#include "uimgcolour.h"

static
const GifColorType* get_fast332_table(void *host)
{
    static GifColorType palette[265];

    static bool inited = false;
    if(inited)
        return palette;

    for(unsigned i = 0; i < 256; ++i)
    {
        palette[i].Red = i & 0xE0;
        palette[i].Green = ( i << 3 ) & 0xE0;
        palette[i].Blue = ( i << 6 ) & 0xC0;
    }

    inited = true;
    return palette;
}

static
unsigned count_fast332_colours(void *host)
{
    return 256;
}

static
uint8_t calc_fast332_index(void *host, uint32_t colour)
{
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned b = UIMG_COLOUR_GET_B(colour);

    return ( ( r & 0xE0 ) | ( ( g & 0xE0 ) >> 3 ) | ( ( b & 0xC0 ) >> 6 ) );
}

static
int get_fast332_transparent(void *host)
{
    return NO_TRANSPARENT_COLOR;
}

#endif
