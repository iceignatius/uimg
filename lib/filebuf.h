#ifndef _FILE_BUFFER_H_
#define _FILE_BUFFER_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

static inline
void* filebuf_load_handle(FILE *file, size_t *filesize)
{
    uint8_t *buf = NULL;
    size_t size = 0;

    bool succ = true;
    while( !feof(file) && !ferror(file) )
    {
        unsigned c = fgetc(file);
        if( c == (unsigned) EOF )
            break;

        uint8_t *newbuf = (uint8_t*) realloc(buf, size + 1);
        if(newbuf)
        {
            buf = newbuf;
            buf[size++] = c;
            *filesize = size;
        }
        else
        {
            succ = false;
            break;
        }
    }

    if( !succ && buf )
    {
        free(buf);
        buf = NULL;
    }

    return buf;
}

static inline
void* filebuf_load(const char *filename, size_t *filesize)
{
    uint8_t *buf = NULL;
    FILE *file = NULL;

    bool succ = false;
    do
    {
        if( !( file = fopen(filename, "rb") ) ) break;

        if(fseek(file, 0, SEEK_END)) break;
        long size = ftell(file);
        if( size <= 0 ) break;

        if( !( buf = (uint8_t*) malloc(size) ) ) break;

        rewind(file);
        if( fread(buf, 1, size, file) != (size_t) size ) break;

        if(filesize)
            *filesize = size;

        succ = true;
    } while(false);

    if(file)
        fclose(file);

    if( !succ && buf )
    {
        free(buf);
        buf = NULL;
    }

    return buf;
}

static inline
bool filebuf_save(const char *filename, const void *data, size_t size)
{
    FILE *file = NULL;

    bool succ = false;
    do
    {
        if( !( file = fopen(filename, "wb") ) ) break;
        if( fwrite(data, 1, size, file) != size ) break;

        succ = true;
    } while(false);

    if(file)
        fclose(file);

    return succ;
}

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
