#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <libheif/heif.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgheif_enc.h"

struct uimgheif_enc
{
    struct heif_context *heif_ctx;
    struct heif_encoder *heif_enc;
    unsigned img_num;
    int width;
    int height;
};

struct write_status
{
    uint8_t *buf;
    size_t rest_size;
    size_t fill_size;
};

uimgheif_enc_t* uimgheif_enc_create(int flags)
{
    /**
     * Create an encode handler
     *
     * @param flags To specify the encode quality.
     *              If the UIMGHEIF_QUALITY_LOSSLESS flag is set, then
     *              the image will be encoded by the lossless compression;
     *              otherwise, this value should be a quality factor with
     *              range 0 ~ 100; or
     *              just set to -1 to use the default value.
     *              See ::uimgheif_quality for predefined values.
     * @return  The created encode handler on success;
     *          otherwise, NULL will be returned.
     */
    uimgheif_enc_t *enc = NULL;

    bool succ = false;
    do
    {
        if( !( enc = calloc(1, sizeof(*enc)) ) )
            break;

        if( !( enc->heif_ctx = heif_context_alloc() ) )
            break;

        struct heif_error err = heif_context_get_encoder_for_format(
            enc->heif_ctx, heif_compression_HEVC, &enc->heif_enc);
        if(err.code)
            break;

        bool use_lossless = flags & UIMGHEIF_QUALITY_LOSSLESS;
        err = heif_encoder_set_lossless(enc->heif_enc, use_lossless);
        if(err.code)
            break;

        if(!use_lossless)
        {
            err = heif_encoder_set_lossy_quality(enc->heif_enc, flags % 101);
            if(err.code)
                break;
        }

        succ = true;
    } while(false);

    if( !succ && enc )
    {
        uimgheif_enc_release(enc);
        enc = NULL;
    }

    return enc;
}

void uimgheif_enc_release(uimgheif_enc_t *enc)
{
    /**
     * End use and release the encode handler
     *
     * @param enc The encode handler
     */
    if(enc)
    {
        if(enc->heif_enc)
            heif_encoder_release(enc->heif_enc);

        if(enc->heif_ctx)
            heif_context_free(enc->heif_ctx);

        free(enc);
    }
}

int uimgheif_enc_count_image(const uimgheif_enc_t *enc)
{
    /**
     * Count images
     *
     * @param enc The encode handler
     * @return  Number of images of this file if available;
     *          otherwise, -1 will be returned.
     */
    return enc ? enc->img_num : -1;
}

int uimgheif_enc_add_image(uimgheif_enc_t *enc, const uimg_t *img)
{
    /**
     * Add one image
     *
     * @param enc   The encode handler
     * @param img   The image to be added.
     *              Dimension (width and height) of this image must be
     *              the same as the previous added image except that
     *              this is the first image be added to the encoder, or
     *              this call will return an error and
     *              the image will not be added.
     * @return ZERO if success; or other values indicate error occurred.
     */
    struct heif_image *heif_img = NULL;

    bool succ = false;
    do
    {
        if(!enc)
            break;
        if( !img || !img->buf || img->w <= 0 || img->h <= 0 )
            break;

        if( enc->img_num && ( enc->width != img->w || enc->height != img->h ) )
            break;

        struct heif_error err = heif_image_create(
            img->w, img->h, heif_colorspace_RGB, heif_chroma_interleaved_RGB, &heif_img);
        if(err.code)
            break;

        err = heif_image_add_plane(
            heif_img, heif_channel_interleaved, img->w, img->h, 24);
        if(err.code)
            break;

        int heif_stride;
        uint8_t *heif_pixels = heif_image_get_plane(
            heif_img, heif_channel_interleaved, &heif_stride);
        if(!heif_pixels)
            break;

        uimg_begin_buf_access(img);
        for(int y = 0; y < img->h; ++y)
        {
            const uint32_t *src_row = uimg_get_row_buf_c(img, y);
            uint8_t *dst_row = heif_pixels + y * heif_stride;

            for(int x = 0; x < img->w; ++x)
            {
                dst_row[ x * 3 + 0 ] = UIMG_COLOUR_GET_R(src_row[x]);
                dst_row[ x * 3 + 1 ] = UIMG_COLOUR_GET_G(src_row[x]);
                dst_row[ x * 3 + 2 ] = UIMG_COLOUR_GET_B(src_row[x]);
            }
        }
        uimg_end_buf_access(img);

        err = heif_context_encode_image(
            enc->heif_ctx, heif_img, enc->heif_enc, NULL, NULL);
        if(err.code)
            break;

        ++enc->img_num;
        enc->width = img->w;
        enc->height = img->h;

        succ = true;
    } while(false);

    if(heif_img)
        heif_image_release(heif_img);

    return succ ? 0 : -1;
}

int uimgheif_enc_write_file(uimgheif_enc_t *enc, const char *filename)
{
    /**
     * Write the encoded data to a file
     *
     * @param enc       The encode handler
     * @param filename  Name of the file to be write
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !enc || !filename )
        return -1;
    if( !enc->img_num || enc->width <= 0 || enc->height <= 0 )
        return -1;

    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        static const size_t redundancy_size = 1024;
        size_t estimate_size =
            enc->width * enc->height * sizeof(uint32_t) + redundancy_size;
        if( !( buf = malloc(estimate_size) ) )
            break;

        size_t encode_size = uimgheif_enc_write_mem(enc, buf, estimate_size);
        if(!encode_size)
            break;

        if(!filebuf_save(filename, buf, encode_size))
            break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}

static
struct heif_error on_write_raw(
    struct heif_context *ctx,
    const void *data,
    size_t size,
    struct write_status *status)
{
    struct heif_error err = { .message = "" };

    if( size > status->rest_size )
    {
        err.code = heif_error_Encoding_error;
        err.subcode = heif_suberror_Cannot_write_output_data;
        err.message = "The output buffer is too small!";
        return err;
    }

    memcpy(status->buf, data, size);
    status->buf += size;
    status->rest_size -= size;
    status->fill_size += size;

    return err;
}

size_t uimgheif_enc_write_mem(uimgheif_enc_t *enc, void *buf, size_t size)
{
    /**
     * Write the encoded data to a memory buffer
     *
     * @param enc   The encode handler
     * @param buf   A buffer to be filled with the WebP format data
     * @param size  Size of the output buffer
     * @return  How many bytes be filled to the output buffer if success;
     *          otherwise, ZERO will be returned.
     */
    if( !enc || !buf || !size )
        return 0;

    struct heif_writer writer =
    {
        .writer_api_version = 1,
        .write =
            (struct heif_error(*)(struct heif_context*, const void*, size_t, void*))
            on_write_raw,
    };

    struct write_status status =
    {
        .buf = buf,
        .rest_size = size,
    };

    struct heif_error err = heif_context_write(enc->heif_ctx, &writer, &status);
    if(err.code)
        return 0;

    return status.fill_size;
}

int uimgheif_hlpr_save_file(
    const uimg_t *img, const char *filename, int flags)
{
    /**
     * @brief Save image to file
     *
     * This is a helper function to simplify the use case of
     * just need to write only one image to a file.
     *
     * @param img       The image object to be saved
     * @param filename  Name of the file to be write
     * @param flags     To specify the encode quality.
     *                  If the UIMGHEIF_QUALITY_LOSSLESS flag is set, then
     *                  the image will be encoded by the lossless compression;
     *                  otherwise, this value should be a quality factor with
     *                  range 0 ~ 100; or
     *                  just set to -1 to use the default value.
     *                  See ::uimgheif_quality for predefined values.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !img->buf || img->w <= 0 || img->h <= 0 )
        return -1;
    if(!filename)
        return -1;

    uimgheif_enc_t *enc = NULL;

    int err = -1;
    do
    {
        if( flags < 0 )
            flags = UIMGHEIF_QUALITY_MEDIUM;

        if( !( enc = uimgheif_enc_create(flags) ) )
            break;

        if(uimgheif_enc_add_image(enc, img))
            break;

        if(uimgheif_enc_write_file(enc, filename))
            break;

        err = 0;
    } while(false);

    if(enc)
        uimgheif_enc_release(enc);

    return err;
}
