#include <string.h>
#include <webp/decode.h>
#include <webp/encode.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgwebp.h"

size_t uimgwebp_encode(const uimg_t *img, void *buf, size_t size, int flags)
{
    /**
     * Encode to the WebP format data
     *
     * @param img   The image object to be encoded.
     * @param buf   The buffer to receive the encoded data.
     * @param size  Size of the output buffer.
     * @param flags To specify the encode quality.
     *              If the UIMGWEBP_QUALITY_LOSSLESS flag is set, then
     *              the image will be encoded by the lossless compression;
     *              otherwise, this value should be a quality factor with
     *              range 0 ~ 100; or
     *              just set to -1 to use the default value.
     *              See ::uimgwebp_quality for predefined values.
     * @return Total size of data has been fill to the buffer; or ZERO if failed.
     */
    if( !img || !img->buf || img->w <= 0 || img->h <= 0 )
        return 0;
    if( !buf || !size )
        return 0;

    if( flags < 0 )
        flags = UIMGWEBP_QUALITY_MEDIUM;

    int stride = img->w * sizeof(uint32_t);

    uint8_t *raw_data;
    size_t raw_size;
    if( flags & UIMGWEBP_QUALITY_LOSSLESS )
    {
        uimg_begin_buf_access(img);
        raw_size = WebPEncodeLosslessBGRA(
            (const uint8_t*) uimg_get_row_buf_c(img, 0),
            img->w,
            img->h,
            stride, &raw_data);
        uimg_end_buf_access(img);
    }
    else
    {
        uimg_begin_buf_access(img);
        raw_size = WebPEncodeBGRA(
            (const uint8_t*) uimg_get_row_buf_c(img, 0),
            img->w,
            img->h,
            stride,
            flags % 101, &raw_data);
        uimg_end_buf_access(img);
    }

    if(!raw_size)
        return 0;

    size_t fill_size = 0;
    if( size >= raw_size )
    {
        memcpy(buf, raw_data, raw_size);
        fill_size = raw_size;
    }

    WebPFree(raw_data);

    return fill_size;
}

size_t uimgwebp_decode(uimg_t *img, const void *data, size_t size)
{
    /**
     * Decode from the WebP format data
     *
     * @param img   The image object to receive the decoded result.
     * @param data  The WebP data to be decoded.
     * @param size  Size of the input data.
     * @return Total size of data has been read to decode; or ZERO if failed.
     */
    if( !img || !data || !size )
        return 0;

    int width, height;
    if(!WebPGetInfo(data, size, &width, &height))
        return 0;
    if( width <= 0 || height <= 0 )
        return 0;

    if(uimg_resize(img, width, height))
        return 0;

    int stride = width * sizeof(uint32_t);
    size_t img_bufsize = stride * height;

    uimg_begin_buf_access(img);
    uint8_t *dptr = WebPDecodeBGRAInto(
        data, size, (uint8_t*) uimg_get_row_buf(img, 0), img_bufsize, stride);
    uimg_end_buf_access(img);

    return dptr ? size : 0;
}

int uimgwebp_save_file(const uimg_t *img, const char *filename, int flags)
{
    /**
     * Save image to file
     *
     * @param img       The image object to be saved.
     * @param filename  Name of the file.
     * @param flags     To specify the encode quality.
     *                  If the UIMGWEBP_QUALITY_LOSSLESS flag is set, then
     *                  the image will be encoded by the lossless compression;
     *                  otherwise, this value should be a quality factor with
     *                  range 0 ~ 100; or
     *                  just set to -1 to use the default value.
     *                  See ::uimgwebp_quality for predefined values.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename )
        return false;

    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if(!img->buf)
            break;

        static const size_t redundancy_size = 1024;
        size_t estimate_size = img->w * img->h * sizeof(uint32_t) + redundancy_size;
        if( !( buf = malloc(estimate_size) ) )
            break;

        size_t encode_size = uimgwebp_encode(img, buf, estimate_size, flags);
        if(!encode_size)
            break;

        if(!filebuf_save(filename, buf, encode_size))
            break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}

int uimgwebp_load_file(uimg_t *img, const char *filename)
{
    /**
     * Load image from file
     *
     * @param img       The image object to be loaded.
     * @param filename  Name of the file.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename )
        return -1;

    size_t size = 0;
    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if( !( buf = filebuf_load(filename, &size) ) )
            break;
        if(!uimgwebp_decode(img, buf, size))
            break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}
