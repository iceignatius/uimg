#include <SDL2/SDL.h>
#include "uimgcolour.h"
#include "uimgdrv.h"

unsigned uimgdrv_get_magic(void)
{
    return UIMG_DRVID_SDL;
}

struct uimg* uimgdrv_alloc_imgslot(void)
{
    return malloc(sizeof(struct uimg));
}

void uimgdrv_free_imgslot(struct uimg *slot)
{
    free(slot);
}

void* uimgdrv_alloc_imgbuf(unsigned w, unsigned h)
{
#if SDL_VERSION_ATLEAST(2, 0, 5)
    return SDL_CreateRGBSurfaceWithFormat(0, w, h, 32, SDL_PIXELFORMAT_ARGB8888);
#else
    return SDL_CreateRGBSurface(
        0,
        w,
        h,
        32,
        UIMG_MAKE_COLOUR(0, 0, 255, 0),
        UIMG_MAKE_COLOUR(0, 255, 0, 0),
        UIMG_MAKE_COLOUR(255, 0, 0, 0),
        UIMG_MAKE_COLOUR(0, 0, 0, 255));
#endif
}

void* uimgdrv_clone_imgbuf(void *drvbuf, unsigned w, unsigned h)
{
    SDL_Surface *dst = uimgdrv_alloc_imgbuf(w, h);
    if(dst)
    {
        SDL_Surface *src = drvbuf;
        memcpy(dst->pixels, src->pixels, h * dst->pitch);
    }

    return dst;
}

void uimgdrv_free_imgbuf(void *drvbuf)
{
    SDL_FreeSurface(drvbuf);
}

int uimgdrv_begin_buf_access(void *drvbuf)
{
    return SDL_MUSTLOCK((SDL_Surface*)drvbuf) ? SDL_LockSurface(drvbuf) : 0;
}

int uimgdrv_end_buf_access(void *drvbuf)
{
    if(SDL_MUSTLOCK((SDL_Surface*)drvbuf))
        SDL_UnlockSurface(drvbuf);

    return 0;
}

uint32_t* uimgdrv_get_row_buf(void *drvbuf, unsigned w, unsigned h, unsigned y)
{
    SDL_Surface *surf = drvbuf;
    return (uint32_t*)( (uint8_t*) surf->pixels + surf->pitch * y );
}

int uimgdrv_fill(
    void *drvbuf, unsigned w, unsigned h, uint32_t val, const struct uimg_rect *rect)
{
    SDL_Rect sdl_rect = {0};
    if(rect)
    {
        sdl_rect.x = rect->x;
        sdl_rect.y = rect->y;
        sdl_rect.w = rect->w;
        sdl_rect.h = rect->h;
    }

    return SDL_FillRect(drvbuf, rect ? &sdl_rect : NULL, val);
}

static
void flip_h(SDL_Surface *dst, SDL_Surface *src, unsigned w, unsigned h)
{
    uint32_t *dstrow = dst->pixels;
    uint32_t *srcrow = src->pixels;

    for(unsigned y = 0; y < h; ++y)
    {
        for(unsigned x = 0; x < w; ++x)
            dstrow[x] = srcrow[ w - x - 1 ];

        dstrow = (uint32_t*)( (uint8_t*) dstrow + dst->pitch );
        srcrow = (uint32_t*)( (uint8_t*) srcrow + src->pitch );
    }
}

static
void flip_v(SDL_Surface *dst, SDL_Surface *src, unsigned w, unsigned h)
{
    uint32_t *dstrow = dst->pixels;
    uint32_t *srcrow = src->pixels + ( h - 1 ) * src->pitch;

    for(unsigned y = 0; y < h; ++y)
    {
        memcpy(dstrow, srcrow, src->pitch);

        dstrow = (uint32_t*)( (uint8_t*) dstrow + dst->pitch );
        srcrow = (uint32_t*)( (uint8_t*) srcrow - src->pitch );
    }
}

int uimgdrv_flip(void *dst_buf, void *src_buf, unsigned w, unsigned h, int dir)
{
    int err = 0;
    switch(dir)
    {
    case UIMG_FLIP_H:
        flip_h(dst_buf, src_buf, w, h);
        break;

    case UIMG_FLIP_V:
        flip_v(dst_buf, src_buf, w, h);
        break;

    default:
        err = -1;
        break;
    }

    return err;
}

static
uint32_t get_pixel(SDL_Surface *surf, unsigned x, unsigned y)
{
    uint32_t *row = (uint32_t*)( (uint8_t*) surf->pixels + surf->pitch * y );
    return row[x];
}

static
void rotate_180(SDL_Surface *dst, SDL_Surface *src, unsigned src_w, unsigned src_h)
{
    unsigned dst_w = src_w;
    unsigned dst_h = src_h;

    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        uint32_t *dstrow =
            (uint32_t*)( (uint8_t*) dst->pixels + dst->pitch * dst_y );

        unsigned src_y = dst_h - dst_y - 1;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            unsigned src_x = dst_w - dst_x - 1;
            dstrow[dst_x] = get_pixel(src, src_x, src_y);
        }
    }
}

static
void rotate_90(SDL_Surface *dst, SDL_Surface *src, unsigned src_w, unsigned src_h)
{
    unsigned dst_w = src_h;
    unsigned dst_h = src_w;

    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        uint32_t *dstrow =
            (uint32_t*)( (uint8_t*) dst->pixels + dst->pitch * dst_y );

        unsigned src_x = dst_y;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            unsigned src_y = dst_w - dst_x - 1;
            dstrow[dst_x] = get_pixel(src, src_x, src_y);
        }
    }
}

static
void rotate_270(SDL_Surface *dst, SDL_Surface *src, unsigned src_w, unsigned src_h)
{
    unsigned dst_w = src_h;
    unsigned dst_h = src_w;

    for(unsigned dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        uint32_t *dstrow =
            (uint32_t*)( (uint8_t*) dst->pixels + dst->pitch * dst_y );

        unsigned src_x = dst_h - dst_y - 1;
        for(unsigned dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            unsigned src_y = dst_x;
            dstrow[dst_x] = get_pixel(src, src_x, src_y);
        }
    }
}

int uimgdrv_rotate(
    void *dst_buf, void *src_buf, unsigned src_w, unsigned src_h, int ang)
{
    int err = 0;
    switch(ang)
    {
    case UIMG_ROTATE_0:
        break;

    case UIMG_ROTATE_90:
        rotate_90(dst_buf, src_buf, src_w, src_h);
        break;

    case UIMG_ROTATE_180:
        rotate_180(dst_buf, src_buf, src_w, src_h);
        break;

    case UIMG_ROTATE_270:
        rotate_270(dst_buf, src_buf, src_w, src_h);
        break;

    default:
        err = -1;
        break;
    }

    return err;
}

int uimgdrv_stretch(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    int scale_mode)
{
    int err;

    SDL_BlendMode old_blend_mode;
    if(( err = SDL_GetSurfaceBlendMode(src_buf, &old_blend_mode) ))
        return err;

    if(( err = SDL_SetSurfaceBlendMode(src_buf, SDL_BLENDMODE_NONE) ))
        return err;

    err = SDL_BlitScaled(src_buf, NULL, dst_buf, NULL);

    SDL_SetSurfaceBlendMode(src_buf, old_blend_mode);

    return err;
}

int uimgdrv_blend(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect,
    int blend_mode)
{
    int err;

    SDL_BlendMode sdl_blend_mode;
    switch(blend_mode)
    {
    case UIMG_BLEND_OVERLAY:
        sdl_blend_mode = SDL_BLENDMODE_NONE;
        break;

    case UIMG_BLEND_ALPHA:
        sdl_blend_mode = SDL_BLENDMODE_BLEND;
        break;

    default:
        return -1;
    }

    SDL_BlendMode old_blend_mode;
    if(( err = SDL_GetSurfaceBlendMode(src_buf, &old_blend_mode) ))
        return err;

    if(( err = SDL_SetSurfaceBlendMode(src_buf, sdl_blend_mode) ))
        return err;

    SDL_Rect sdl_src_rect =
    {
        .x = src_rect->x,
        .y = src_rect->y,
        .w = src_rect->w,
        .h = src_rect->h,
    };

    SDL_Rect sdl_dst_rect =
    {
        .x = dst_rect->x,
        .y = dst_rect->y,
        .w = dst_rect->w,
        .h = dst_rect->h,
    };

    err = SDL_BlitSurface(src_buf, &sdl_src_rect, dst_buf, &sdl_dst_rect);

    SDL_SetSurfaceBlendMode(src_buf, old_blend_mode);

    return err;
}
