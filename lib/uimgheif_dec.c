#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <libheif/heif.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgheif_dec.h"

struct read_status
{
    uint8_t *raw;
    size_t raw_size;
    size_t read_pos;
    size_t max_pos;
};

struct uimgheif_dec
{
    struct heif_context *heif_ctx;
    unsigned img_num;
    unsigned id_num;
    heif_item_id *id_list;
    unsigned primary_id_idx;

    struct read_status file;
};

static
int64_t on_get_position(struct read_status *status)
{
    return status->read_pos;
}

static
int on_read(void *buf, size_t size, struct read_status *status)
{
    if( status->read_pos + size > status->raw_size )
        return -1;

    memcpy(buf, &status->raw[status->read_pos], size);
    status->read_pos += size;

    if( status->max_pos < status->read_pos )
        status->max_pos = status->read_pos;

    return 0;
}

static
int on_seek(int64_t pos, struct read_status *status)
{
    if( pos > status->raw_size )
        return -1;

    status->read_pos = pos;
    return 0;
}

enum heif_reader_grow_status on_wait_for_file_size(
    int64_t target_size, struct read_status *status)
{
    return target_size <= status->raw_size ?
        heif_reader_grow_status_size_reached :
        heif_reader_grow_status_size_beyond_eof;
}

static struct heif_reader reader_ops =
{
    .reader_api_version = 1,
    .get_position = (int64_t(*)(void*)) on_get_position,
    .read = (int(*)(void*, size_t, void*)) on_read,
    .seek = (int(*)(int64_t, void*)) on_seek,
    .wait_for_file_size =
        (enum heif_reader_grow_status(*)(int64_t, void*)) on_wait_for_file_size,
};

uimgheif_dec_t* uimgheif_dec_open_file(const char *filename)
{
    /**
     * Create a decode handler by parse a file
     *
     * @param filename Name of the file to be parsed
     * @return  The created decode handler on success;
     *          otherwise, NULL will be returned.
     */
    if(!filename)
        return NULL;

    size_t size;
    void *buf = filebuf_load(filename, &size);
    if(!buf)
        return NULL;

    uimgheif_dec_t *dec = uimgheif_dec_open_mem(buf, size, NULL);
    free(buf);

    return dec;
}

uimgheif_dec_t* uimgheif_dec_open_mem(
    const void *raw, size_t raw_size, size_t *read_size)
{
    /**
     * Create a decode handler by parse data from memory
     *
     * @param raw       The raw WebP format data to be parsed
     * @param raw_size  Size of the input data
     * @param read_size Return the size of data be read from the input data.
     *                  This parameter can be NULL if not needed.
     * @return  The created decode handler on success;
     *          otherwise, NULL will be returned.
     */
    uimgheif_dec_t *dec = NULL;

    bool succ = false;
    do
    {
        if( !raw || !raw_size )
            break;

        if( !( dec = calloc(1, sizeof(*dec)) ) )
            break;

        if( !( dec->heif_ctx = heif_context_alloc() ) )
            break;

        if( !( dec->file.raw = malloc(raw_size) ) )
            break;
        memcpy(dec->file.raw, raw, raw_size);
        dec->file.raw_size = raw_size;

        struct heif_error err = heif_context_read_from_reader(
            dec->heif_ctx, &reader_ops, &dec->file, NULL);
        if(err.code)
            break;

        int img_num = heif_context_get_number_of_top_level_images(dec->heif_ctx);
        if( img_num <= 0 )
            break;

        dec->img_num = img_num;
        dec->id_list = malloc( img_num * sizeof(dec->id_list[0]) );
        if(!dec->id_list)
            break;

        int n = heif_context_get_list_of_top_level_image_IDs(
            dec->heif_ctx, dec->id_list, img_num);
        if( n != img_num )
            break;

        heif_item_id primary_id;
        err = heif_context_get_primary_image_ID(dec->heif_ctx, &primary_id);
        if(err.code)
            break;

        for(int i = 0; i < img_num; ++i)
        {
            if( dec->id_list[i] == primary_id )
                dec->primary_id_idx = i;
        }

        if( dec->file.max_pos < dec->file.raw_size )
        {
            uint8_t *buf = realloc(dec->file.raw, dec->file.max_pos);
            if(!buf)
                break;

            dec->file.raw = buf;
            dec->file.raw_size = dec->file.max_pos;
        }

        if(read_size)
            *read_size = dec->file.raw_size;

        succ = true;
    } while(false);

    if( !succ && dec )
    {
        uimgheif_dec_close(dec);
        dec = NULL;
    }

    return dec;
}

void uimgheif_dec_close(uimgheif_dec_t *dec)
{
    /**
     * End use of the decode handler and release its resources
     *
     * @param dec The decode handler
     */
    if(dec)
    {
        if(dec->file.raw)
            free(dec->file.raw);

        if(dec->id_list)
            free(dec->id_list);

        if(dec->heif_ctx)
            heif_context_free(dec->heif_ctx);

        free(dec);
    }
}

int uimgheif_dec_count_image(const uimgheif_dec_t *dec)
{
    /**
     * Count images
     *
     * @param dec The decode handler
     * @return  Number of images in this file if available;
     *          otherwise, -1 will be returned.
     */
    return dec ? dec->img_num : -1;
}

int uimgheif_dec_get_primary_idx(const uimgheif_dec_t *dec)
{
    /**
     * Get index of the primary image
     *
     * @param dec The decode handler
     * @return  Index of the primary image if available;
     *          otherwise, -1 will be returned.
     */
    return dec && dec->id_list ? dec->primary_id_idx : -1;
}

int uimgheif_dec_get_image(uimgheif_dec_t *dec, int idx, uimg_t *img)
{
    /**
     * Read image
     *
     * @param dec   The decode handler
     * @param idx   Index of the image to get
     * @param img   Return the read out image
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !dec || idx < 0 || idx >= dec->img_num || !img )
        return -1;

    struct heif_image_handle *handle = NULL;
    struct heif_image *heif_img = NULL;

    bool succ = false;
    do
    {
        struct heif_error err = heif_context_get_image_handle(
            dec->heif_ctx, dec->id_list[idx], &handle);
        if(err.code)
            break;

        err = heif_decode_image(
            handle, &heif_img, heif_colorspace_RGB, heif_chroma_interleaved_RGB, NULL);
        if(err.code)
            break;

        int width = heif_image_get_width(heif_img, heif_channel_interleaved);
        int height = heif_image_get_height(heif_img, heif_channel_interleaved);
        if( width <= 0 || height <= 0 )
            break;

        if(uimg_resize(img, width, height))
            break;

        int stride;
        const uint8_t *pixels = heif_image_get_plane_readonly(
            heif_img, heif_channel_interleaved, &stride);
        if(!pixels)
            break;

        uimg_begin_buf_access(img);
        for(int y = 0; y < height; ++y)
        {
            const uint8_t *src_row = pixels + stride * y;
            uint32_t *dst_row = uimg_get_row_buf(img, y);
            for(int x = 0; x < width; ++x)
            {
                int src_off = x * 3;
                unsigned b = src_row[ src_off + 2 ];
                unsigned g = src_row[ src_off + 1 ];
                unsigned r = src_row[ src_off + 0 ];
                dst_row[x] = UIMG_MAKE_COLOUR(b, g, r, 255);
            }
        }
        uimg_end_buf_access(img);

        succ = true;
    } while(false);

    if(heif_img)
        heif_image_release(heif_img);
    if(handle)
        heif_image_handle_release(handle);

    return succ ? 0 : -1;
}

int uimgheif_hlpr_load_file(uimg_t *img, const char *filename)
{
    /**
     * @brief Load image from file
     *
     * This is a helper function to simplify the use case of
     * just need to read only one image from a file.
     *
     * @param img       The image object to be loaded
     * @param filename  Name of the file to be parsed
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename )
        return -1;

    uimgheif_dec_t *dec = NULL;

    int err = -1;
    do
    {
        if( !( dec = uimgheif_dec_open_file(filename) ) )
            break;

        if(uimgheif_dec_get_image(dec, uimgheif_dec_get_primary_idx(dec), img))
            break;

        err = 0;
    } while(false);

    if(dec)
        uimgheif_dec_close(dec);

    return err;
}
