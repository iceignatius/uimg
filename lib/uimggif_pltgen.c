#define _GNU_SOURCE
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>
#include <gif_lib.h>
#include "uimggif_pltgen.h"

struct statnode
{
    uint32_t colour;
    unsigned count;
};

struct uimggif_pltgen
{
    struct uimggif_palette inf;
    int id;

    void *stat_map;
    unsigned stat_num;
    bool stat_dirty;

    GifColorType colour_tbl[256];
    unsigned colour_num;
    unsigned gif_colour_num;
    int transparent_idx;
};

static
int statmap_compare(const struct statnode *l, const struct statnode *r)
{
    return (long long) l->colour - (long long) r->colour;
}

static
struct statnode* statmap_create_node(uint32_t c)
{
    struct statnode *node = malloc(sizeof(*node));
    if(node)
    {
        node->colour = c;
        node->count = 1;
    }

    return node;
}

static
void statmap_release_node(struct statnode *node)
{
    if(node)
        free(node);
}

static
int statmap_add_colour(void **map, unsigned *num, uint32_t c)
{
    struct statnode *node = NULL;

    int err = 0;
    do
    {
        if(!( node = statmap_create_node(c) ))
            break;

        struct statnode **iter =
            tsearch(node, map, (int(*)(const void*, const void*)) statmap_compare);
        if(!iter)
            break;

        if( *iter == node )
        {
            ++(*num);
            node = NULL;
        }
        else
        {
            ++ (*iter)->count;
        }

        err = 0;
    } while(false);

    if(node)
        statmap_release_node(node);

    return err;
}

uimggif_pltgen_t* uimggif_pltgen_create(void)
{
    /**
     * Create a palette generator object
     *
     * @return The created generator on success; or NULL if error occurred.
     */
    uimggif_pltgen_t *inst = malloc(sizeof(*inst));
    if(!inst)
        return NULL;

    inst->id = uimggif_palette_acquire_id(&inst->inf);
    if( inst->id < 0 )
    {
        free(inst);
        return NULL;
    }

    inst->inf.get_table = uimggif_pltgen_get_table;
    inst->inf.count_colours = uimggif_pltgen_count_colours;
    inst->inf.calc_index = uimggif_pltgen_calc_index;
    inst->inf.get_transparent = uimggif_pltgen_get_transparent;

    inst->stat_map = NULL;
    inst->stat_num = 0;

    memset(inst->colour_tbl, 0, sizeof(inst->colour_tbl));
    inst->colour_num = 0;
    inst->gif_colour_num = 0;
    inst->transparent_idx = -1;

    return inst;
}

void uimggif_pltgen_release(uimggif_pltgen_t *self)
{
    /**
     * Release a palette generator object
     *
     * @param self The palette generator object
     */
    if(!self)
        return;

    uimggif_pltgen_clear_parser(self);

    if( self->id >= 0 )
        uimggif_palette_release_id(self->id);

    free(self);
}

void uimggif_pltgen_clear_parser(uimggif_pltgen_t *self)
{
    /**
     * Reset parser state and release its resources
     *
     * @param self The palette generator object
     */
    if(self->stat_map)
    {
        tdestroy(self->stat_map, (void(*)(void*)) statmap_release_node);
        self->stat_map = NULL;
        self->stat_num = 0;
    }
}

int uimggif_pltgen_parse_image(uimggif_pltgen_t *self, const uimg_t *img)
{
    /**
     * Parse one more image
     *
     * @param self  The palette generator object
     * @param img   The image to be parsed
     * @return ZERO if success; or -1 otherwise!
     */
    if(!img)
        return -1;

    if(uimg_begin_buf_access(img))
        return -1;

    int w = uimg_get_width(img);
    int h = uimg_get_height(img);
    int err = 0;
    for(int y = 0; y < h && !err; ++y)
    {
        const uint32_t *row = uimg_get_row_buf_c(img, y);
        for(int x = 0; x < w && !err; ++x)
        {
            unsigned a = UIMG_COLOUR_GET_A(row[x]);
            uint32_t c =
                a == 255 ? row[x] :
                a > 127 ? UIMG_COLOUR_SET_A(row[x], 255) :
                UIMG_COLOUR_BLANK;
            err = statmap_add_colour(&self->stat_map, &self->stat_num, c);
        }
    }

    uimg_end_buf_access(img);

    self->stat_dirty = true;
    return err;
}

static
void on_walk_node(
    const struct statnode **iter, VISIT which, struct statnode **arrpos)
{
    if( which != postorder && which != leaf )
        return;

    const struct statnode *node = *iter;
    **arrpos = *node;
    (*arrpos)++;
}

static
int compare_usecount(const struct statnode *l, const struct statnode *r)
{
    return (long) r->count - (long) l->count;
}

static
unsigned ceil_pow2(unsigned x)
{
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    ++x;

    return x;
}

int uimggif_pltgen_update_palette(uimggif_pltgen_t *self)
{
    /**
     * Calculate and update the palette at current parser state
     *
     * @param self The palette generator object
     * @return ZERO if success; or -1 otherwise!
     */
    struct statnode *stat_arr = NULL;

    int err = -1;
    do
    {
        if(!self->stat_num)
            break;

        if(!( stat_arr = malloc( sizeof(stat_arr[0]) * self->stat_num ) ))
            break;

        struct statnode *arrpos = stat_arr;
        twalk_r(
            self->stat_map,
            (void(*)(const void*, VISIT, void*)) on_walk_node,
            &arrpos);
        qsort(
            stat_arr, self->stat_num, sizeof(stat_arr[0]),
            (int(*)(const void*, const void*)) compare_usecount);

        memset(self->colour_tbl, 0, sizeof(self->colour_tbl));
        self->colour_num = self->stat_num <= 256 ? self->stat_num : 256;
        self->gif_colour_num = ceil_pow2(self->colour_num);
        self->transparent_idx = -1;

        for(unsigned i = 0; i < self->colour_num; ++i)
        {
            self->colour_tbl[i].Red = UIMG_COLOUR_GET_R(stat_arr[i].colour);
            self->colour_tbl[i].Green = UIMG_COLOUR_GET_G(stat_arr[i].colour);
            self->colour_tbl[i].Blue = UIMG_COLOUR_GET_B(stat_arr[i].colour);

            if( self->transparent_idx < 0 &&
                UIMG_COLOUR_GET_A(stat_arr[i].colour) == 0 )
            {
                self->transparent_idx = i;
            }
        }

        self->stat_dirty = false;

        err = 0;
    } while(false);

    if(stat_arr)
        free(stat_arr);

    return err;
}

int uimggif_pltgen_get_id(uimggif_pltgen_t *self)
{
    /**
     * Get the palette ID which can be used as the argument of
     * uimggif_enc_open_file(), uimggif_enc_open_mem(), and
     * uimggif_hlpr_save_file(), etc...
     *
     * @param self The palette generator object
     * @return On success, a positive number which is the palette ID is returned;
     *  or -1 will be returned when error occurred.
     *
     * @remarks
     * This function may recalculate and update palette if its state has changed
     */
    if( self->stat_dirty && uimggif_pltgen_update_palette(self) )
        return -1;

    return self->id;
}

const struct GifColorType* uimggif_pltgen_get_table(struct uimggif_palette *host)
{
    uimggif_pltgen_t *self = (uimggif_pltgen_t*) host;
    return self->colour_tbl;
}

unsigned uimggif_pltgen_count_colours(struct uimggif_palette *host)
{
    uimggif_pltgen_t *self = (uimggif_pltgen_t*) host;
    return self->gif_colour_num;
}

static
int calc_fast_abs(int x)
{
    int mask = x >> ( sizeof(int) * CHAR_BIT - 1 );
    return ( ( x + mask ) ^ mask );
}

uint8_t uimggif_pltgen_calc_index(struct uimggif_palette *host, uint32_t colour)
{
    uimggif_pltgen_t *self = (uimggif_pltgen_t*) host;

    unsigned a = UIMG_COLOUR_GET_A(colour);
    if( a <= 127 && self->transparent_idx >= 0 )
        return self->transparent_idx;

    int r = UIMG_COLOUR_GET_R(colour);
    int g = UIMG_COLOUR_GET_G(colour);
    int b = UIMG_COLOUR_GET_B(colour);

    int min_diff = INT_MAX;
    int min_index = -1;

    for(int i = 0; i < self->colour_num; ++i)
    {
        if( i == self->transparent_idx )
            continue;

        int diff =
            calc_fast_abs( r - (int) self->colour_tbl[i].Red ) +
            calc_fast_abs( g - (int) self->colour_tbl[i].Green ) +
            calc_fast_abs( b - (int) self->colour_tbl[i].Blue );
        if( min_diff > diff )
        {
            min_diff = diff;
            min_index = i;
        }
    }

    return min_index;
}

int uimggif_pltgen_get_transparent(struct uimggif_palette *host)
{
    uimggif_pltgen_t *self = (uimggif_pltgen_t*) host;
    return self->transparent_idx >= 0 ?
        self->transparent_idx : NO_TRANSPARENT_COLOR;
}
