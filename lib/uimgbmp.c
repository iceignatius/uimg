#include <assert.h>
#include <endian.h>
#include <string.h>
#include "static_assert.h"
#include "bits.h"
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgbmp.h"

static const char bmp_signature[2] = { 'B','M' };

#pragma pack(push,1)
struct filehdr
{
    char        signature[2];
    uint32_t    filesize;
    uint8_t     reserved[4];
    uint32_t    imgoffset;
};
#pragma pack(pop)
STATIC_ASSERT( sizeof(struct filehdr) == 14 );

#pragma pack(push,1)
struct infohdr
{
    uint32_t    hdrsize;            // Size of this header.
    int32_t     width;
    int32_t     height;
    uint16_t    planes;             // Fixed to 1.
    uint16_t    pixdepth;           // 1, 4, 8, 16, 24, 32.
    uint32_t    compression;        // We support 0 (uncompressed) only.
    uint32_t    imgsize;
    int32_t     reso_h;             // Horizontal resolution (pixels per meter), and can be zero.
    int32_t     reso_v;             // Vertical resolution (pixels per meter), and can be zero.
    uint32_t    palette_colours;    // Items count of the palette, and can be zero to assume that
                                    // count number to be 2^pixdepth.
    uint32_t    important_colours;  // Usually not used and be set to zero.
};
#pragma pack(pop)
STATIC_ASSERT( sizeof(struct infohdr) == 40 );

#pragma pack(push,1)
struct bmppix24
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
#pragma pack(pop)
STATIC_ASSERT( sizeof(struct bmppix24) == 3 );

static const uint32_t palette_bw[2] =
{
    UIMG_MAKE_COLOUR(0x00, 0x00, 0x00, 0xFF),
    UIMG_MAKE_COLOUR(0xFF, 0xFF, 0xFF, 0xFF),
};

static const uint32_t palette_grey[256] =
{
    UIMG_MAKE_COLOUR(0x00, 0x00, 0x00, 0xFF),
    UIMG_MAKE_COLOUR(0x01, 0x01, 0x01, 0xFF),
    UIMG_MAKE_COLOUR(0x02, 0x02, 0x02, 0xFF),
    UIMG_MAKE_COLOUR(0x03, 0x03, 0x03, 0xFF),
    UIMG_MAKE_COLOUR(0x04, 0x04, 0x04, 0xFF),
    UIMG_MAKE_COLOUR(0x05, 0x05, 0x05, 0xFF),
    UIMG_MAKE_COLOUR(0x06, 0x06, 0x06, 0xFF),
    UIMG_MAKE_COLOUR(0x07, 0x07, 0x07, 0xFF),
    UIMG_MAKE_COLOUR(0x08, 0x08, 0x08, 0xFF),
    UIMG_MAKE_COLOUR(0x09, 0x09, 0x09, 0xFF),
    UIMG_MAKE_COLOUR(0x0A, 0x0A, 0x0A, 0xFF),
    UIMG_MAKE_COLOUR(0x0B, 0x0B, 0x0B, 0xFF),
    UIMG_MAKE_COLOUR(0x0C, 0x0C, 0x0C, 0xFF),
    UIMG_MAKE_COLOUR(0x0D, 0x0D, 0x0D, 0xFF),
    UIMG_MAKE_COLOUR(0x0E, 0x0E, 0x0E, 0xFF),
    UIMG_MAKE_COLOUR(0x0F, 0x0F, 0x0F, 0xFF),
    UIMG_MAKE_COLOUR(0x10, 0x10, 0x10, 0xFF),
    UIMG_MAKE_COLOUR(0x11, 0x11, 0x11, 0xFF),
    UIMG_MAKE_COLOUR(0x12, 0x12, 0x12, 0xFF),
    UIMG_MAKE_COLOUR(0x13, 0x13, 0x13, 0xFF),
    UIMG_MAKE_COLOUR(0x14, 0x14, 0x14, 0xFF),
    UIMG_MAKE_COLOUR(0x15, 0x15, 0x15, 0xFF),
    UIMG_MAKE_COLOUR(0x16, 0x16, 0x16, 0xFF),
    UIMG_MAKE_COLOUR(0x17, 0x17, 0x17, 0xFF),
    UIMG_MAKE_COLOUR(0x18, 0x18, 0x18, 0xFF),
    UIMG_MAKE_COLOUR(0x19, 0x19, 0x19, 0xFF),
    UIMG_MAKE_COLOUR(0x1A, 0x1A, 0x1A, 0xFF),
    UIMG_MAKE_COLOUR(0x1B, 0x1B, 0x1B, 0xFF),
    UIMG_MAKE_COLOUR(0x1C, 0x1C, 0x1C, 0xFF),
    UIMG_MAKE_COLOUR(0x1D, 0x1D, 0x1D, 0xFF),
    UIMG_MAKE_COLOUR(0x1E, 0x1E, 0x1E, 0xFF),
    UIMG_MAKE_COLOUR(0x1F, 0x1F, 0x1F, 0xFF),
    UIMG_MAKE_COLOUR(0x20, 0x20, 0x20, 0xFF),
    UIMG_MAKE_COLOUR(0x21, 0x21, 0x21, 0xFF),
    UIMG_MAKE_COLOUR(0x22, 0x22, 0x22, 0xFF),
    UIMG_MAKE_COLOUR(0x23, 0x23, 0x23, 0xFF),
    UIMG_MAKE_COLOUR(0x24, 0x24, 0x24, 0xFF),
    UIMG_MAKE_COLOUR(0x25, 0x25, 0x25, 0xFF),
    UIMG_MAKE_COLOUR(0x26, 0x26, 0x26, 0xFF),
    UIMG_MAKE_COLOUR(0x27, 0x27, 0x27, 0xFF),
    UIMG_MAKE_COLOUR(0x28, 0x28, 0x28, 0xFF),
    UIMG_MAKE_COLOUR(0x29, 0x29, 0x29, 0xFF),
    UIMG_MAKE_COLOUR(0x2A, 0x2A, 0x2A, 0xFF),
    UIMG_MAKE_COLOUR(0x2B, 0x2B, 0x2B, 0xFF),
    UIMG_MAKE_COLOUR(0x2C, 0x2C, 0x2C, 0xFF),
    UIMG_MAKE_COLOUR(0x2D, 0x2D, 0x2D, 0xFF),
    UIMG_MAKE_COLOUR(0x2E, 0x2E, 0x2E, 0xFF),
    UIMG_MAKE_COLOUR(0x2F, 0x2F, 0x2F, 0xFF),
    UIMG_MAKE_COLOUR(0x30, 0x30, 0x30, 0xFF),
    UIMG_MAKE_COLOUR(0x31, 0x31, 0x31, 0xFF),
    UIMG_MAKE_COLOUR(0x32, 0x32, 0x32, 0xFF),
    UIMG_MAKE_COLOUR(0x33, 0x33, 0x33, 0xFF),
    UIMG_MAKE_COLOUR(0x34, 0x34, 0x34, 0xFF),
    UIMG_MAKE_COLOUR(0x35, 0x35, 0x35, 0xFF),
    UIMG_MAKE_COLOUR(0x36, 0x36, 0x36, 0xFF),
    UIMG_MAKE_COLOUR(0x37, 0x37, 0x37, 0xFF),
    UIMG_MAKE_COLOUR(0x38, 0x38, 0x38, 0xFF),
    UIMG_MAKE_COLOUR(0x39, 0x39, 0x39, 0xFF),
    UIMG_MAKE_COLOUR(0x3A, 0x3A, 0x3A, 0xFF),
    UIMG_MAKE_COLOUR(0x3B, 0x3B, 0x3B, 0xFF),
    UIMG_MAKE_COLOUR(0x3C, 0x3C, 0x3C, 0xFF),
    UIMG_MAKE_COLOUR(0x3D, 0x3D, 0x3D, 0xFF),
    UIMG_MAKE_COLOUR(0x3E, 0x3E, 0x3E, 0xFF),
    UIMG_MAKE_COLOUR(0x3F, 0x3F, 0x3F, 0xFF),
    UIMG_MAKE_COLOUR(0x40, 0x40, 0x40, 0xFF),
    UIMG_MAKE_COLOUR(0x41, 0x41, 0x41, 0xFF),
    UIMG_MAKE_COLOUR(0x42, 0x42, 0x42, 0xFF),
    UIMG_MAKE_COLOUR(0x43, 0x43, 0x43, 0xFF),
    UIMG_MAKE_COLOUR(0x44, 0x44, 0x44, 0xFF),
    UIMG_MAKE_COLOUR(0x45, 0x45, 0x45, 0xFF),
    UIMG_MAKE_COLOUR(0x46, 0x46, 0x46, 0xFF),
    UIMG_MAKE_COLOUR(0x47, 0x47, 0x47, 0xFF),
    UIMG_MAKE_COLOUR(0x48, 0x48, 0x48, 0xFF),
    UIMG_MAKE_COLOUR(0x49, 0x49, 0x49, 0xFF),
    UIMG_MAKE_COLOUR(0x4A, 0x4A, 0x4A, 0xFF),
    UIMG_MAKE_COLOUR(0x4B, 0x4B, 0x4B, 0xFF),
    UIMG_MAKE_COLOUR(0x4C, 0x4C, 0x4C, 0xFF),
    UIMG_MAKE_COLOUR(0x4D, 0x4D, 0x4D, 0xFF),
    UIMG_MAKE_COLOUR(0x4E, 0x4E, 0x4E, 0xFF),
    UIMG_MAKE_COLOUR(0x4F, 0x4F, 0x4F, 0xFF),
    UIMG_MAKE_COLOUR(0x50, 0x50, 0x50, 0xFF),
    UIMG_MAKE_COLOUR(0x51, 0x51, 0x51, 0xFF),
    UIMG_MAKE_COLOUR(0x52, 0x52, 0x52, 0xFF),
    UIMG_MAKE_COLOUR(0x53, 0x53, 0x53, 0xFF),
    UIMG_MAKE_COLOUR(0x54, 0x54, 0x54, 0xFF),
    UIMG_MAKE_COLOUR(0x55, 0x55, 0x55, 0xFF),
    UIMG_MAKE_COLOUR(0x56, 0x56, 0x56, 0xFF),
    UIMG_MAKE_COLOUR(0x57, 0x57, 0x57, 0xFF),
    UIMG_MAKE_COLOUR(0x58, 0x58, 0x58, 0xFF),
    UIMG_MAKE_COLOUR(0x59, 0x59, 0x59, 0xFF),
    UIMG_MAKE_COLOUR(0x5A, 0x5A, 0x5A, 0xFF),
    UIMG_MAKE_COLOUR(0x5B, 0x5B, 0x5B, 0xFF),
    UIMG_MAKE_COLOUR(0x5C, 0x5C, 0x5C, 0xFF),
    UIMG_MAKE_COLOUR(0x5D, 0x5D, 0x5D, 0xFF),
    UIMG_MAKE_COLOUR(0x5E, 0x5E, 0x5E, 0xFF),
    UIMG_MAKE_COLOUR(0x5F, 0x5F, 0x5F, 0xFF),
    UIMG_MAKE_COLOUR(0x60, 0x60, 0x60, 0xFF),
    UIMG_MAKE_COLOUR(0x61, 0x61, 0x61, 0xFF),
    UIMG_MAKE_COLOUR(0x62, 0x62, 0x62, 0xFF),
    UIMG_MAKE_COLOUR(0x63, 0x63, 0x63, 0xFF),
    UIMG_MAKE_COLOUR(0x64, 0x64, 0x64, 0xFF),
    UIMG_MAKE_COLOUR(0x65, 0x65, 0x65, 0xFF),
    UIMG_MAKE_COLOUR(0x66, 0x66, 0x66, 0xFF),
    UIMG_MAKE_COLOUR(0x67, 0x67, 0x67, 0xFF),
    UIMG_MAKE_COLOUR(0x68, 0x68, 0x68, 0xFF),
    UIMG_MAKE_COLOUR(0x69, 0x69, 0x69, 0xFF),
    UIMG_MAKE_COLOUR(0x6A, 0x6A, 0x6A, 0xFF),
    UIMG_MAKE_COLOUR(0x6B, 0x6B, 0x6B, 0xFF),
    UIMG_MAKE_COLOUR(0x6C, 0x6C, 0x6C, 0xFF),
    UIMG_MAKE_COLOUR(0x6D, 0x6D, 0x6D, 0xFF),
    UIMG_MAKE_COLOUR(0x6E, 0x6E, 0x6E, 0xFF),
    UIMG_MAKE_COLOUR(0x6F, 0x6F, 0x6F, 0xFF),
    UIMG_MAKE_COLOUR(0x70, 0x70, 0x70, 0xFF),
    UIMG_MAKE_COLOUR(0x71, 0x71, 0x71, 0xFF),
    UIMG_MAKE_COLOUR(0x72, 0x72, 0x72, 0xFF),
    UIMG_MAKE_COLOUR(0x73, 0x73, 0x73, 0xFF),
    UIMG_MAKE_COLOUR(0x74, 0x74, 0x74, 0xFF),
    UIMG_MAKE_COLOUR(0x75, 0x75, 0x75, 0xFF),
    UIMG_MAKE_COLOUR(0x76, 0x76, 0x76, 0xFF),
    UIMG_MAKE_COLOUR(0x77, 0x77, 0x77, 0xFF),
    UIMG_MAKE_COLOUR(0x78, 0x78, 0x78, 0xFF),
    UIMG_MAKE_COLOUR(0x79, 0x79, 0x79, 0xFF),
    UIMG_MAKE_COLOUR(0x7A, 0x7A, 0x7A, 0xFF),
    UIMG_MAKE_COLOUR(0x7B, 0x7B, 0x7B, 0xFF),
    UIMG_MAKE_COLOUR(0x7C, 0x7C, 0x7C, 0xFF),
    UIMG_MAKE_COLOUR(0x7D, 0x7D, 0x7D, 0xFF),
    UIMG_MAKE_COLOUR(0x7E, 0x7E, 0x7E, 0xFF),
    UIMG_MAKE_COLOUR(0x7F, 0x7F, 0x7F, 0xFF),
    UIMG_MAKE_COLOUR(0x80, 0x80, 0x80, 0xFF),
    UIMG_MAKE_COLOUR(0x81, 0x81, 0x81, 0xFF),
    UIMG_MAKE_COLOUR(0x82, 0x82, 0x82, 0xFF),
    UIMG_MAKE_COLOUR(0x83, 0x83, 0x83, 0xFF),
    UIMG_MAKE_COLOUR(0x84, 0x84, 0x84, 0xFF),
    UIMG_MAKE_COLOUR(0x85, 0x85, 0x85, 0xFF),
    UIMG_MAKE_COLOUR(0x86, 0x86, 0x86, 0xFF),
    UIMG_MAKE_COLOUR(0x87, 0x87, 0x87, 0xFF),
    UIMG_MAKE_COLOUR(0x88, 0x88, 0x88, 0xFF),
    UIMG_MAKE_COLOUR(0x89, 0x89, 0x89, 0xFF),
    UIMG_MAKE_COLOUR(0x8A, 0x8A, 0x8A, 0xFF),
    UIMG_MAKE_COLOUR(0x8B, 0x8B, 0x8B, 0xFF),
    UIMG_MAKE_COLOUR(0x8C, 0x8C, 0x8C, 0xFF),
    UIMG_MAKE_COLOUR(0x8D, 0x8D, 0x8D, 0xFF),
    UIMG_MAKE_COLOUR(0x8E, 0x8E, 0x8E, 0xFF),
    UIMG_MAKE_COLOUR(0x8F, 0x8F, 0x8F, 0xFF),
    UIMG_MAKE_COLOUR(0x90, 0x90, 0x90, 0xFF),
    UIMG_MAKE_COLOUR(0x91, 0x91, 0x91, 0xFF),
    UIMG_MAKE_COLOUR(0x92, 0x92, 0x92, 0xFF),
    UIMG_MAKE_COLOUR(0x93, 0x93, 0x93, 0xFF),
    UIMG_MAKE_COLOUR(0x94, 0x94, 0x94, 0xFF),
    UIMG_MAKE_COLOUR(0x95, 0x95, 0x95, 0xFF),
    UIMG_MAKE_COLOUR(0x96, 0x96, 0x96, 0xFF),
    UIMG_MAKE_COLOUR(0x97, 0x97, 0x97, 0xFF),
    UIMG_MAKE_COLOUR(0x98, 0x98, 0x98, 0xFF),
    UIMG_MAKE_COLOUR(0x99, 0x99, 0x99, 0xFF),
    UIMG_MAKE_COLOUR(0x9A, 0x9A, 0x9A, 0xFF),
    UIMG_MAKE_COLOUR(0x9B, 0x9B, 0x9B, 0xFF),
    UIMG_MAKE_COLOUR(0x9C, 0x9C, 0x9C, 0xFF),
    UIMG_MAKE_COLOUR(0x9D, 0x9D, 0x9D, 0xFF),
    UIMG_MAKE_COLOUR(0x9E, 0x9E, 0x9E, 0xFF),
    UIMG_MAKE_COLOUR(0x9F, 0x9F, 0x9F, 0xFF),
    UIMG_MAKE_COLOUR(0xA0, 0xA0, 0xA0, 0xFF),
    UIMG_MAKE_COLOUR(0xA1, 0xA1, 0xA1, 0xFF),
    UIMG_MAKE_COLOUR(0xA2, 0xA2, 0xA2, 0xFF),
    UIMG_MAKE_COLOUR(0xA3, 0xA3, 0xA3, 0xFF),
    UIMG_MAKE_COLOUR(0xA4, 0xA4, 0xA4, 0xFF),
    UIMG_MAKE_COLOUR(0xA5, 0xA5, 0xA5, 0xFF),
    UIMG_MAKE_COLOUR(0xA6, 0xA6, 0xA6, 0xFF),
    UIMG_MAKE_COLOUR(0xA7, 0xA7, 0xA7, 0xFF),
    UIMG_MAKE_COLOUR(0xA8, 0xA8, 0xA8, 0xFF),
    UIMG_MAKE_COLOUR(0xA9, 0xA9, 0xA9, 0xFF),
    UIMG_MAKE_COLOUR(0xAA, 0xAA, 0xAA, 0xFF),
    UIMG_MAKE_COLOUR(0xAB, 0xAB, 0xAB, 0xFF),
    UIMG_MAKE_COLOUR(0xAC, 0xAC, 0xAC, 0xFF),
    UIMG_MAKE_COLOUR(0xAD, 0xAD, 0xAD, 0xFF),
    UIMG_MAKE_COLOUR(0xAE, 0xAE, 0xAE, 0xFF),
    UIMG_MAKE_COLOUR(0xAF, 0xAF, 0xAF, 0xFF),
    UIMG_MAKE_COLOUR(0xB0, 0xB0, 0xB0, 0xFF),
    UIMG_MAKE_COLOUR(0xB1, 0xB1, 0xB1, 0xFF),
    UIMG_MAKE_COLOUR(0xB2, 0xB2, 0xB2, 0xFF),
    UIMG_MAKE_COLOUR(0xB3, 0xB3, 0xB3, 0xFF),
    UIMG_MAKE_COLOUR(0xB4, 0xB4, 0xB4, 0xFF),
    UIMG_MAKE_COLOUR(0xB5, 0xB5, 0xB5, 0xFF),
    UIMG_MAKE_COLOUR(0xB6, 0xB6, 0xB6, 0xFF),
    UIMG_MAKE_COLOUR(0xB7, 0xB7, 0xB7, 0xFF),
    UIMG_MAKE_COLOUR(0xB8, 0xB8, 0xB8, 0xFF),
    UIMG_MAKE_COLOUR(0xB9, 0xB9, 0xB9, 0xFF),
    UIMG_MAKE_COLOUR(0xBA, 0xBA, 0xBA, 0xFF),
    UIMG_MAKE_COLOUR(0xBB, 0xBB, 0xBB, 0xFF),
    UIMG_MAKE_COLOUR(0xBC, 0xBC, 0xBC, 0xFF),
    UIMG_MAKE_COLOUR(0xBD, 0xBD, 0xBD, 0xFF),
    UIMG_MAKE_COLOUR(0xBE, 0xBE, 0xBE, 0xFF),
    UIMG_MAKE_COLOUR(0xBF, 0xBF, 0xBF, 0xFF),
    UIMG_MAKE_COLOUR(0xC0, 0xC0, 0xC0, 0xFF),
    UIMG_MAKE_COLOUR(0xC1, 0xC1, 0xC1, 0xFF),
    UIMG_MAKE_COLOUR(0xC2, 0xC2, 0xC2, 0xFF),
    UIMG_MAKE_COLOUR(0xC3, 0xC3, 0xC3, 0xFF),
    UIMG_MAKE_COLOUR(0xC4, 0xC4, 0xC4, 0xFF),
    UIMG_MAKE_COLOUR(0xC5, 0xC5, 0xC5, 0xFF),
    UIMG_MAKE_COLOUR(0xC6, 0xC6, 0xC6, 0xFF),
    UIMG_MAKE_COLOUR(0xC7, 0xC7, 0xC7, 0xFF),
    UIMG_MAKE_COLOUR(0xC8, 0xC8, 0xC8, 0xFF),
    UIMG_MAKE_COLOUR(0xC9, 0xC9, 0xC9, 0xFF),
    UIMG_MAKE_COLOUR(0xCA, 0xCA, 0xCA, 0xFF),
    UIMG_MAKE_COLOUR(0xCB, 0xCB, 0xCB, 0xFF),
    UIMG_MAKE_COLOUR(0xCC, 0xCC, 0xCC, 0xFF),
    UIMG_MAKE_COLOUR(0xCD, 0xCD, 0xCD, 0xFF),
    UIMG_MAKE_COLOUR(0xCE, 0xCE, 0xCE, 0xFF),
    UIMG_MAKE_COLOUR(0xCF, 0xCF, 0xCF, 0xFF),
    UIMG_MAKE_COLOUR(0xD0, 0xD0, 0xD0, 0xFF),
    UIMG_MAKE_COLOUR(0xD1, 0xD1, 0xD1, 0xFF),
    UIMG_MAKE_COLOUR(0xD2, 0xD2, 0xD2, 0xFF),
    UIMG_MAKE_COLOUR(0xD3, 0xD3, 0xD3, 0xFF),
    UIMG_MAKE_COLOUR(0xD4, 0xD4, 0xD4, 0xFF),
    UIMG_MAKE_COLOUR(0xD5, 0xD5, 0xD5, 0xFF),
    UIMG_MAKE_COLOUR(0xD6, 0xD6, 0xD6, 0xFF),
    UIMG_MAKE_COLOUR(0xD7, 0xD7, 0xD7, 0xFF),
    UIMG_MAKE_COLOUR(0xD8, 0xD8, 0xD8, 0xFF),
    UIMG_MAKE_COLOUR(0xD9, 0xD9, 0xD9, 0xFF),
    UIMG_MAKE_COLOUR(0xDA, 0xDA, 0xDA, 0xFF),
    UIMG_MAKE_COLOUR(0xDB, 0xDB, 0xDB, 0xFF),
    UIMG_MAKE_COLOUR(0xDC, 0xDC, 0xDC, 0xFF),
    UIMG_MAKE_COLOUR(0xDD, 0xDD, 0xDD, 0xFF),
    UIMG_MAKE_COLOUR(0xDE, 0xDE, 0xDE, 0xFF),
    UIMG_MAKE_COLOUR(0xDF, 0xDF, 0xDF, 0xFF),
    UIMG_MAKE_COLOUR(0xE0, 0xE0, 0xE0, 0xFF),
    UIMG_MAKE_COLOUR(0xE1, 0xE1, 0xE1, 0xFF),
    UIMG_MAKE_COLOUR(0xE2, 0xE2, 0xE2, 0xFF),
    UIMG_MAKE_COLOUR(0xE3, 0xE3, 0xE3, 0xFF),
    UIMG_MAKE_COLOUR(0xE4, 0xE4, 0xE4, 0xFF),
    UIMG_MAKE_COLOUR(0xE5, 0xE5, 0xE5, 0xFF),
    UIMG_MAKE_COLOUR(0xE6, 0xE6, 0xE6, 0xFF),
    UIMG_MAKE_COLOUR(0xE7, 0xE7, 0xE7, 0xFF),
    UIMG_MAKE_COLOUR(0xE8, 0xE8, 0xE8, 0xFF),
    UIMG_MAKE_COLOUR(0xE9, 0xE9, 0xE9, 0xFF),
    UIMG_MAKE_COLOUR(0xEA, 0xEA, 0xEA, 0xFF),
    UIMG_MAKE_COLOUR(0xEB, 0xEB, 0xEB, 0xFF),
    UIMG_MAKE_COLOUR(0xEC, 0xEC, 0xEC, 0xFF),
    UIMG_MAKE_COLOUR(0xED, 0xED, 0xED, 0xFF),
    UIMG_MAKE_COLOUR(0xEE, 0xEE, 0xEE, 0xFF),
    UIMG_MAKE_COLOUR(0xEF, 0xEF, 0xEF, 0xFF),
    UIMG_MAKE_COLOUR(0xF0, 0xF0, 0xF0, 0xFF),
    UIMG_MAKE_COLOUR(0xF1, 0xF1, 0xF1, 0xFF),
    UIMG_MAKE_COLOUR(0xF2, 0xF2, 0xF2, 0xFF),
    UIMG_MAKE_COLOUR(0xF3, 0xF3, 0xF3, 0xFF),
    UIMG_MAKE_COLOUR(0xF4, 0xF4, 0xF4, 0xFF),
    UIMG_MAKE_COLOUR(0xF5, 0xF5, 0xF5, 0xFF),
    UIMG_MAKE_COLOUR(0xF6, 0xF6, 0xF6, 0xFF),
    UIMG_MAKE_COLOUR(0xF7, 0xF7, 0xF7, 0xFF),
    UIMG_MAKE_COLOUR(0xF8, 0xF8, 0xF8, 0xFF),
    UIMG_MAKE_COLOUR(0xF9, 0xF9, 0xF9, 0xFF),
    UIMG_MAKE_COLOUR(0xFA, 0xFA, 0xFA, 0xFF),
    UIMG_MAKE_COLOUR(0xFB, 0xFB, 0xFB, 0xFF),
    UIMG_MAKE_COLOUR(0xFC, 0xFC, 0xFC, 0xFF),
    UIMG_MAKE_COLOUR(0xFD, 0xFD, 0xFD, 0xFF),
    UIMG_MAKE_COLOUR(0xFE, 0xFE, 0xFE, 0xFF),
    UIMG_MAKE_COLOUR(0xFF, 0xFF, 0xFF, 0xFF),
};

static
unsigned ceil4(unsigned x)
{
    return ( x + 4 - 1 ) & ~( 4 - 1 );
}

static
unsigned ceil8(unsigned x)
{
    return ( x + 8 - 1 ) & ~( 8 - 1 );
}

static
unsigned infohdr_get_palette_colours(const struct infohdr *hdr)
{
    unsigned pixdepth = le16toh(hdr->pixdepth);
    if( pixdepth > 8 )
        return 0;

    unsigned palette_colours = le32toh(hdr->palette_colours);
    return palette_colours ? palette_colours : ( 1 << pixdepth );
}

static
size_t calc_palette_size(int format)
{
    switch(format)
    {
    case UIMGBMP_ENC_BW:
        return sizeof(palette_bw);

    case UIMGBMP_ENC_GREY:
        return sizeof(palette_grey);

    case UIMGBMP_ENC_RGB:
        return 0;

    case UIMGBMP_ENC_RGBA:
        return 0;

    default:
        return 0;
    }
}

static
size_t calc_row_size(int format, unsigned width)
{
    switch(format)
    {
    case UIMGBMP_ENC_BW:
        return ceil4( ceil8(width) >> 3 );

    case UIMGBMP_ENC_GREY:
        return ceil4(width);

    case UIMGBMP_ENC_RGB:
        return ceil4(width*3);

    case UIMGBMP_ENC_RGBA:
        return width*4;

    default:
        return 0;
    }
}

static
uint32_t get_palette_colour(
    const uint32_t *palette, unsigned palette_colours, unsigned colour_index)
{
    return colour_index < palette_colours ?
        UIMG_COLOUR_SET_A(palette[colour_index], 0xFF) :
        UIMG_COLOUR_EMPTY;
}

static
void write_imgdat_1(const uimg_t *img, uint8_t *buf, bool topdown)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_BW, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        int src_y = topdown ? y : img->h - y - 1;
        const uint32_t *srcrow = uimg_get_row_buf_c(img, src_y);
        uint8_t *dstrow = buf + rowsize * y;

        for(int x = 0; x < img->w; ++x)
        {
            static const uint8_t threshold = 127;
            bool bit_on = UIMG_COLOUR_TO_GREYSCALE(srcrow[x]) > threshold;
            bits_switch_bit(dstrow, x, bit_on);
        }
    }
    uimg_end_buf_access(img);
}

static
void read_imgdat_1(
    uimg_t *img,
    const uint8_t *buf,
    bool topdown,
    const uint32_t *palette,
    unsigned colour_num)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_BW, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        const uint8_t *srcrow = buf + rowsize * y;
        int dst_y = topdown ? y : img->h - y - 1;
        uint32_t *dstrow = uimg_get_row_buf(img, dst_y);

        for(int x = 0; x < img->w; ++x)
        {
            unsigned colour_index = bits_get_bit(srcrow, x);
            dstrow[x] = get_palette_colour(palette, colour_num, colour_index);
        }
    }
    uimg_end_buf_access(img);
}

static
void write_imgdat_8(const uimg_t *img, uint8_t *buf, bool topdown)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_GREY, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        int src_y = topdown ? y : img->h - y - 1;
        const uint32_t *srcrow = uimg_get_row_buf_c(img, src_y);
        uint8_t *dstrow = buf + rowsize * y;

        for(int x = 0; x < img->w; ++x)
            dstrow[x] = UIMG_COLOUR_TO_GREYSCALE(srcrow[x]);
    }
    uimg_end_buf_access(img);
}

static
void read_imgdat_8(
    uimg_t *img,
    const uint8_t *buf,
    bool topdown,
    const uint32_t *palette,
    unsigned colour_num)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_GREY, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        const uint8_t *srcrow = buf + rowsize * y;
        int dst_y = topdown ? y : img->h - y - 1;
        uint32_t *dstrow = uimg_get_row_buf(img, dst_y);

        for(int x = 0; x < img->w; ++x)
            dstrow[x] = get_palette_colour(palette, colour_num, srcrow[x]);
    }
    uimg_end_buf_access(img);
}

static
void write_imgdat_24(const uimg_t *img, uint8_t *buf, bool topdown)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_RGB, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        int src_y = topdown ? y : img->h - y - 1;
        const uint32_t *srcrow = uimg_get_row_buf_c(img, src_y);
        struct bmppix24 *dstrow = (struct bmppix24*)( buf + rowsize * y );

        for(int x = 0; x < img->w; ++x)
        {
            dstrow[x].b = UIMG_COLOUR_GET_B(srcrow[x]);
            dstrow[x].g = UIMG_COLOUR_GET_G(srcrow[x]);
            dstrow[x].r = UIMG_COLOUR_GET_R(srcrow[x]);
        }
    }
    uimg_end_buf_access(img);
}

static
void read_imgdat_24(uimg_t *img, const uint8_t *buf, bool topdown)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_RGB, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        const struct bmppix24 *srcrow = (const struct bmppix24*)( buf + rowsize * y );
        int dst_y = topdown ? y : img->h - y - 1;
        uint32_t *dstrow = uimg_get_row_buf(img, dst_y);

        for(int x = 0; x < img->w; ++x)
            dstrow[x] = UIMG_MAKE_COLOUR(srcrow[x].b, srcrow[x].g, srcrow[x].r, 0xFF);
    }
    uimg_end_buf_access(img);
}

static
void write_imgdat_32(const uimg_t *img, uint8_t *buf, bool topdown)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_RGBA, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        int src_y = topdown ? y : img->h - y - 1;
        const uint32_t *srcrow = uimg_get_row_buf_c(img, src_y);
        uint32_t *dstrow = (uint32_t*)( buf + rowsize * y );

        for(int x = 0; x < img->w; ++x)
            dstrow[x] = srcrow[x];
    }
    uimg_end_buf_access(img);
}

static
void read_imgdat_32(uimg_t *img, const void *buf, bool topdown, bool ignore_alpha)
{
    size_t rowsize = calc_row_size(UIMGBMP_ENC_RGBA, img->w);

    uimg_begin_buf_access(img);
    for(int y = 0; y < img->h; ++y)
    {
        const uint32_t *srcrow =
            (const uint32_t*)( (const uint8_t*) buf + rowsize * y );
        int dst_y = topdown ? y : img->h - y - 1;
        uint32_t *dstrow = uimg_get_row_buf(img, dst_y);

        for(int x = 0; x < img->w; ++x)
        {
            dstrow[x] =
                ignore_alpha ? UIMG_COLOUR_SET_A(dstrow[x], 0xFF) : srcrow[x];
        }
    }
    uimg_end_buf_access(img);
}

static
uint32_t* bmpraw_get_palette(void *bmpraw)
{
    struct infohdr *infohdr =
        (struct infohdr*)( (uint8_t*) bmpraw + sizeof(struct filehdr) );
    unsigned infosize = le32toh(infohdr->hdrsize);
    uint32_t *palette =
        (uint32_t*)( (uint8_t*) bmpraw + sizeof(struct filehdr) + infosize );
    return palette;
}

static
void* bmpraw_get_imgdata(void *bmpraw)
{
    struct filehdr *filehdr = bmpraw;
    unsigned imgoffset = le32toh(filehdr->imgoffset);
    uint8_t *imgdata = (uint8_t*) bmpraw + imgoffset;
    return imgdata;
}

static
size_t bmpraw_write_header(
    uint8_t *buf, size_t bufsz, int width, int height, int flags)
{
    int format = flags & UIMGBMP_ENC_COLOURMASK;
    bool topdown = flags & UIMGBMP_ENC_TOPDOWN;
    assert( buf && bufsz && width > 0 && height > 0 && format );

    // Calculate properties.
    size_t palette_size = calc_palette_size(format);
    size_t imgsize = height * calc_row_size(format, width);
    size_t imgoffset = sizeof(struct filehdr) + sizeof(struct infohdr) + palette_size;
    size_t filesize = imgoffset + imgsize;
    if( !imgsize || bufsz < filesize ) return 0;

    // Get fields by offset.
    struct filehdr *filehdr = (struct filehdr* )( (uint8_t*) buf + 0 );
    struct infohdr *infohdr =
        (struct infohdr* )( (uint8_t*) buf + sizeof(struct filehdr) );

    // Write header.
    memset(filehdr, 0, sizeof(*filehdr));
    memset(infohdr, 0, sizeof(*infohdr));
    memcpy(filehdr->signature, bmp_signature, sizeof(bmp_signature));
    filehdr->filesize  = htole32(filesize);
    filehdr->imgoffset = htole32(imgoffset);
    infohdr->hdrsize   = htole32(sizeof(*infohdr));
    infohdr->width     = htole32(width);
    infohdr->height    = htole32( topdown ? -height : height );
    infohdr->planes    = htole16(1);
    infohdr->pixdepth  = htole16(format);
    infohdr->imgsize   = htole32(imgsize);

    return filesize;
}

static
bool bmpraw_read_header(
    const uint8_t *bmpraw,
    size_t rawsize,
    struct filehdr *filehdr,
    struct infohdr *infohdr,
    unsigned *width,
    unsigned *height)
{
    // Get header.
    if( rawsize < sizeof(*filehdr) + sizeof(*infohdr) )
        return false;
    *filehdr = *(struct filehdr*)( (const uint8_t*) bmpraw + 0 );
    *infohdr = *(struct infohdr*)( (const uint8_t*) bmpraw + sizeof(struct filehdr) );

    // Check header basic information.
    if(memcmp(filehdr->signature, bmp_signature, sizeof(bmp_signature)))
        return false;
    if( rawsize < le32toh(filehdr->filesize) )
        return false;
    if( le32toh(infohdr->hdrsize) < sizeof(infohdr) )
        return false;   // Unsupported information header!
    int w = (int32_t) le32toh(infohdr->width);
    int h = (int32_t) le32toh(infohdr->height);
    if( w <= 0 || h == 0 )
        return false;
    if( le16toh(infohdr->planes) != 1 )
        return false;
    if(le32toh(infohdr->compression))
        return false;   // Not support for compressed data!

    // Check image data size.
    *width = w;
    *height = h > 0 ? h : -h;
    size_t imgsize = (*height) * calc_row_size(le16toh(infohdr->pixdepth), *width);
    if(!imgsize)
        return false;
    if( le32toh(infohdr->imgsize) && le32toh(infohdr->imgsize) < imgsize )
        return false;
    if( le32toh(filehdr->imgoffset) + imgsize > le32toh(filehdr->filesize) )
        return false;

    // Check palette properties.
    unsigned palette_colours = infohdr_get_palette_colours(infohdr);
    size_t palette_size = palette_colours * sizeof(uint32_t);
    size_t palette_offset = sizeof(struct filehdr) + le32toh(infohdr->hdrsize);
    if( palette_offset + palette_size < le32toh(filehdr->imgoffset) )
        return false;

    return true;
}

size_t uimgbmp_encode(const uimg_t *img, void *buf, size_t size, int flags)
{
    /**
     * Encode to the BMP format data
     *
     * @param img   The image object to be encoded.
     * @param buf   The buffer to receive the encoded data.
     * @param size  Size of the output buffer.
     * @param flags Encode options in ::uimgbmp_encopt.
     *              This parameter can be ZERO to use defaults.
     * @return Total size of data has been fill to the buffer; or ZERO if failed.
     */
    if( !img || !img->buf || !img->w || !img->h ) return 0;
    if( !buf || !size ) return 0;

    if( !( flags & UIMGBMP_ENC_COLOURMASK ) )
        flags |= UIMGBMP_ENC_RGBA;
    int colour_format = flags & UIMGBMP_ENC_COLOURMASK;
    bool rows_topdown = flags & UIMGBMP_ENC_TOPDOWN;

    // Write header.
    size_t filesize = bmpraw_write_header(buf, size, img->w, img->h, flags);
    if( !filesize ) return 0;

    // Write palette.
    uint32_t *palette = bmpraw_get_palette(buf);
    switch(colour_format)
    {
    case UIMGBMP_ENC_BW:
        memcpy(palette, palette_bw, sizeof(palette_bw));
        break;

    case UIMGBMP_ENC_GREY:
        memcpy(palette, palette_grey, sizeof(palette_grey));
        break;
    }

    // Write image data.
    uint8_t *imgdata = bmpraw_get_imgdata(buf);
    switch(colour_format)
    {
    case UIMGBMP_ENC_BW:
        write_imgdat_1(img, imgdata, rows_topdown);
        break;

    case UIMGBMP_ENC_GREY:
        write_imgdat_8(img, imgdata, rows_topdown);
        break;

    case UIMGBMP_ENC_RGB:
        write_imgdat_24(img, imgdata, rows_topdown);
        break;

    case UIMGBMP_ENC_RGBA:
        write_imgdat_32(img, imgdata, rows_topdown);
        break;

    default:
        return 0;
    }

    return filesize;
}

size_t uimgbmp_decode(uimg_t *img, const void *data, size_t size, int flags)
{
    /**
     * Decode from the BMP format data
     *
     * @param img   The image object to receive the decoded result.
     * @param data  The BMP data to be decoded.
     * @param size  Size of the input data.
     * @param flags Decode options in ::uimgbmp_decopt.
     *              This parameter can be ZERO to use defaults.
     * @return Total size of data has been read to decode; or ZERO if failed.
     */
    if( !img || !data || !size ) return 0;

    // Read header.

    struct filehdr filehdr;
    struct infohdr infohdr;
    unsigned width, height;
    if(!bmpraw_read_header(data, size, &filehdr, &infohdr, &width, &height))
        return 0;

    bool rows_topdown = (int32_t) le32toh(infohdr.height) < 0;

    // Read image data.

    if(uimg_resize(img, width, height))
        return 0;

    unsigned palette_colours = infohdr_get_palette_colours(&infohdr);
    const uint32_t *palette = bmpraw_get_palette((void*)data);
    const uint8_t *imgdata = bmpraw_get_imgdata((void*)data);

    switch(le16toh(infohdr.pixdepth))
    {
    case UIMGBMP_ENC_BW:
        read_imgdat_1(img, imgdata, rows_topdown, palette, palette_colours);
        break;

    case UIMGBMP_ENC_GREY:
        read_imgdat_8(img, imgdata, rows_topdown, palette, palette_colours);
        break;

    case UIMGBMP_ENC_RGB:
        read_imgdat_24(img, imgdata, rows_topdown);
        break;

    case UIMGBMP_ENC_RGBA:
        read_imgdat_32(img, imgdata, rows_topdown, flags & UIMGBMP_DEC_IGNORE_ALPHA);
        break;

    default:
        return 0;
    }

    return le32toh(filehdr.filesize);
}

int uimgbmp_save_file(const uimg_t *img, const char *filename, int flags)
{
    /**
     * Save image to file
     *
     * @param img       The image object to be saved.
     * @param filename  Name of the file.
     * @param flags     Encode options in ::uimgbmp_encopt.
     *                  This parameter can be ZERO to use defaults.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename ) return -1;

    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if(!img->buf) break;

        static const size_t redundancy_size = 1280;
        size_t estimate_size = img->w * img->h * sizeof(uint32_t) + redundancy_size;
        if( !( buf = malloc(estimate_size) ) ) break;

        size_t encode_size = uimgbmp_encode(img, buf, estimate_size, flags);
        if(!encode_size) break;

        filebuf_save(filename, buf, encode_size);

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}

int uimgbmp_load_file(uimg_t *img, const char *filename, int flags)
{
    /**
     * Load image from file
     *
     * @param img       The image object to be loaded.
     * @param filename  Name of the file.
     * @param flags     Decode options in ::uimgbmp_decopt.
     *                  This parameter can be ZERO to use defaults.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename ) return -1;

    size_t size = 0;
    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if( !( buf = filebuf_load(filename, &size) ) ) break;
        if(!uimgbmp_decode(img, buf, size, flags)) break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}
