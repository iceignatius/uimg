#include <stdbool.h>
#include <string.h>
#include <rkmedia_buffer.h>
#include <rga/RgaApi.h>
#include "uimgcolour.h"
#include "uimgdrv.h"

#define RGA_BLENDMODE_OVERLAY   0xFF0100
#define RGA_BLENDMODE_BLEND     0xFF0405

#define RGA_SCALEMODE_NEARBY    0
#define RGA_SCALEMODE_LINEAR    1
#define RGA_SCALEMODE_CUBIC     2

/*
 * The 2-D render functions on RV1126 (RGA) are so slow,
 * so that it is recommended to implement some of simple render functions
 * by CPU instead of the RGA.
 */
#define NO_RGA_ON_SIMPLE_BLIT   1

unsigned uimgdrv_get_magic(void)
{
    return UIMG_DRVID_RV1126;
}

struct uimg* uimgdrv_alloc_imgslot(void)
{
    return malloc(sizeof(struct uimg));
}

void uimgdrv_free_imgslot(struct uimg *slot)
{
    free(slot);
}

void* uimgdrv_alloc_imgbuf(unsigned w, unsigned h)
{
    MB_IMAGE_INFO_S mediainfo =
    {
        .u32Width = w,
        .u32Height = h,
        .u32HorStride = w,
        .u32VerStride = h,
        .enImgType = IMAGE_TYPE_ARGB8888,
    };

    return RK_MPI_MB_CreateImageBuffer(&mediainfo, RK_TRUE, 0);
}

void* uimgdrv_clone_imgbuf(void *drvbuf, unsigned w, unsigned h)
{
    MEDIA_BUFFER src_media = drvbuf;
    MEDIA_BUFFER dst_media = NULL;

    bool succ = false;
    do
    {
        MB_IMAGE_INFO_S mediainfo =
        {
            .u32Width = w,
            .u32Height = h,
            .u32HorStride = w,
            .u32VerStride = h,
            .enImgType = IMAGE_TYPE_ARGB8888,
        };

        if( !( dst_media = RK_MPI_MB_CreateImageBuffer(&mediainfo, RK_TRUE, 0) ) )
            break;

        void *dst_base = RK_MPI_MB_GetPtr(dst_media);
        void *src_base = RK_MPI_MB_GetPtr(src_media);
        if( !dst_base || !src_base )
            break;

        RK_MPI_MB_BeginCPUAccess(dst_media, RK_FALSE);
        RK_MPI_MB_BeginCPUAccess(src_media, RK_TRUE);

        size_t bufsize = w * h * sizeof(uint32_t);
        memcpy(dst_base, src_base, bufsize);

        RK_MPI_MB_EndCPUAccess(dst_media, RK_FALSE);
        RK_MPI_MB_EndCPUAccess(src_media, RK_TRUE);

        succ = true;
    } while(false);

    if( !succ && dst_media )
    {
        RK_MPI_MB_ReleaseBuffer(dst_media);
        dst_media = NULL;
    }

    return dst_media;
}

void uimgdrv_free_imgbuf(void *drvbuf)
{
    RK_MPI_MB_ReleaseBuffer(drvbuf);
}

int uimgdrv_begin_buf_access(void *drvbuf)
{
    return RK_MPI_MB_BeginCPUAccess(drvbuf, RK_FALSE);
}

int uimgdrv_end_buf_access(void *drvbuf)
{
    return RK_MPI_MB_EndCPUAccess(drvbuf, RK_FALSE);
}

uint32_t* uimgdrv_get_row_buf(void *drvbuf, unsigned w, unsigned h, unsigned y)
{
    uint32_t *base = RK_MPI_MB_GetPtr(drvbuf);
    return base + w * y;
}

int uimgdrv_fill(
    void *drvbuf, unsigned w, unsigned h, uint32_t val, const struct uimg_rect *rect)
{
    uint32_t fillcolour =
        ( UIMG_COLOUR_GET_A(val) << 3*8 ) |
        ( UIMG_COLOUR_GET_B(val) << 2*8 ) |
        ( UIMG_COLOUR_GET_G(val) << 1*8 ) |
        UIMG_COLOUR_GET_R(val);

#if NO_RGA_ON_SIMPLE_BLIT
    RK_MPI_MB_BeginCPUAccess(drvbuf, RK_FALSE);
    uint32_t *dst_base = (uint32_t*) RK_MPI_MB_GetPtr(drvbuf);

    if( !rect ||
        ( rect->x <= 0 && rect->y <= 0 && rect->w >= w && rect->h >= h ) )
    {
        int n = w * h;
        for(int i = 0; i < n; ++i)
            dst_base[i] = fillcolour;
    }
    else
    {
        for(int j = 0; j < rect->h; ++j)
        {
            int y = rect->y + j;
            uint32_t *row = dst_base + y * w;
            for(int i = 0; i < rect->w; ++i)
            {
                int x = rect->x + i;
                row[x] = fillcolour;
            }
        }
    }

    RK_MPI_MB_EndCPUAccess(drvbuf, RK_FALSE);

    return 0;
#else
    size_t bufsize = w * h * sizeof(uint32_t);

    rga_info_t dstinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(drvbuf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = rect ? rect->x : 0,
            .yoffset = rect ? rect->y : 0,
            .width = rect ? rect->w : w,
            .height = rect ? rect->h : h,
            .wstride = w,
            .hstride = h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .bufferSize = bufsize,
        .color = fillcolour,
        .mmuFlag = 1,
    };

    return c_RkRgaColorFill(&dstinfo);
#endif
}

int uimgdrv_flip(void *dst_buf, void *src_buf, unsigned w, unsigned h, int dir)
{
    size_t bufsize = w * h * sizeof(uint32_t);

    int rga_flip;
    switch(dir)
    {
    case UIMG_FLIP_H:
        rga_flip = HAL_TRANSFORM_FLIP_H;
        break;

    case UIMG_FLIP_V:
        rga_flip = HAL_TRANSFORM_FLIP_V;
        break;

    default:
        return -1;
    }

    rga_info_t dstinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(dst_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = w,
            .height = h,
            .wstride = w,
            .hstride = h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .bufferSize = bufsize,
        .mmuFlag = 1,
    };

    rga_info_t srcinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(src_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = w,
            .height = h,
            .wstride = w,
            .hstride = h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .blend = RGA_BLENDMODE_OVERLAY,
        .bufferSize = bufsize,
        .rotation = rga_flip,
        .mmuFlag = 1,
    };

    return c_RkRgaBlit(&srcinfo, &dstinfo, NULL);
}

int uimgdrv_rotate(
    void *dst_buf, void *src_buf, unsigned src_w, unsigned src_h, int ang)
{
    size_t bufsize = src_w * src_h * sizeof(uint32_t);

    int rga_rotate;
    unsigned dst_w = 0, dst_h = 0;
    switch(ang)
    {
    case UIMG_ROTATE_0:
        rga_rotate = 0;
        dst_w = src_w;
        dst_h = src_h;
        break;

    case UIMG_ROTATE_90:
        rga_rotate = HAL_TRANSFORM_ROT_90;
        dst_w = src_h;
        dst_h = src_w;
        break;

    case UIMG_ROTATE_180:
        rga_rotate = HAL_TRANSFORM_ROT_180;
        dst_w = src_w;
        dst_h = src_h;
        break;

    case UIMG_ROTATE_270:
        rga_rotate = HAL_TRANSFORM_ROT_270;
        dst_w = src_h;
        dst_h = src_w;
        break;

    default:
        return -1;
    }

    rga_info_t dstinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(dst_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = dst_w,
            .height = dst_h,
            .wstride = dst_w,
            .hstride = dst_h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .bufferSize = bufsize,
        .mmuFlag = 1,
    };

    rga_info_t srcinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(src_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = src_w,
            .height = src_h,
            .wstride = src_w,
            .hstride = src_h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .blend = RGA_BLENDMODE_OVERLAY,
        .bufferSize = bufsize,
        .rotation = rga_rotate,
        .mmuFlag = 1,
    };

    return c_RkRgaBlit(&srcinfo, &dstinfo, NULL);
}

int uimgdrv_stretch(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    int scale_mode)
{
    size_t dst_bufsize = dst_w * dst_h * sizeof(uint32_t);
    size_t src_bufsize = src_w * src_h * sizeof(uint32_t);

    int rga_scale_mode;
    switch(scale_mode)
    {
    case UIMG_SCALE_NEARBY:
        rga_scale_mode = RGA_SCALEMODE_NEARBY;
        break;

    case UIMG_SCALE_LINEAR:
        rga_scale_mode = RGA_SCALEMODE_LINEAR;
        break;

    case UIMG_SCALE_SPLINE:
        rga_scale_mode = RGA_SCALEMODE_CUBIC;
        break;

    default:
        return -1;
    }

    rga_info_t dstinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(dst_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = dst_w,
            .height = dst_h,
            .wstride = dst_w,
            .hstride = dst_h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .bufferSize = dst_bufsize,
        .mmuFlag = 1,
    };

    rga_info_t srcinfo =
    {
        .fd = -1,
        .virAddr = RK_MPI_MB_GetPtr(src_buf),
        .format = RK_FORMAT_BGRA_8888,
        .rect =
        {
            .xoffset = 0,
            .yoffset = 0,
            .width = src_w,
            .height = src_h,
            .wstride = src_w,
            .hstride = src_h,
            .format = RK_FORMAT_BGRA_8888,
        },
        .blend = RGA_BLENDMODE_OVERLAY,
        .bufferSize = src_bufsize,
        .mmuFlag = 1,
        .scale_mode = rga_scale_mode,
    };

    return c_RkRgaBlit(&srcinfo, &dstinfo, NULL);
}

int uimgdrv_blend(
    void *dst_buf,
    unsigned dst_w,
    unsigned dst_h,
    const struct uimg_rect *dst_rect,
    void *src_buf,
    unsigned src_w,
    unsigned src_h,
    const struct uimg_rect *src_rect,
    int blend_mode)
{
    size_t dst_bufsize = dst_w * dst_h * sizeof(uint32_t);
    size_t src_bufsize = src_w * src_h * sizeof(uint32_t);

    int rga_blend_mode;
    switch(blend_mode)
    {
    case UIMG_BLEND_OVERLAY:
        rga_blend_mode = RGA_BLENDMODE_OVERLAY;
        break;

    case UIMG_BLEND_ALPHA:
        rga_blend_mode = RGA_BLENDMODE_BLEND;
        break;

    default:
        return -1;
    }

#if NO_RGA_ON_SIMPLE_BLIT
    if( rga_blend_mode == RGA_BLENDMODE_OVERLAY )
    {
        RK_MPI_MB_BeginCPUAccess(dst_buf, RK_FALSE);
        RK_MPI_MB_BeginCPUAccess(src_buf, RK_TRUE);

        uint32_t *dst_base = RK_MPI_MB_GetPtr(dst_buf);
        const uint32_t *src_base = RK_MPI_MB_GetPtr(src_buf);

        for(int j = 0; j < src_rect->h; ++j)
        {
            int dst_y = dst_rect->y + j;
            int src_y = src_rect->y + j;
            uint32_t *dst_row = dst_base + dst_y * dst_w;
            const uint32_t *src_row = src_base + src_y * src_w;

            for(int i = 0; i < src_rect->w; ++i)
            {
                int dst_x = dst_rect->x + i;
                int src_x = src_rect->x + i;
                dst_row[dst_x] = src_row[src_x];
            }
        }

        RK_MPI_MB_EndCPUAccess(dst_buf, RK_FALSE);
        RK_MPI_MB_EndCPUAccess(src_buf, RK_TRUE);

        return 0;
    }
    else
#endif
    {
        rga_info_t dstinfo =
        {
            .fd = -1,
            .virAddr = RK_MPI_MB_GetPtr(dst_buf),
            .format = RK_FORMAT_BGRA_8888,
            .rect =
            {
                .xoffset = dst_rect->x,
                .yoffset = dst_rect->y,
                .width = dst_rect->w,
                .height = dst_rect->h,
                .wstride = dst_w,
                .hstride = dst_h,
                .format = RK_FORMAT_BGRA_8888,
            },
            .bufferSize = dst_bufsize,
            .mmuFlag = 1,
        };

        rga_info_t srcinfo =
        {
            .fd = -1,
            .virAddr = RK_MPI_MB_GetPtr(src_buf),
            .format = RK_FORMAT_BGRA_8888,
            .rect =
            {
                .xoffset = src_rect->x,
                .yoffset = src_rect->y,
                .width = src_rect->w,
                .height = src_rect->h,
                .wstride = src_w,
                .hstride = src_h,
                .format = RK_FORMAT_BGRA_8888,
            },
            .blend = rga_blend_mode,
            .bufferSize = src_bufsize,
            .mmuFlag = 1,
        };

        return c_RkRgaBlit(&srcinfo, &dstinfo, NULL);
    }
}
