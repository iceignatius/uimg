#ifndef _GIF_PALETTE_GREY_H_
#define _GIF_PALETTE_GREY_H_

#include <gif_lib.h>
#include "uimgcolour.h"

static
const GifColorType* get_grey_table(void *host)
{
    static GifColorType palette[265];

    static bool inited = false;
    if(inited)
        return palette;

    for(unsigned i = 0; i < 256; ++i)
    {
        palette[i].Red = i;
        palette[i].Green = i;
        palette[i].Blue = i;
    }

    inited = true;
    return palette;
}

static
unsigned count_grey_colours(void *host)
{
    return 256;
}

static
uint8_t calc_grey_index(void *host, uint32_t colour)
{
    return UIMG_COLOUR_TO_GREYSCALE(colour);
}

static
int get_grey_transparent(void *host)
{
    return NO_TRANSPARENT_COLOR;
}

#endif
