#include <stdbool.h>
#include "uimgdrv.h"
#include "uimgblit.h"

static
int max(int a, int b)
{
    return a > b ? a : b;
}

int uimgblit_fill(uimg_t *img, uint32_t val, const struct uimg_rect *rect)
{
    /**
     * Fill image with a specific colour
     *
     * @param img   The image object to be operated.
     * @param val   The colour to be filled to the image.
     * @param rect  The area to be filled.
     *              This parameter can be NULL to fill the whole image.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     */
#ifdef UIMG_ENABLE_BLITMOD
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;

    struct uimg_rect area = {0};
    if(rect)
    {
        int trim_left = max(-rect->x, 0);
        int trim_right = max(rect->x + rect->w - uimg_get_width(img), 0);
        int trim_top = max(-rect->y, 0);
        int trim_bottom = max(rect->y + rect->h - uimg_get_height(img), 0);

        area.x = rect->x + trim_left;
        area.y = rect->y + trim_top;
        area.w = rect->w - trim_left - trim_right;
        area.h = rect->h - trim_top - trim_bottom;
        if( area.w <= 0 || area.h <= 0 )
            return 0;
    }

    return uimgdrv_fill(img->buf, img->w, img->h, val, rect ? &area : NULL);
#else
    return -1;
#endif
}

int uimgblit_flip(uimg_t *img, int dir)
{
    /**
     * Flip image
     *
     * @param img The image object to be operated.
     * @param dir The flip direction (::uimg_flip).
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     */
#ifdef UIMG_ENABLE_BLITMOD
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;

    uimg_t *srcbuf = uimgdrv_clone_imgbuf(img->buf, img->w, img->h);
    if(!srcbuf) return -1;

    int err = uimgdrv_flip(img->buf, srcbuf, img->w, img->h, dir);

    uimgdrv_free_imgbuf(srcbuf);
    return err;
#else
    return -1;
#endif
}

int uimgblit_rotate(uimg_t *img, int ang)
{
    /**
     * Rotate image
     *
     * @param img   The image object to be operated.
     * @param ang   The rotation angle (clockwise),
     *              only 0, 90, 180, and 270 are supported (::uimg_rotate).
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     *
     * @remarks Image resolution may be changed after operation.
     */
#ifdef UIMG_ENABLE_BLITMOD
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;
    if( ang == UIMG_ROTATE_0 )
        return 0;

    unsigned src_w = img->w;
    unsigned src_h = img->h;
    unsigned dst_w, dst_h;
    switch(ang)
    {
    case UIMG_ROTATE_90:
    case UIMG_ROTATE_270:
        dst_w = src_h;
        dst_h = src_w;
        break;

    case UIMG_ROTATE_0:
    case UIMG_ROTATE_180:
        dst_w = src_w;
        dst_h = src_h;
        break;

    default:
        return -1;
    }

    void *src_buf = NULL;

    int err = -1;
    do
    {
        if( !( src_buf = uimgdrv_clone_imgbuf(img->buf, img->w, img->h) ) )
            break;

        if(( err = uimg_resize(img, dst_w, dst_h) ))
            break;

        err = uimgdrv_rotate(img->buf, src_buf, src_w, src_h, ang);
    } while(false);

    if(src_buf)
        uimgdrv_free_imgbuf(src_buf);

    return err;
#else
    return -1;
#endif
}

int uimgblit_stretch(uimg_t *img, int w, int h, int scale_mode)
{
    /**
     * Resize image and stretch its contents
     *
     * @param img           The image object to be operated.
     * @param w             New width of the image.
     * @param h             New height of the image.
     * @param scale_mode    The scale mode to be used to scaling the source image.
     *                      If no scale mode be applied or be supported, then
     *                      the behaviour is undefined.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     *
     * @remarks
     * The @p scale_mode parameters is the "wanted" methods,
     * not the "required" methods.
     * It means that the driver may fallback to use another method
     * if the wanted method is not supported,
     * or it may even return the failure result,
     * it is depend on the driver implementation.
     */
#ifdef UIMG_ENABLE_BLITMOD
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;
    if( img->w == w && img->h == h )
        return 0;

    void *src_buf = NULL;

    int err = -1;
    do
    {
        unsigned src_w = img->w;
        unsigned src_h = img->h;
        if( !( src_buf = uimgdrv_clone_imgbuf(img->buf, img->w, img->h) ) )
            break;

        if(( err = uimg_resize(img, w, h) ))
            break;

        err = uimgdrv_stretch(
            img->buf, img->w, img->h, src_buf, src_w, src_h, scale_mode);
    } while(false);

    if(src_buf)
        uimgdrv_free_imgbuf(src_buf);

    return err;
#else
    return -1;
#endif
}

int uimgblit_blend(
    uimg_t *img,
    int dst_x,
    int dst_y,
    const uimg_t *src,
    const struct uimg_rect *src_rect,
    int blend_mode)
{
    /**
     * Blend image
     *
     * @param img           The image object to be operated.
     * @param dst_x         The blend position X on @p img.
     * @param dst_y         The blend position Y on @p img.
     * @param src           The source image be used to blend.
     * @param src_rect      The area on @p src to be used to blend.
     *                      This parameter can be NULL to use the whole source image.
     * @param blend_mode    The blend mode (::uimg_blend_mode) should be used to
     *                      blend image.
     * @return  If success, ZERO is returned; or
     *          a driver defined error code will be returned if error occurred.
     *
     * @remarks
     * The @p blend_mode parameters is the "wanted" methods,
     * not the "required" methods.
     * It means that the driver may fallback to use another method
     * if the wanted method is not supported,
     * or it may even return the failure result,
     * it is depend on the driver implementation.
     */
#ifdef UIMG_ENABLE_BLITMOD
    if( !img || img->magic != uimgdrv_get_magic() || !img->buf )
        return -1;
    if( !src || src->magic != uimgdrv_get_magic() || !src->buf )
        return -1;

    struct uimg_rect src_area;
    if(src_rect)
    {
        src_area = *src_rect;
    }
    else
    {
        src_area.x = 0;
        src_area.y = 0;
        src_area.w = uimg_get_width(src);
        src_area.h = uimg_get_height(src);
    }

    struct uimg_rect dst_area =
    {
        .x = dst_x,
        .y = dst_y,
        .w = src_area.w,
        .h = src_area.h,
    };

    int trim_left = max(-dst_area.x, 0);
    int trim_right = max(dst_area.x + dst_area.w - uimg_get_width(img), 0);
    int trim_top = max(-dst_area.y, 0);
    int trim_bottom = max(dst_area.y + dst_area.h - uimg_get_height(img), 0);

    src_area.x += trim_left;
    src_area.y += trim_top;
    src_area.w -= trim_left + trim_right;
    src_area.h -= trim_top + trim_bottom;
    if( src_area.w <= 0 || src_area.h <= 0 )
        return 0;

    dst_area.x += trim_left;
    dst_area.y += trim_top;
    dst_area.w -= trim_left + trim_right;
    dst_area.h -= trim_top + trim_bottom;
    if( dst_area.w <= 0 || dst_area.h <= 0 )
        return 0;

    return uimgdrv_blend(
        img->buf,
        img->w,
        img->h,
        &dst_area,
        src->buf,
        src->w,
        src->h,
        &src_area,
        blend_mode);
#else
    return -1;
#endif
}
