#include <string.h>
#include <endian.h>
#include <zlib.h>
#include <png.h>
#include "filebuf.h"
#include "uimgdrv.h"
#include "uimgpng.h"

struct stream
{
    uint8_t *buf;
    size_t rest_size;
    size_t proc_size;
};

static
void stream_init(struct stream *stm, void *buf, size_t size)
{
    stm->buf = buf;
    stm->rest_size = size;
    stm->proc_size = 0;
}

static
bool stream_write(struct stream *stm, const void *data, size_t size)
{
    if( stm->rest_size < size )
        return false;

    memcpy(stm->buf, data, size);
    stm->buf += size;
    stm->rest_size -= size;
    stm->proc_size += size;

    return true;
}

static
bool stream_read(struct stream *stm, void *buf, size_t size)
{
    if( stm->rest_size < size )
        return false;

    memcpy(buf, stm->buf, size);
    stm->buf += size;
    stm->rest_size -= size;
    stm->proc_size += size;

    return true;
}

static
int select_bit_depth(int fmt)
{
    switch(fmt)
    {
    case UIMGPNG_BGRA:
    case UIMGPNG_BGR:
    case UIMGPNG_GREY8:
    case UIMGPNG_GREY8A:
        return 8;

    case UIMGPNG_GREY16:
        return 16;

    case UIMGPNG_BW:
        return 1;

    default:
        return -1;
    }
}

static
int select_colour_type(int fmt)
{
    switch(fmt)
    {
    case UIMGPNG_BGRA:
        return PNG_COLOR_TYPE_RGB_ALPHA;

    case UIMGPNG_BGR:
        return PNG_COLOR_TYPE_RGB;

    case UIMGPNG_GREY8A:
        return PNG_COLOR_TYPE_GRAY_ALPHA;

    case UIMGPNG_GREY8:
    case UIMGPNG_GREY16:
    case UIMGPNG_BW:
        return PNG_COLOR_TYPE_GRAY;

    default:
        return -1;
    }
}

static
int select_comp_level(int comp_type)
{
    switch(comp_type)
    {
    case UIMGPNG_COMP_BEST:
        return Z_BEST_COMPRESSION;

    case UIMGPNG_COMP_FAST:
        return Z_BEST_SPEED;

    case UIMGPNG_COMP_NO:
        return Z_NO_COMPRESSION;

    default:
        return Z_DEFAULT_COMPRESSION;
    }
}

static
const uint8_t* row_bgra_to_bgr(uint8_t *dst, const uint32_t *src, int w)
{
    for(int dst_i = 0, src_i = 0; src_i < w; ++src_i, dst_i += 3)
    {
        dst[ dst_i + 0 ] = UIMG_COLOUR_GET_B(src[src_i]);
        dst[ dst_i + 1 ] = UIMG_COLOUR_GET_G(src[src_i]);
        dst[ dst_i + 2 ] = UIMG_COLOUR_GET_R(src[src_i]);
    }

    return dst;
}

static
const uint16_t* row_bgra_to_grey8a(uint16_t *dst, const uint32_t *src, int w)
{
    for(int i = 0; i < w; ++i)
    {
        uint16_t v =
            ( UIMG_COLOUR_TO_GREYSCALE(src[i]) << 8 ) |
            UIMG_COLOUR_GET_A(src[i]);
        dst[i] = htobe16(v);
    }

    return dst;
}

static
const uint8_t* row_bgra_to_grey8(uint8_t *dst, const uint32_t *src, int w)
{
    for(int i = 0; i < w; ++i)
        dst[i] = UIMG_COLOUR_TO_GREYSCALE(src[i]);

    return dst;
}

static
const uint16_t* row_bgra_to_grey16(uint16_t *dst, const uint32_t *src, int w)
{
    for(int i = 0; i < w; ++i)
        dst[i] = htobe16(src[i]);

    return dst;
}

static
const uint8_t* row_bgra_to_bw(uint8_t *dst, const uint32_t *src, int w)
{
    memset(dst, 0, ( w + 8 - 1 ) >> 3);
    for(int i = 0; i < w; ++i)
    {
        uint8_t grey = UIMG_COLOUR_TO_GREYSCALE(src[i]);
        unsigned bw = grey > 127 ? 0x80 : 0x00;
        dst[ i >> 3 ] |= bw >> ( i & 0x7 );
    }

    return dst;
}

static
const uint8_t* row_bgra_to_fmt(uint8_t *row_buf, const uint32_t *src, int w, int fmt)
{
    switch(fmt)
    {
    case UIMGPNG_BGRA:
        return (const uint8_t*) src;

    case UIMGPNG_BGR:
        return row_bgra_to_bgr(row_buf, src, w);

    case UIMGPNG_GREY8A:
        return (const uint8_t*) row_bgra_to_grey8a((uint16_t*) row_buf, src, w);

    case UIMGPNG_GREY8:
        return row_bgra_to_grey8(row_buf, src, w);

    case UIMGPNG_GREY16:
        return (const uint8_t*) row_bgra_to_grey16((uint16_t*) row_buf, src, w);

    case UIMGPNG_BW:
        return row_bgra_to_bw(row_buf, src, w);

    default:
        return (const uint8_t*) src;
    }
}

static
void on_write_data(png_structp png, png_bytep src, png_size_t size)
{
    if(!stream_write(png_get_io_ptr(png), src, size) )
        png_error(png, "Writer buffer fulled up and not enough!");
}

size_t uimgpng_encode(const uimg_t *img, void *buf, size_t size, int flags)
{
    /**
     * Encode to the PNG format data
     *
     * @param img   The image object to be encoded.
     * @param buf   The buffer to receive the encoded data.
     * @param size  Size of the output buffer.
     * @param flags Encode options in ::uimgpng_encopt.
     *              This parameter can be ZERO to use defaults.
     * @return Total size of data has been fill to the buffer; or ZERO if failed.
     */
    if( !img || !img->buf || !img->w || !img->h ) return 0;
    if( !buf || !size ) return 0;

    if( !( flags & UIMGPNG_FMT_MASK ) )
        flags |= UIMGPNG_BGRA;
    if( !( flags & UIMGPNG_COMP_MASK ) )
        flags |= UIMGPNG_COMP_BEST;
    int format = flags & UIMGPNG_FMT_MASK;

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop hdrinfo = png_create_info_struct(png);
    uint8_t *rowbuf = NULL;

    size_t encsize = 0;
    if( png && hdrinfo && 0 == setjmp(png_jmpbuf(png)) )
    {
        // Set write stream
        struct stream stm;
        stream_init(&stm, buf, size);
        png_set_write_fn(png, &stm, on_write_data, NULL);

        if( format != UIMGPNG_BGRA )
        {
            if( !( rowbuf = malloc( img->w * sizeof(uint32_t) ) ) )
                png_error(png, "Cannot allocale more memory!");
        }

        int bit_depth = select_bit_depth(format);
        int colour_type = select_colour_type(format);
        if( bit_depth < 0 || colour_type < 0 )
            png_error(png, "Unsupported pixel format!");

        // Set and write properties
        png_set_filter(png, PNG_FILTER_TYPE_BASE, PNG_ALL_FILTERS);
        png_set_compression_level(png, select_comp_level(flags & UIMGPNG_COMP_MASK));
        png_set_IHDR(
            png,
            hdrinfo,
            img->w,
            img->h,
            bit_depth,
            colour_type,
            PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_DEFAULT,
            PNG_FILTER_TYPE_DEFAULT);
        png_set_bgr(png);
        if(!UIMG_IS_BE)
            png_set_swap(png);
        png_write_info(png, hdrinfo);

        // Write image data
        uimg_begin_buf_access(img);
        for(int i = 0; i < img->h; ++i)
        {
            const uint32_t *srcrow = uimg_get_row_buf_c(img, i);
            png_write_row(png, row_bgra_to_fmt(rowbuf, srcrow, img->w, format));
        }
        uimg_end_buf_access(img);
        png_write_end(png, NULL);

        // Calculate record size
        encsize = stm.proc_size;
    }
    else
    {
        encsize = 0;
    }

    if(rowbuf)
        free(rowbuf);

    png_destroy_write_struct(&png, &hdrinfo);

    return encsize;
}

static
void on_read_data(png_structp png, png_bytep dest, png_size_t size_required)
{
    if(!stream_read(png_get_io_ptr(png), dest, size_required))
        png_error(png, "Raw data length error!");
}

size_t uimgpng_decode(uimg_t *img, const void *data, size_t size)
{
    /**
     * Decode from the PNG format data
     *
     * @param img   The image object to receive the decoded result.
     * @param data  The PNG data to be decoded.
     * @param size  Size of the input data.
     * @return Total size of data has been read to decode; or ZERO if failed.
     */
    if( !img || !data || !size ) return 0;

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop hdrinfo = png_create_info_struct(png);
    png_infop endinfo = png_create_info_struct(png);
    uint32_t **imgrows = NULL;

    size_t decsize = 0;
    if( png && hdrinfo && endinfo && 0 == setjmp(png_jmpbuf(png)) )
    {
        // Set read stream
        struct stream stm;
        stream_init(&stm, (void*) data, size);
        png_set_read_fn(png, &stm, on_read_data);

        // Read header
        png_read_info(png, hdrinfo);
        unsigned width = png_get_image_width(png, hdrinfo);
        unsigned height = png_get_image_height(png, hdrinfo);
        int bit_depth = png_get_bit_depth(png, hdrinfo);
        int color_type = png_get_color_type(png, hdrinfo);
        int interlace = png_get_interlace_type(png, hdrinfo);

        // Translate image format
        if(!UIMG_IS_BE)
            png_set_swap(png);
        if( bit_depth == 16 )
            png_set_strip_16(png);
        png_set_expand(png);
        if( !( color_type & PNG_COLOR_MASK_COLOR ) )
            png_set_gray_to_rgb(png);
        if( !( color_type & PNG_COLOR_MASK_ALPHA ) )
            png_set_add_alpha(png, 0xFF, PNG_FILLER_AFTER);
        png_set_bgr(png);
        if( interlace != PNG_INTERLACE_NONE )
            png_set_interlace_handling(png);
        png_read_update_info(png, hdrinfo);

        // Prepare image buffer
        if(uimg_resize(img, width, height))
            png_error(png, "Cannot allocate more memory!");
        if( !( imgrows = malloc( height * sizeof(uint32_t*) ) ) )
            png_error(png, "Cannot allocate more memory!");
        uimg_begin_buf_access(img);
        for(int i = 0; i < img->h; ++i)
            imgrows[i] = uimg_get_row_buf(img, i);
        uimg_end_buf_access(img);

        // Read image data
        png_read_image(png, (png_bytepp) imgrows);
        // Read the trailing information after image data
        png_read_end(png, endinfo);

        // Calculate read size
        decsize = stm.proc_size;
    }
    else
    {
        decsize = 0;
    }

    if(imgrows)
        free(imgrows);

    png_destroy_read_struct(&png, &hdrinfo, &endinfo);

    return decsize;
}

int uimgpng_save_file(const uimg_t *img, const char *filename, int flags)
{
    /**
     * Save image to file
     *
     * @param img       The image object to be saved.
     * @param filename  Name of the file.
     * @param flags     Encode options in ::uimgpng_encopt.
     *                  This parameter can be ZERO to use defaults.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename ) return false;

    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if(!img->buf) break;

        static const size_t redundancy_size = 1024;
        size_t estimate_size = img->w * img->h * sizeof(uint32_t) + redundancy_size;
        if( !( buf = malloc(estimate_size) ) ) break;

        size_t encode_size = uimgpng_encode(img, buf, estimate_size, flags);
        if(!encode_size) break;

        filebuf_save(filename, buf, encode_size);

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}

int uimgpng_load_file(uimg_t *img, const char *filename)
{
    /**
     * Load image from file
     *
     * @param img       The image object to be loaded.
     * @param filename  Name of the file.
     * @return ZERO if success; or other values indicate error occurred.
     */
    if( !img || !filename ) return false;

    size_t size = 0;
    uint8_t *buf = NULL;

    bool succ = false;
    do
    {
        if( !( buf = filebuf_load(filename, &size) ) ) break;
        if(!uimgpng_decode(img, buf, size)) break;

        succ = true;
    } while(false);

    if(buf)
        free(buf);

    return succ ? 0 : -1;
}
