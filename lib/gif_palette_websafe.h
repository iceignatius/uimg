#ifndef _GIF_PALETTE_WEBSAFE_H_
#define _GIF_PALETTE_WEBSAFE_H_

#include <gif_lib.h>
#include "uimgcolour.h"

static
const GifColorType* get_websafe_table(void *host)
{
    static GifColorType palette[256] = {0};

    static bool inited = false;
    if(inited)
        return palette;

    static const int step = 255 / 5;
    for(unsigned i = 0; i < 216; ++i)
    {
        palette[i].Red = i / 6 / 6 * step;
        palette[i].Green = i / 6 % 6 * step;
        palette[i].Blue = i % 6 * step;
    }

    inited = true;
    return palette;
}

static
unsigned count_websafe_colours(void *host)
{
    return 256;
}

static
uint8_t calc_websafe_index(void *host, uint32_t colour)
{
    unsigned a = UIMG_COLOUR_GET_A(colour);
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned b = UIMG_COLOUR_GET_B(colour);
    static const int step = 255 / 5;

    return a ? ( ( r / step * 6 + g / step ) * 6 + b / step ) : 216;
}

static
int get_websafe_transparent(void *host)
{
    return 216;
}

#endif
