#include <ctype.h>
#include <string.h>
#include "uimg_config.h"
#include "uimgfile.h"

int uimgfile_parse_type_by_name(const char *filename)
{
    /**
     * Detect file type by parsing file name extension
     *
     * @param filename The file name to be parsed.
     * @return One of the type values defined in ::uimgfile_type.
     */
    const char *pos = filename ? strrchr(filename, '.') : NULL;
    if(!pos)
        return UIMGFILE_UNSUPPORTED;

    size_t len = strlen(pos);
    char ext[len+1];
    memcpy(ext, pos, len);
    ext[len] = 0;

    for(int i = 0; i < len; ++i)
        ext[i] = tolower(ext[i]);

    if( 0 == strcmp(ext, ".bmp") )
        return UIMGFILE_BMP;
    else if( 0 == strcmp(ext, ".png") )
        return UIMGFILE_PNG;
    else if( 0 == strcmp(ext, ".jpg") )
        return UIMGFILE_JPEG;
    else if( 0 == strcmp(ext, ".jpeg") )
        return UIMGFILE_JPEG;
    else if( 0 == strcmp(ext, ".gif") )
        return UIMGFILE_GIF;
    else if( 0 == strcmp(ext, ".webp") )
        return UIMGFILE_WEBP;
    else if( 0 == strcmp(ext, ".heif") )
        return UIMGFILE_HEIF;
    else if( 0 == strcmp(ext, ".heic") )
        return UIMGFILE_HEIF;
    else
        return UIMGFILE_UNSUPPORTED;
}

int uimgfile_parse_type_by_signature(const void *data, size_t size)
{
    /**
     * Detect file type by parsing signature of file data
     *
     * @param data The raw data to be parsed.
     * @param size Size of the raw data.
     * @return One of the type values defined in ::uimgfile_type.
     */
    const uint8_t *raw = data;
    if( !raw || !size )
        return UIMGFILE_UNSUPPORTED;

    switch(raw[0])
    {
    case 'B':
        if( size >= 2 && raw[1] == 'M' )
            return UIMGFILE_BMP;
        else
            return UIMGFILE_UNSUPPORTED;

    case 0xFF:
        if( size >= 4 && raw[1] == 0xD8 && raw[2] == 0xFF )
        {
            switch(raw[3])
            {
            case 0xDB:
            case 0xE0:
            case 0xEE:
            case 0xE1:
                return UIMGFILE_JPEG;

            default:
                return UIMGFILE_UNSUPPORTED;
            }
        }
        else
        {
            return UIMGFILE_UNSUPPORTED;
        }

    case 0x89:
        if( size >= 8 && 0 == memcmp(raw, "\x89PNG\x0D\x0A\x1A\x0A", 8))
            return UIMGFILE_PNG;
        else
            return UIMGFILE_UNSUPPORTED;

    case 'G':
        if( size >= 6 && 0 == memcmp(raw, "GIF89a", 6) )
            return UIMGFILE_GIF;
        else if( size >= 6 && 0 == memcmp(raw, "GIF89a", 6) )
            return UIMGFILE_GIF;
        else
            return UIMGFILE_UNSUPPORTED;

    case 'R':
        if( size >= 12 &&
            0 == memcmp(raw, "RIFF", 4) &&
            0 == memcmp(raw + 8, "WEBP", 4) )
        {
            return UIMGFILE_WEBP;
        }
        else
        {
            return UIMGFILE_UNSUPPORTED;
        }

    case 0:
        if( size >= 12 && 0 == memcmp(raw + 4, "ftypheic", 8) )
            return UIMGFILE_HEIF;
        else
            return UIMGFILE_UNSUPPORTED;

    default:
        return UIMGFILE_UNSUPPORTED;
    }
}

int uimgfile_save_file(const uimg_t *img, const char *filename, int flags)
{
    /**
     * Save an image to a file.
     *
     * @param img       The image object to be saved.
     * @param filename  Name of the file,
     *                  The name extension will be used to
     *                  detect which file format to be used.
     * @param flags     Flags will be passed to the file encoder,
     *                  the definitions of flag values are depend on each encoder.
     *                  If not know what to set, then
     *                  just set to ZERO to use the defaults.
     * @return ZERO if success; or other values indicate error occurred.
     */
    switch(uimgfile_parse_type_by_name(filename))
    {
    case UIMGFILE_BMP:
        return uimgbmp_save_file(img, filename, flags);

#ifdef UIMG_ENABLE_PNG
    case UIMGFILE_PNG:
        return uimgpng_save_file(img, filename, flags);
#endif

#ifdef UIMG_ENABLE_JPEG
    case UIMGFILE_JPEG:
        return uimgjpg_save_file(img, filename, flags > 0 ? flags : -1);
#endif

#ifdef UIMG_ENABLE_GIF
    case UIMGFILE_GIF:
        return uimggif_hlpr_save_file(img, filename, flags);
#endif

#ifdef UIMG_ENABLE_WEBP
    case UIMGFILE_WEBP:
        return uimgwebp_save_file(img, filename, flags > 0 ? flags : -1);
#endif

#ifdef UIMG_ENABLE_HEIF
    case UIMGFILE_HEIF:
        return uimgheif_hlpr_save_file(img, filename, flags > 0 ? flags : -1);
#endif

    default:
        return -1;
    }
}

int uimgfile_load_file(uimg_t *img, const char *filename, int flags)
{
    /**
     * Load image from file
     *
     * @param img       The image object to be loaded.
     * @param filename  Name of the file.
     *                  The name extension will be used to
     *                  detect which file format to be used.
     * @param flags     Flags will be passed to the file decoder,
     *                  the definitions of flag values are depend on each decoder.
     *                  If not know what to set, then
     *                  just set to ZERO to use the defaults.
     * @return ZERO if success; or other values indicate error occurred.
     */
    switch(uimgfile_parse_type_by_name(filename))
    {
    case UIMGFILE_BMP:
        return uimgbmp_load_file(img, filename, flags);

#ifdef UIMG_ENABLE_PNG
    case UIMGFILE_PNG:
        return uimgpng_load_file(img, filename);
#endif

#ifdef UIMG_ENABLE_JPEG
    case UIMGFILE_JPEG:
        return uimgjpg_load_file(img, filename);
#endif

#ifdef UIMG_ENABLE_GIF
    case UIMGFILE_GIF:
        return uimggif_hlpr_load_file(img, filename);
#endif

#ifdef UIMG_ENABLE_WEBP
    case UIMGFILE_WEBP:
        return uimgwebp_load_file(img, filename);
#endif

#ifdef UIMG_ENABLE_HEIF
    case UIMGFILE_HEIF:
        return uimgheif_hlpr_load_file(img, filename);
#endif

    default:
        return -1;
    }
}
