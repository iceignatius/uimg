#include <stdio.h>
#include <string.h>
#include <vector>
#include "cmdopt.h"
#include "filebuf.h"
#ifdef UIMG_ENABLE_GIF
#   include "uimggif_pltgen.h"
#endif
#ifdef UIMG_ENABLE_BLITMOD
#   include "uimgblit.h"
#endif

using namespace std;

static
void PrintSupportedFileTypes(void)
{
    printf("bmp ");
#if defined(UIMG_ENABLE_JPEG) && defined(UIMG_ENABLE_JPEG_ENCODE)
    printf("jpeg jpg ");
#endif
#ifdef UIMG_ENABLE_PNG
    printf("png ");
#endif
#ifdef UIMG_ENABLE_GIF
    printf("gif ");
#endif
#ifdef UIMG_ENABLE_WEBP
    printf("webp ");
#endif
#ifdef UIMG_ENABLE_HEIF
    printf("heif heic ");
#endif
    printf("\n");
}

static
void PrintSupportedColours(uimgfile_type type)
{
    switch(type)
    {
    case UIMGFILE_BMP:
        printf("bw grey grey8 rgb rgba\n");
        break;

    case UIMGFILE_PNG:
        printf("bw grey grey8 grey8a grey16 bgr bgra\n");
        break;

    case UIMGFILE_JPEG:
        printf("rgb\n");
        break;

    case UIMGFILE_GIF:
        printf("bw grey grey8 fast332 websafe vga16 vga256 autogen\n");
        break;

    case UIMGFILE_WEBP:
        printf("bgra\n");
        break;

    case UIMGFILE_HEIF:
        printf("rgb\n");
        break;

    default:
        printf("bw grey grey8 grey8a grey16"
            " rgb bgr rgba bgra"
            " fast332 websafe vga16 vga256 autogen"
            "\n");
        break;
    }
}

static
void AppendImageList(list<Uimg> &imglist, const list<Uimg> srclist)
{
    for(Uimg img : srclist)
        imglist.push_back(img);
}

static
string SelectFilename(const string &name, FILE *file)
{
    return !name.empty() ? name :
        !file ? "<NONE>" :
        file == stdin ? "<STDIN>" :
        file == stdout ? "<STDOUT>" :
        file == stderr ? "<STDERR>" :
        "<FILE>";
}

static
const char* FileTypeToStr(uimgfile_type type)
{
    switch(type)
    {
    case UIMGFILE_BMP:
        return "BMP";

    case UIMGFILE_PNG:
        return "PNG";

    case UIMGFILE_JPEG:
        return "JPEG";

    case UIMGFILE_GIF:
        return "GIF";

    case UIMGFILE_WEBP:
        return "WEBP";

    case UIMGFILE_HEIF:
        return "HEIF";

    case UIMGFILE_UNSUPPORTED:
        return "UNSUPPORTED";

    default:
        return "UNKNOWN";
    }
}

static
Uimg DecodeSingleFrameImage(
    const uint8_t *data,
    size_t size,
    size_t(*decode_func)(uimg_t*, const void*, size_t),
    int *width,
    int *height,
    int *frame_num)
{
    Uimg img;
    if(!decode_func(img.cptr(), data, size))
        throw runtime_error("Failed to decode image file!");

    *width = img.Width();
    *height = img.Height();
    *frame_num = 1;

    return img;
}

#ifdef UIMG_ENABLE_GIF
static
list<Uimg> DecodeGifImages(
    const uint8_t *data, size_t size, int *width, int *height, int *frame_num, int *dura)
{
    list<Uimg> imglist;
    uimggif_dec_t *dec = NULL;

    bool succ = false;
    do
    {
        if( !( dec = uimggif_dec_open_mem(data, size, NULL) ) )
            break;

        *width = uimggif_dec_get_width(dec);
        *height = uimggif_dec_get_height(dec);
        *frame_num = uimggif_dec_count_image(dec);

        int decoded_num = 0, total_dura = 0;
        for(int i = 0; i < *frame_num; ++i)
        {
            Uimg img;
            int curr_dura;
            if(uimggif_dec_get_image(dec, i, img.cptr(), &curr_dura))
                break;

            imglist.push_back(img);
            total_dura += curr_dura;
            ++decoded_num;
        }

        if( decoded_num != *frame_num )
            break;

        *dura = total_dura / *frame_num;

        succ = true;
    } while(false);

    if(dec)
        uimggif_dec_close(dec);

    if(!succ)
        throw runtime_error("Failed to decode image file!");

    return imglist;
}
#endif

#ifdef UIMG_ENABLE_HEIF
static
list<Uimg> DecodeHeifImages(
    const uint8_t *data, size_t size, int *width, int *height, int *frame_num, int *dura)
{
    list<Uimg> imglist;
    uimgheif_dec_t *dec = NULL;

    bool succ = false;
    do
    {
        if( !( dec = uimgheif_dec_open_mem(data, size, NULL) ) )
            break;

        *frame_num = uimgheif_dec_count_image(dec);

        int decoded_num = 0;
        for(int i = 0; i < *frame_num; ++i)
        {
            Uimg img;
            if(uimgheif_dec_get_image(dec, i, img.cptr()))
                break;

            if( i == uimgheif_dec_get_primary_idx(dec) )
            {
                *width = img.Width();
                *height = img.Height();
            }

            imglist.push_back(img);
            ++decoded_num;
        }

        if( decoded_num != *frame_num )
            break;

        succ = true;
    } while(false);

    if(dec)
        uimgheif_dec_close(dec);

    if(!succ)
        throw runtime_error("Failed to decode image file!");

    return imglist;
}
#endif

static
list<Uimg> ReadOneFile(
    const string &name,
    FILE *file,
    uimgfile_type type,
    bool silent)
{
    uint8_t *data = NULL;
    size_t size = 0;

    int width = -1, height = -1;
    int frame_num = 0;
    int dura = -1;

    if( !data && !name.empty() &&
        !( data = static_cast<uint8_t*>(filebuf_load(name.c_str(), &size)) ) )
    {
        throw runtime_error("Failed read from \"" + name + "\"!");
    }

    if( !data && file &&
        !( data = static_cast<uint8_t*>(filebuf_load_handle(file, &size)) ) )
    {
        throw runtime_error("Failed read from \"" + SelectFilename("", file) + "\"!");
    }

    if(!data)
        throw runtime_error("No inputs!");

    if( type == UIMGFILE_UNSUPPORTED && !name.empty() )
        type = static_cast<uimgfile_type>(uimgfile_parse_type_by_name(name.c_str()));
    if( type == UIMGFILE_UNSUPPORTED )
        type = static_cast<uimgfile_type>(uimgfile_parse_type_by_signature(data, size));
    if( type == UIMGFILE_UNSUPPORTED )
        throw runtime_error("Unsupported file format!");

    list<Uimg> imglist;
    switch(type)
    {
#ifdef UIMG_ENABLE_BMP
    case UIMGFILE_BMP:
        {
            auto bmp_decode = [](uimg_t *img, const void *data, size_t size) -> size_t
            {
                return uimgbmp_decode(img, data, size, 0);
            };
            imglist.push_back(DecodeSingleFrameImage(
                data, size, bmp_decode, &width, &height, &frame_num));
        }
        break;
#endif

#ifdef UIMG_ENABLE_PNG
    case UIMGFILE_PNG:
        imglist.push_back(DecodeSingleFrameImage(
            data, size, uimgpng_decode, &width, &height, &frame_num));
        break;
#endif

#ifdef UIMG_ENABLE_JPEG
    case UIMGFILE_JPEG:
        imglist.push_back(DecodeSingleFrameImage(
            data, size, uimgjpg_decode, &width, &height, &frame_num));
        break;
#endif

#ifdef UIMG_ENABLE_GIF
    case UIMGFILE_GIF:
        AppendImageList(imglist, DecodeGifImages(
            data, size, &width, &height, &frame_num, &dura));
        break;
#endif

#ifdef UIMG_ENABLE_WEBP
    case UIMGFILE_WEBP:
        imglist.push_back(DecodeSingleFrameImage(
            data, size, uimgwebp_decode, &width, &height, &frame_num));
        break;
#endif

#ifdef UIMG_ENABLE_HEIF
    case UIMGFILE_HEIF:
        AppendImageList(imglist, DecodeHeifImages(
            data, size, &width, &height, &frame_num, &dura));
        break;
#endif

    default:
        throw runtime_error("Unsupported file format!");
        break;
    }

    if(!silent)
    {
        printf("File: \"%s\"\n", SelectFilename(name, file).c_str());
        printf("  Type: %s\n", FileTypeToStr(type));
        printf("  Resolution: %dx%d\n", width, height);
        printf("  Frames: %d\n", frame_num);
        if( dura >= 0 )
            printf("  Duration: %d (ms)\n", dura);
        printf("\n");
    }

    if(data)
        free(data);

    return imglist;
}

static
list<Uimg> ReadImageFiles(const list<string> &filelist, uimgfile_type type, bool silent)
{
    list<Uimg> imglist;

    for(const string &name : filelist)
        AppendImageList(imglist, ReadOneFile(name, NULL, type, silent));

    return imglist;
}

static
list<Uimg> ReadImageStdin(uimgfile_type type, bool silent)
{
    list<Uimg> imglist;

    AppendImageList(imglist, ReadOneFile("", stdin, type, silent));

    return imglist;
}

static
list<Uimg> ExtractImageList(const list<Uimg> &srclist, int index)
{
    if( index < 0 )
        return srclist;
    if( index >= (int) srclist.size() )
        throw runtime_error(
            "Index " + to_string(index) + "/" + to_string(srclist.size()-1) + " out of range!");

    auto iter = srclist.begin();
    for(int i = 0; i < index; ++i)
        ++iter;

    list<Uimg> dstlist;
    dstlist.push_back(*iter);

    return dstlist;
}

#ifdef UIMG_ENABLE_BLITMOD
void ResizeImageByReso(list<Uimg> &imglist, unsigned width, unsigned height)
{
    for(Uimg &img : imglist)
    {
        if(uimgblit_stretch(img.cptr(), width, height, UIMG_SCALE_LINEAR))
        {
            throw runtime_error(
                "Failed resize image resolution to " +
                to_string(width) + "x" + to_string(height) + "!");
        }
    }
}
#endif

#ifdef UIMG_ENABLE_BLITMOD
void ResizeImageByScale(list<Uimg> &imglist, unsigned scale, bool silent)
{
    for(Uimg &img : imglist)
    {
        unsigned width = img.Width() * scale / 100;
        unsigned height = img.Height() * scale / 100;
        width = width > 0 ? width : 1;
        height = height > 0 ? height : 1;

        if(!silent)
            printf("Resize %ux%u -> %ux%u\n", img.Width(), img.Height(), width, height);

        if(uimgblit_stretch(img.cptr(), width, height, UIMG_SCALE_LINEAR))
        {
            throw runtime_error(
                "Failed resize image resolution to " +
                to_string(width) + "x" + to_string(height) + "!");
        }
    }
}
#endif

#if defined(UIMG_ENABLE_GIF) || defined(UIMG_ENABLE_HEIF)
static
bool CheckImagesDimension(const list<Uimg> &imglist)
{
    if(imglist.empty())
        return false;

    int width = imglist.front().Width();
    int height = imglist.front().Height();

    for(const Uimg &img : imglist)
    {
        if( img.Width() != width || img.Height() != height )
            return false;
    }

    return true;
}
#endif

#ifdef UIMG_ENABLE_BMP
static
vector<uint8_t> EncodeBmpImage(const Uimg &img, const cmdopt &opts)
{
    int flags = 0;
    switch(opts.colour)
    {
    case CMDOPT_COLOUR_DEFAULT:
        flags = UIMGBMP_ENC_DEFAULT;
        break;

    case CMDOPT_COLOUR_BW:
        flags = UIMGBMP_ENC_BW;
        break;

    case CMDOPT_COLOUR_GREY8:
    case CMDOPT_COLOUR_GREY:
        flags = UIMGBMP_ENC_GREY;
        break;

    case CMDOPT_COLOUR_RGB:
        flags = UIMGBMP_ENC_RGB;
        break;

    case CMDOPT_COLOUR_RGBA:
        flags = UIMGBMP_ENC_RGBA;
        break;

    default:
        throw runtime_error("Does not support this colour format for BMP!");
        break;
    }

    if(opts.rev_rows)
        flags |= UIMGBMP_ENC_TOPDOWN;

    vector<uint8_t> buf( 4096 + img.Width() * img.Height() * 4 );
    size_t size = uimgbmp_encode(img, buf.data(), buf.size(), flags);
    if(size)
        buf.resize(size);
    else
        throw runtime_error("Failed to encode BMP!");

    return buf;
}
#endif

#ifdef UIMG_ENABLE_PNG
static
vector<uint8_t> EncodePngImage(const Uimg &img, const cmdopt &opts)
{
    int flags = 0;

    switch(opts.colour)
    {
    case CMDOPT_COLOUR_BW:
        flags |= UIMGPNG_BW;
        break;

    case CMDOPT_COLOUR_GREY8:
        flags |= UIMGPNG_GREY8;
        break;

    case CMDOPT_COLOUR_GREY8A:
        flags |= UIMGPNG_GREY8A;
        break;

    case CMDOPT_COLOUR_GREY16:
        flags |= UIMGPNG_GREY16;
        break;

    case CMDOPT_COLOUR_GREY:
        flags |= UIMGPNG_GREY;
        break;

    case CMDOPT_COLOUR_BGR:
        flags |= UIMGPNG_BGR;
        break;

    case CMDOPT_COLOUR_BGRA:
        flags |= UIMGPNG_BGRA;
        break;

    case CMDOPT_COLOUR_DEFAULT:
        break;

    default:
        throw runtime_error("Does not support this colour format for PNG!");
        break;
    }

    switch(opts.compress)
    {
    case CMDOPT_COMPRESS_NONE:
        flags |= UIMGPNG_COMP_NO;
        break;

    case CMDOPT_COMPRESS_FAST:
        flags |= UIMGPNG_COMP_FAST;
        break;

    case CMDOPT_COMPRESS_BEST:
        flags |= UIMGPNG_COMP_BEST;
        break;

    case CMDOPT_COMPRESS_DEFAULT:
        break;

    default:
        throw runtime_error("Does not support this compression method for PNG!");
        break;
    }

    vector<uint8_t> buf( 4096 + img.Width() * img.Height() * 4 );
    size_t size = uimgpng_encode(img, buf.data(), buf.size(), flags);
    if(size)
        buf.resize(size);
    else
        throw runtime_error("Failed to encode PNG!");

    return buf;
}
#endif

#ifdef UIMG_ENABLE_JPEG
static
vector<uint8_t> EncodeJpegImage(const Uimg &img, const cmdopt &opts)
{
    if( opts.colour != CMDOPT_COLOUR_RGB && opts.colour != CMDOPT_COLOUR_DEFAULT )
        throw runtime_error("Does not support this colour format for JPEG!");

    int flags = 0;
    switch(opts.quality)
    {
    case CMDOPT_QUALITY_WORST:
        flags = UIMGJPG_QUALITY_WORST;
        break;

    case CMDOPT_QUALITY_LOW:
        flags = UIMGJPG_QUALITY_LOW;
        break;

    case CMDOPT_QUALITY_MEDIUM:
        flags = UIMGJPG_QUALITY_MEDIUM;
        break;

    case CMDOPT_QUALITY_HIGH:
        flags = UIMGJPG_QUALITY_HIGH;
        break;

    case CMDOPT_QUALITY_BEST:
        flags = UIMGJPG_QUALITY_BEST;
        break;

    case CMDOPT_QUALITY_DEFAULT:
        flags = -1;
        break;

    case CMDOPT_QUALITY_LOSSLESS:
        throw runtime_error("Does not support lossless compresion for JPEG!");
        break;

    default:
        if( CMDOPT_QUALITY_MANUAL_MIN <= opts.quality &&
            opts.quality <= CMDOPT_QUALITY_MANUAL_MAX )
        {
            flags = opts.quality;
        }
        else
        {
            throw runtime_error("Quality value is out of range!");
        }
    }

    vector<uint8_t> buf( 4096 + img.Width() * img.Height() * 3 );
    size_t size = uimgjpg_encode(img, buf.data(), buf.size(), flags);
    if(size)
        buf.resize(size);
    else
        throw runtime_error("Failed to encode JPEG!");

    return buf;
}
#endif

#ifdef UIMG_ENABLE_GIF
static
vector<uint8_t> EncodeGifImage(const list<Uimg> &imglist, const cmdopt &opts)
{
    if(!CheckImagesDimension(imglist))
        throw runtime_error("Dimension of input images are not equal!");

    int flags = 0;
    switch(opts.colour)
    {
    case CMDOPT_COLOUR_DEFAULT:
        break;

    case CMDOPT_COLOUR_BW:
        flags = UIMGGIF_PALETTE_BW;
        break;

    case CMDOPT_COLOUR_GREY8:
    case CMDOPT_COLOUR_GREY:
        flags = UIMGGIF_PALETTE_GREY;
        break;

    case CMDOPT_COLOUR_FAST332:
        flags = UIMGGIF_PALETTE_FAST332;
        break;

    case CMDOPT_COLOUR_WEBSAFE:
        flags = UIMGGIF_PALETTE_WEBSAFE;
        break;

    case CMDOPT_COLOUR_VGA16:
        flags = UIMGGIF_PALETTE_VGA16;
        break;

    case CMDOPT_COLOUR_VGA256:
        flags = UIMGGIF_PALETTE_VGA256;
        break;

    case CMDOPT_COLOUR_AUTOGEN:
        flags = -1;
        break;

    default:
        throw runtime_error("Does not support this colour format for GIF!");
    }

    vector<uint8_t> buf(
        4096 +
        imglist.size() *
        imglist.front().Width() * imglist.front().Height() );

    uimggif_pltgen_t *pltgen = NULL;
    uimggif_enc_t *enc = NULL;

    bool succ = false;
    do
    {
        if( flags < 0 )
        {
            if(!( pltgen = uimggif_pltgen_create() ))
                break;

            for(const Uimg &img : imglist)
            {
                if(uimggif_pltgen_parse_image(pltgen, img))
                    break;
            }

            if( 0 > ( flags = uimggif_pltgen_get_id(pltgen) ) )
                break;
        }

        if(!( enc = uimggif_enc_open_mem(buf.data(), buf.size(), flags) ))
            break;

        for(const Uimg &img : imglist)
        {
            if(uimggif_enc_add_image(enc, img, opts.duration))
                break;
        }

        if( uimggif_enc_count_image(enc) != (int) imglist.size() )
            break;

        size_t size = uimggif_enc_write(enc);
        if(size)
            buf.resize(size);
        else
            break;

        succ = true;
    } while(false);

    if(enc)
        uimggif_enc_close(enc);
    if(pltgen)
        uimggif_pltgen_release(pltgen);

    if(!succ)
        throw runtime_error("Failed to encode GIF!");

    return buf;
}
#endif

#ifdef UIMG_ENABLE_WEBP
static
vector<uint8_t> EncodeWebpImage(const Uimg &img, const cmdopt &opts)
{
    if( opts.colour != CMDOPT_COLOUR_BGRA && opts.colour != CMDOPT_COLOUR_DEFAULT )
        throw runtime_error("Does not support this colour format for WebP!");

    int flags = 0;
    switch(opts.quality)
    {
    case CMDOPT_QUALITY_WORST:
        flags = UIMGWEBP_QUALITY_WORST;
        break;

    case CMDOPT_QUALITY_LOW:
        flags = UIMGWEBP_QUALITY_LOW;
        break;

    case CMDOPT_QUALITY_MEDIUM:
        flags = UIMGWEBP_QUALITY_MEDIUM;
        break;

    case CMDOPT_QUALITY_HIGH:
        flags = UIMGWEBP_QUALITY_HIGH;
        break;

    case CMDOPT_QUALITY_BEST:
        flags = UIMGWEBP_QUALITY_BEST;
        break;

    case CMDOPT_QUALITY_LOSSLESS:
        flags = UIMGWEBP_QUALITY_LOSSLESS;
        break;

    case CMDOPT_QUALITY_DEFAULT:
        flags = -1;
        break;

    default:
        if( CMDOPT_QUALITY_MANUAL_MIN <= opts.quality &&
            opts.quality <= CMDOPT_QUALITY_MANUAL_MAX )
        {
            flags = opts.quality;
        }
        else
        {
            throw runtime_error("Quality value is out of range!");
        }
    }

    vector<uint8_t> buf( 4096 + img.Width() * img.Height() * 4 );
    size_t size = uimgwebp_encode(img, buf.data(), buf.size(), flags);
    if(size)
        buf.resize(size);
    else
        throw runtime_error("Failed to encode WebP!");

    return buf;
}
#endif

#ifdef UIMG_ENABLE_HEIF
static
vector<uint8_t> EncodeHeifImage(const list<Uimg> &imglist, const cmdopt &opts)
{
    if(!CheckImagesDimension(imglist))
        throw runtime_error("Dimension of input images are not equal!");

    if( opts.colour != CMDOPT_COLOUR_RGB && opts.colour != CMDOPT_COLOUR_DEFAULT )
        throw runtime_error("Does not support this colour format for HEIF/HEIC!");

    int flags = 0;
    switch(opts.quality)
    {
    case CMDOPT_QUALITY_WORST:
        flags = UIMGHEIF_QUALITY_WORST;
        break;

    case CMDOPT_QUALITY_LOW:
        flags = UIMGHEIF_QUALITY_LOW;
        break;

    case CMDOPT_QUALITY_MEDIUM:
        flags = UIMGHEIF_QUALITY_MEDIUM;
        break;

    case CMDOPT_QUALITY_HIGH:
        flags = UIMGHEIF_QUALITY_HIGH;
        break;

    case CMDOPT_QUALITY_BEST:
        flags = UIMGHEIF_QUALITY_BEST;
        break;

    case CMDOPT_QUALITY_LOSSLESS:
        flags = UIMGHEIF_QUALITY_LOSSLESS;
        break;

    case CMDOPT_QUALITY_DEFAULT:
        flags = -1;
        break;

    default:
        if( CMDOPT_QUALITY_MANUAL_MIN <= opts.quality &&
            opts.quality <= CMDOPT_QUALITY_MANUAL_MAX )
        {
            flags = opts.quality;
        }
        else
        {
            throw runtime_error("Quality value is out of range!");
        }
    }

    vector<uint8_t> buf(
        4096 +
        imglist.size() *
        imglist.front().Width() * imglist.front().Height() * 3 );
    uimgheif_enc_t *enc = NULL;

    bool succ = false;
    do
    {
        if( !( enc = uimgheif_enc_create(flags) ) )
            break;

        for(const Uimg &img : imglist)
        {
            if(uimgheif_enc_add_image(enc, img))
                break;
        }

        if( uimgheif_enc_count_image(enc) != (int) imglist.size() )
            break;

        size_t size = uimgheif_enc_write_mem(enc, buf.data(), buf.size());
        if(size)
            buf.resize(size);
        else
            break;

        succ = true;
    } while(false);

    if(enc)
        uimgheif_enc_release(enc);

    if(!succ)
        throw runtime_error("Failed to encode HEIF/HEIC!");

    return buf;
}
#endif

static
vector<uint8_t> EncodeImage(const list<Uimg> &imglist, const cmdopt &opts)
{
    if(imglist.empty())
        throw runtime_error("No input images!");

    uimgfile_type filetype = opts.to_type;
    if( filetype == UIMGFILE_UNSUPPORTED )
    {
        filetype = static_cast<uimgfile_type>(
            uimgfile_parse_type_by_name(opts.out_name.c_str()));
    }

    switch(filetype)
    {
#ifdef UIMG_ENABLE_BMP
    case UIMGFILE_BMP:
        return EncodeBmpImage(imglist.front(), opts);
#endif

#ifdef UIMG_ENABLE_PNG
    case UIMGFILE_PNG:
        return EncodePngImage(imglist.front(), opts);
#endif

#ifdef UIMG_ENABLE_JPEG
    case UIMGFILE_JPEG:
        return EncodeJpegImage(imglist.front(), opts);
#endif

#ifdef UIMG_ENABLE_GIF
    case UIMGFILE_GIF:
        return EncodeGifImage(imglist, opts);
#endif

#ifdef UIMG_ENABLE_WEBP
    case UIMGFILE_WEBP:
        return EncodeWebpImage(imglist.front(), opts);
#endif

#ifdef UIMG_ENABLE_HEIF
    case UIMGFILE_HEIF:
        return EncodeHeifImage(imglist, opts);
#endif

    case UIMGFILE_UNSUPPORTED:
    default:
        throw runtime_error("Unsupported file format!");
        break;
    }

    return vector<uint8_t>();
}

int main(int argc, char *argv[])
{
    try
    {
        int err;

        cmdopt opts;
        cmdopt_init(opts);
        if(( err = cmdopt_parse_args(opts, argc, argv) ))
            throw runtime_error(strerror(err));

        if( opts.list_types || opts.list_colours )
        {
            if(opts.list_types)
                PrintSupportedFileTypes();
            if(opts.list_colours)
                PrintSupportedColours(opts.to_type);
            return 0;
        }

        list<Uimg> imglist =
            ReadImageFiles(opts.in_list, opts.from_type, opts.silent);
        if(imglist.empty())
            imglist = ReadImageStdin(opts.from_type, opts.silent);
        if(imglist.empty())
            throw runtime_error("No input files!");

        // If no output be required, then the work is done here!
        if( opts.out_name.empty() && opts.to_type == UIMGFILE_UNSUPPORTED )
            return 0;

        imglist = ExtractImageList(imglist, opts.index);

        if( opts.resize_scale > 0 ||
            ( opts.resize_width > 0 && opts.resize_height > 0 ) )
        {
#ifdef UIMG_ENABLE_BLITMOD
            if( opts.resize_scale > 0 )
            {
                if(!opts.silent)
                    printf("Resize by scale %d%%\n", opts.resize_scale);
                ResizeImageByScale(imglist, opts.resize_scale, opts.silent);
            }
            else
            {
                if(!opts.silent)
                    printf("Resize to resolution %dx%d\n", opts.resize_width, opts.resize_height);
                ResizeImageByReso(imglist, opts.resize_width, opts.resize_height);
            }
#else
            throw runtime_error("The resize function is not supported!");
#endif
        }

        vector<uint8_t> outdata = EncodeImage(imglist, opts);
        if(!opts.silent)
            printf("Output data encoded: size=%zu\n", outdata.size());

        if(opts.out_name.empty())
            fwrite(outdata.data(), 1, outdata.size(), stdout);
        else if(!filebuf_save(opts.out_name.c_str(), outdata.data(),  outdata.size()))
            throw runtime_error("Failed to write file \"" + opts.out_name + "\"!");
        else if(!opts.silent)
            printf("Output file: \"%s\"\n", opts.out_name.c_str());
    }
    catch(exception &e)
    {
        fprintf(stderr, "ERROR: %s\n", e.what());
        return 1;
    }

    return 0;
}
