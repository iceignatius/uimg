#include <string.h>
#include <argp.h>
#include "cmdopt.h"

using namespace std;

#define CEIL_PAGESIZE(x) ( ( (x) + 4096 - 1 ) & ~( 4096 - 1 ) )

#define HIDDEN(c) ( 0x80 | (c) )

static const struct argp_option options[] =
{
    /*
     * { name, key, argument, flags, document, group },
     */
    { NULL, 0, NULL, 0, "Input/Output", 0 },
    {
        "output",
        'o',
        "file",
        0,
        "Specify the output file name."
        " If does not present this option, then"
        " the output will be print to the standard output.",
        0
    },
    {
        "INPUT-FILE",
        0,
        NULL,
        OPTION_DOC,
        "Name of the input file(s) to read from."
        " If the output file format does not support multi-frames feature, then"
        " only the first input will be used."
        " If no input file names be presented, then"
        " application will try to read from the standard input."
        " When reading image data from the standard input,"
        " only the first file will be read!",
        0
    },
    { NULL, 0, NULL, 0, "File Format", 0 },
    {
        "from-format",
        'f',
        "format",
        0,
        "Specify format of the input."
        " If does not present this option, then"
        " the format will be auto-detected by the input file name extension, then"
        " the file signature."
        " If this option is presented, then"
        " all input files must have the same format!",
        0
    },
    {
        "to-format",
        't',
        "format",
        0,
        "Specify format of the output."
        " If does not present this option, then"
        " the format will be auto-detected by the output file name extension."
        " if both this option and the output option are not presented, then"
        " no output will be generated.",
        0
    },
    {
        "list-formats",
        HIDDEN('l'),
        NULL,
        0,
        "Print the supported file formats.",
        0
    },
    { NULL, 0, NULL, 0, "Multi Frames", 0 },
    {
        "index",
        HIDDEN('I'),
        "num",
        0,
        "Specify which frame to be used to generate the output."
        " The index begin from 0, and the default is 0."
        " This option may be useful to"
        " extract frame form a multi-frames format of image (GIF, HEIC, ...),"
        " then output to the format which does not support multi-frames."
        " If this index is out of range of the first input file or"
        " the first input file does not support multi-frames feature, then"
        " the behaviour is undefined!",
        0
    },
    {
        "duration",
        HIDDEN('D'),
        "msec",
        0,
        "Specify duration of the single frame of the output."
        " This option is available for"
        " the output formats whiches support the multi-frames.",
        0
    },
    { NULL, 0, NULL, 0, "Lossy Compression", 0 },
    {
        "quality",
        HIDDEN('Q'),
        "value",
        0,
        "Specify the quality (0-100) of lossy compression.",
        0
    },
    {
        "quality-worst",
        HIDDEN('W'),
        NULL,
        0,
        "Auto select a value of worst quality (smallest size).",
        0
    },
    {
        "quality-low",
        HIDDEN('L'),
        NULL,
        0,
        "Auto select a value of low quality.",
        0
    },
    {
        "quality-medium",
        HIDDEN('M'),
        NULL,
        0,
        "Auto select a value of medium quality (balanced quality and size).",
        0
    },
    {
        "quality-high",
        HIDDEN('H'),
        NULL,
        0,
        "Auto select a value of high quality.",
        0
    },
    {
        "quality-best",
        HIDDEN('E'),
        NULL,
        0,
        "Auto select a value of best quality (largest size).",
        0
    },
    {
        "lossless",
        HIDDEN('S'),
        NULL,
        0,
        "Specify to use the lossless compression for"
        " the usually be lossy compressed format."
        " This option may not be valid for every lossy compression format.",
        0
    },
    { NULL, 0, NULL, 0, "Lossless Compression", 0 },
    {
        "compress-best",
        HIDDEN('B'),
        NULL,
        0,
        "Use the best compression (spend more time) for the lossless format.",
        0
    },
    {
        "compress-fast",
        HIDDEN('F'),
        NULL,
        0,
        "Use the fast compression (spend less time) for the lossless format.",
        0
    },
    {
        "no-compress",
        HIDDEN('N'),
        NULL,
        0,
        "Do not compress for the lossless format.",
        0
    },
    { NULL, 0, NULL, 0, "Colour Format", 0 },
    {
        "colour",
        'c',
        "format",
        0,
        "Specify which colour format will be used to encode the output.",
        0
    },
    {
        "list-colours",
        HIDDEN('C'),
        NULL,
        0,
        "Print the supported colour formats."
        " If the \"-t,--to-format\" option is set, then"
        " it will only print the colour formats whiches"
        " be supported by that file format.",
        0
    },
    { NULL, 0, NULL, 0, "Others", 0 },
    {
        "reverse-rows",
        HIDDEN('R'),
        NULL,
        0,
        "Encode image rows in top-down sequence"
        " instead of the default bottom-up sequence."
        " This is the BMP format specified option,"
        " and will be ignored if the output format is not BMP.",
        0
    },
    {
        "resize",
        HIDDEN('Z'),
        "reso",
        0,
        "Resize (stretch) image."
        " The 'reso' value have two available formats:"
        " the combined width and height description with a 'x' character,"
        " and the stretch scale with a '%' character."
        " for examples: '1024x768', '640x480', '35%', '250%'."
        " And note that, the scale value does not support the floating formats.",
        0
    },
    {
        "silent",
        's',
        NULL,
        0,
        "Do not print any message, and"
        " also not ask if overwrite the existed output file.",
        0
    },
    {0}
};

static
int str_to_uint(const char *str, int minval, int maxval)
{
    char *endpos = NULL;
    long val = str ? strtol(str, &endpos, 10) : -1;
    return *endpos == 0 && minval <= val && val <= maxval ? val : -1;
}

static
uimgfile_type str_to_filetype(const string &str)
{
    if( str == "bmp" )
        return UIMGFILE_BMP;
    else if( str == "jpeg" )
        return UIMGFILE_JPEG;
    else if( str == "jpg" )
        return UIMGFILE_JPEG;
    else if( str == "png" )
        return UIMGFILE_PNG;
    else if( str == "gif" )
        return UIMGFILE_GIF;
    else if( str == "webp" )
        return UIMGFILE_WEBP;
    else if( str == "heif" )
        return UIMGFILE_HEIF;
    else if( str == "heic" )
        return UIMGFILE_HEIF;
    else
        return UIMGFILE_UNSUPPORTED;
}

static
cmdopt_colour str_to_colour(const string &str)
{
    if( str == "bw" )
        return CMDOPT_COLOUR_BW;
    else if( str == "grey8" )
        return CMDOPT_COLOUR_GREY8;
    else if( str == "grey8a" )
        return CMDOPT_COLOUR_GREY8A;
    else if( str == "grey16" )
        return CMDOPT_COLOUR_GREY16;
    else if( str == "grey" )
        return CMDOPT_COLOUR_GREY;
    else if( str == "rgb" )
        return CMDOPT_COLOUR_RGB;
    else if( str == "bgr" )
        return CMDOPT_COLOUR_BGR;
    else if( str == "rgba" )
        return CMDOPT_COLOUR_RGBA;
    else if( str == "bgra" )
        return CMDOPT_COLOUR_BGRA;
    else if( str == "fast332" )
        return CMDOPT_COLOUR_FAST332;
    else if( str == "websafe" )
        return CMDOPT_COLOUR_WEBSAFE;
    else if( str == "vga16" )
        return CMDOPT_COLOUR_VGA16;
    else if( str == "vga256" )
        return CMDOPT_COLOUR_VGA256;
    else if( str == "autogen" )
        return CMDOPT_COLOUR_AUTOGEN;
    else
        return CMDOPT_COLOUR_INVALID;
}

static
int resostr_get_width(const char *str)
{
    const char *markpos = strchr(str, 'x');
    if(!markpos)
        return -1;

    size_t len = markpos - str;
    if(!len)
        return -1;

    char buf[ len + 1 ] = {0};
    memcpy(buf, str, len);

    char *endpos;
    long val = strtol(buf, &endpos, 10);
    if(*endpos)
        return -1;

    return val;
}

static
int resostr_get_height(const char *str)
{
    const char *markpos = strchr(str, 'x');
    if(!markpos)
        return -1;

    char *endpos;
    long val = strtol(markpos + 1, &endpos, 10);
    if(*endpos)
        return -1;

    return val;
}

static
int resostr_get_scale(const char *str)
{
    const char *markpos = strchr(str, '%');
    if(!markpos)
        return -1;

    char *endpos;
    long val = strtol(str, &endpos, 10);
    if( endpos != markpos )
        return -1;

    return val;
}

static
error_t key_parser(int key, char *arg, argp_state *state)
{
    cmdopt *info = static_cast<cmdopt*>(state->input);

    switch(key)
    {
    case ARGP_KEY_INIT:
    case ARGP_KEY_ARGS:
    case ARGP_KEY_NO_ARGS:
    case ARGP_KEY_END:
    case ARGP_KEY_SUCCESS:
    case ARGP_KEY_ERROR:
    case ARGP_KEY_FINI:
        return 0;

    case ARGP_KEY_ARG:
        info->in_list.push_back(arg);
        return 0;

    case 'o':
        info->out_name = arg;
        return 0;

    case 'f':
        if( UIMGFILE_UNSUPPORTED != ( info->from_type = str_to_filetype(arg) ) )
            return 0;
        else
            return EINVAL;

    case 't':
        if( UIMGFILE_UNSUPPORTED != ( info->to_type = str_to_filetype(arg) ) )
            return 0;
        else
            return EINVAL;

    case HIDDEN('l'):
        info->list_types = true;
        return 0;

    case HIDDEN('I'):
        if( 0 <= ( info->index = str_to_uint(arg, 0, INT_MAX) ) )
            return 0;
        else
            return EINVAL;

    case HIDDEN('D'):
        if( 0 <= ( info->duration = str_to_uint(arg, 0, INT_MAX) ) )
            return 0;
        else
            return EINVAL;

    case HIDDEN('Q'):
        if( 0 <= ( info->quality = static_cast<cmdopt_quality>(str_to_uint(
            arg, CMDOPT_QUALITY_MANUAL_MIN, CMDOPT_QUALITY_MANUAL_MAX)) ) )
        {
            return 0;
        }
        else
        {
            return EINVAL;
        }

    case HIDDEN('W'):
        info->quality = CMDOPT_QUALITY_WORST;
        return 0;

    case HIDDEN('L'):
        info->quality = CMDOPT_QUALITY_LOW;
        return 0;

    case HIDDEN('M'):
        info->quality = CMDOPT_QUALITY_MEDIUM;
        return 0;

    case HIDDEN('H'):
        info->quality = CMDOPT_QUALITY_HIGH;
        return 0;

    case HIDDEN('E'):
        info->quality = CMDOPT_QUALITY_BEST;
        return 0;

    case HIDDEN('S'):
        info->quality = CMDOPT_QUALITY_LOSSLESS;
        return 0;

    case HIDDEN('B'):
        info->compress = CMDOPT_COMPRESS_BEST;
        return 0;

    case HIDDEN('F'):
        info->compress = CMDOPT_COMPRESS_FAST;
        return 0;

    case HIDDEN('N'):
        info->compress = CMDOPT_COMPRESS_NONE;
        return 0;

    case 'c':
        if( CMDOPT_COLOUR_INVALID != ( info->colour = str_to_colour(arg) ) )
            return 0;
        else
            return EINVAL;

    case HIDDEN('C'):
        info->list_colours = true;
        return 0;

    case HIDDEN('R'):
        info->rev_rows = true;
        return 0;

    case HIDDEN('Z'):
        if(strchr(arg, 'x'))
        {
            info->resize_width = resostr_get_width(arg);
            info->resize_height = resostr_get_height(arg);
            return info->resize_width > 0 && info->resize_height > 0 ?
                0 : EINVAL;
        }
        else if(strchr(arg, '%'))
        {
            info->resize_scale = resostr_get_scale(arg);
            return info->resize_scale > 0 ? 0 : EINVAL;
        }
        else
        {
            return EINVAL;
        }

    case 's':
        info->silent = true;
        return 0;

    default:
        return EINVAL;
    }
}

static const struct argp argp =
{
    .options = options,
    .parser = key_parser,
    .args_doc = "[INPUT-FILE...]",
    .doc = "Convert image file to another format.",
    .children = NULL,
    .help_filter = NULL,
    .argp_domain = NULL,
};


void cmdopt_init(cmdopt &info)
{
    info.in_list.clear();
    info.out_name = "";
    info.from_type = UIMGFILE_UNSUPPORTED;
    info.to_type = UIMGFILE_UNSUPPORTED;
    info.list_types = false;
    info.index = -1;
    info.duration = -1;
    info.quality = CMDOPT_QUALITY_DEFAULT;
    info.compress = CMDOPT_COMPRESS_DEFAULT;
    info.colour = CMDOPT_COLOUR_DEFAULT;
    info.list_colours = false;
    info.rev_rows = false;
    info.resize_width = -1;
    info.resize_height = -1;
    info.resize_scale = -1;
    info.silent = false;
}

int cmdopt_parse_args(cmdopt &info, int argc, char *argv[])
{
    return argp_parse(&argp, argc, argv, 0, NULL, &info);
}
