#ifndef _CMDOPT_H_
#define _CMDOPT_H_

#include <list>
#include "uimgfile.h"

enum cmdopt_quality
{
    CMDOPT_QUALITY_MANUAL_MIN   = 0,
    CMDOPT_QUALITY_MANUAL_MAX   = 100,
    CMDOPT_QUALITY_WORST        = 1001,
    CMDOPT_QUALITY_LOW          = 1002,
    CMDOPT_QUALITY_MEDIUM       = 1003,
    CMDOPT_QUALITY_HIGH         = 1004,
    CMDOPT_QUALITY_BEST         = 1005,
    CMDOPT_QUALITY_LOSSLESS     = 1006,
    CMDOPT_QUALITY_DEFAULT      = 1007,
};

enum cmdopt_compress
{
    CMDOPT_COMPRESS_NONE,
    CMDOPT_COMPRESS_FAST,
    CMDOPT_COMPRESS_BEST,
    CMDOPT_COMPRESS_DEFAULT,
};

enum cmdopt_colour
{
    CMDOPT_COLOUR_INVALID = -1,
    CMDOPT_COLOUR_DEFAULT = 0,
    CMDOPT_COLOUR_BW,
    CMDOPT_COLOUR_GREY8,
    CMDOPT_COLOUR_GREY8A,
    CMDOPT_COLOUR_GREY16,
    CMDOPT_COLOUR_GREY,
    CMDOPT_COLOUR_RGB,
    CMDOPT_COLOUR_BGR,
    CMDOPT_COLOUR_RGBA,
    CMDOPT_COLOUR_BGRA,
    CMDOPT_COLOUR_FAST332,
    CMDOPT_COLOUR_WEBSAFE,
    CMDOPT_COLOUR_VGA16,
    CMDOPT_COLOUR_VGA256,
    CMDOPT_COLOUR_AUTOGEN,
};

struct cmdopt
{
    std::list<std::string> in_list;
    std::string out_name;
    uimgfile_type from_type;
    uimgfile_type to_type;
    bool list_types;
    int index;
    int duration;
    cmdopt_quality quality;
    cmdopt_compress compress;
    cmdopt_colour colour;
    bool list_colours;
    bool rev_rows;
    int resize_width;
    int resize_height;
    int resize_scale;   // in percentage (%)
    bool silent;
};

void cmdopt_init(cmdopt &info);
int cmdopt_parse_args(cmdopt &info, int argc, char *argv[]);

#endif
