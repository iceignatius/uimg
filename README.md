# Universal Image (uimg)

A simple image library to support the
universal image object to
exchange image data between software modules.

This library is designed to build a common image object type to
make software modules exchange image data easly
(althought that maybe all of the projects
which using this image library are created by me).

For the efficiency reason,
this image library can call the hareware acceleration functions behide.
The hardware acceleration methods of the local hosts are called "driver" here,
in the other word, the drivers encapsulate local system/framework/hardware functions
to help this library be more efficiency.

To simplify the data exchange problem,
this library does not implement the data and buffer convert method
but reject operations between different drivers.
It means that all software modules which share images by this library
must use the save driver, and
that is one of the reason why the library
bind drivers at compile time but not the runtime.
For the same reason, build this library to a shared library may be better to
build to a static library in most cases.
