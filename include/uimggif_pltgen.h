/**
 * @file
 * @brief       GIF palette generator
 * @author      王文佑
 * @date        2024/08/26
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_GIF_PALETTE_GENERATOR_H_
#define _UIMG_GIF_PALETTE_GENERATOR_H_

#include "uimg.h"
#include "uimggif_palette.h"

#ifdef __cplusplus
extern "C" {
#endif

/// GIF palette generator type
typedef struct uimggif_pltgen uimggif_pltgen_t;

uimggif_pltgen_t* uimggif_pltgen_create(void);
void uimggif_pltgen_release(uimggif_pltgen_t *self);

void uimggif_pltgen_clear_parser(uimggif_pltgen_t *self);
int uimggif_pltgen_parse_image(uimggif_pltgen_t *self, const uimg_t *img);
int uimggif_pltgen_update_palette(uimggif_pltgen_t *self);
int uimggif_pltgen_get_id(uimggif_pltgen_t *self);

const struct GifColorType* uimggif_pltgen_get_table(struct uimggif_palette *host);
unsigned uimggif_pltgen_count_colours(struct uimggif_palette *host);
uint8_t uimggif_pltgen_calc_index(struct uimggif_palette *host, uint32_t colour);
int uimggif_pltgen_get_transparent(struct uimggif_palette *host);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
