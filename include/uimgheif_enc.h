/**
 * @file
 * @brief       HEIF/HEIC file encoder
 * @author      王文佑
 * @date        2023/11/23
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_HEIF_ENCODER_H_
#define _UIMG_HEIF_ENCODER_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Predefined HEIF quality parameter
 */
enum uimgheif_quality
{
    UIMGHEIF_QUALITY_WORST      = 10,   ///< Worst quality
    UIMGHEIF_QUALITY_LOW        = 75,   ///< Low quality
    UIMGHEIF_QUALITY_MEDIUM     = 85,   ///< Medium quality
    UIMGHEIF_QUALITY_HIGH       = 92,   ///< High quality
    UIMGHEIF_QUALITY_BEST       = 100,  ///< Best quality

    UIMGHEIF_QUALITY_LOSSLESS   = 0x80, ///< Lossless compression
};

/// HEIF encoder object type
typedef struct uimgheif_enc uimgheif_enc_t;

UIMGAPI uimgheif_enc_t* uimgheif_enc_create(int flags);
UIMGAPI void uimgheif_enc_release(uimgheif_enc_t *enc);

UIMGAPI int uimgheif_enc_count_image(const uimgheif_enc_t *enc);
UIMGAPI int uimgheif_enc_add_image(uimgheif_enc_t *enc, const uimg_t *img);

UIMGAPI int uimgheif_enc_write_file(uimgheif_enc_t *enc, const char *filename);
UIMGAPI size_t uimgheif_enc_write_mem(uimgheif_enc_t *enc, void *buf, size_t size);

UIMGAPI int uimgheif_hlpr_save_file(
    const uimg_t *img, const char *filename, int flags);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
