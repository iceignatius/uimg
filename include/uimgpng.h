/**
 * @file
 * @brief       PNG file encoder and decoder
 * @author      王文佑
 * @date        2022/06/21
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_PNG_H_
#define _UIMG_PNG_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Encode/decode options
 */
enum uimgpng_encopt
{
    UIMGPNG_DEFAULT = 0,    ///< Auto selection.

    UIMGPNG_BGRA        = 32,               ///< Colourful with alpha channel. (default)
    UIMGPNG_BGR         = 24,               ///< Colourful without alpha channel.
    UIMGPNG_GREY8A      = 9,                ///< 8-bits greyscale with alpha channel.
    UIMGPNG_GREY8       = 8,                ///< 8-bits greyscale without alpha channel.
    UIMGPNG_GREY16      = 16,               ///< 16-bits greyscale without alpha channel.
    UIMGPNG_GREY        = UIMGPNG_GREY8,    ///< Alias of UIMGPNG_GREY8.
    UIMGPNG_BW          = 1,                ///< Black & white.
    UIMGPNG_FMT_MASK    = 0x3F,

    UIMGPNG_COMP_BEST   = 1 << 6,   ///< Best compression (default).
    UIMGPNG_COMP_FAST   = 2 << 6,   ///< Fast compression.
    UIMGPNG_COMP_NO     = 3 << 6,   ///< No compression.
    UIMGPNG_COMP_MASK   = 0x3 << 6,
};

UIMGAPI size_t uimgpng_encode(const uimg_t *img, void *buf, size_t size, int flags);
UIMGAPI size_t uimgpng_decode(uimg_t *img, const void *data, size_t size);

UIMGAPI int uimgpng_save_file(const uimg_t *img, const char *filename, int flags);
UIMGAPI int uimgpng_load_file(uimg_t *img, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
