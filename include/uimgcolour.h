/**
 * @file
 * @brief       Colour definition and utility
 * @details     The only supported colour format is BGRA8888 in big-endian
 * @author      王文佑
 * @date        2022/06/20
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_COLOUR_H_
#define _UIMG_COLOUR_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__GNUC__) && defined(__BYTE_ORDER__)
#   define UIMG_IS_BE ( __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__ )
#elif defined(_WIN32)
#   define UIMG_IS_BE (0)
#else
#   error Unknown endian format!
#endif

/// Generate colour value by assign B, G, R, A values
#define UIMG_MAKE_COLOUR(b, g, r, a) \
    ( \
        UIMG_IS_BE ? \
        ( \
            ( ( ((uint32_t)(b)) & 0xFF ) << 3*8 ) | \
            ( ( ((uint32_t)(g)) & 0xFF ) << 2*8 ) | \
            ( ( ((uint32_t)(r)) & 0xFF ) << 1*8 ) | \
            ( ( ((uint32_t)(a)) & 0xFF ) << 0*8 ) \
        ) : \
        ( \
            ( ( ((uint32_t)(a)) & 0xFF ) << 3*8 ) | \
            ( ( ((uint32_t)(r)) & 0xFF ) << 2*8 ) | \
            ( ( ((uint32_t)(g)) & 0xFF ) << 1*8 ) | \
            ( ( ((uint32_t)(b)) & 0xFF ) << 0*8 ) \
        ) \
    )

/// Get the blue component from a colour value
#define UIMG_COLOUR_GET_B(c) \
    ( UIMG_IS_BE ? ( ( (c) >> 3*8 ) & 0xFF ) : ( ( (c) >> 0*8 ) & 0xFF ) )

/// Get the green component from a colour value
#define UIMG_COLOUR_GET_G(c) \
    ( UIMG_IS_BE ? ( ( (c) >> 2*8 ) & 0xFF ) : ( ( (c) >> 1*8 ) & 0xFF ) )

/// Get the red component from a colour value
#define UIMG_COLOUR_GET_R(c) \
    ( UIMG_IS_BE ? ( ( (c) >> 1*8 ) & 0xFF ) : ( ( (c) >> 2*8 ) & 0xFF ) )

/// Get the alpha component from a colour value
#define UIMG_COLOUR_GET_A(c) \
    ( UIMG_IS_BE ? ( ( (c) >> 0*8 ) & 0xFF ) : ( ( (c) >> 3*8 ) & 0xFF ) )

/// Set the blue component from a colour value
#define UIMG_COLOUR_SET_B(c, b) \
    ( \
        UIMG_IS_BE ? \
        ( ( (c) & ~( 0xFF << 3*8 ) ) | ( ( (b) & 0xFF ) << 3*8 ) ) : \
        ( ( (c) & ~( 0xFF << 0*8 ) ) | ( ( (b) & 0xFF ) << 0*8 ) ) \
    )

/// Set the green component from a colour value
#define UIMG_COLOUR_SET_G(c, g) \
    ( \
        UIMG_IS_BE ? \
        ( ( (c) & ~( 0xFF << 2*8 ) ) | ( ( (g) & 0xFF ) << 2*8 ) ) : \
        ( ( (c) & ~( 0xFF << 1*8 ) ) | ( ( (g) & 0xFF ) << 1*8 ) ) \
    )

/// Set the red component from a colour value
#define UIMG_COLOUR_SET_R(c, r) \
    ( \
        UIMG_IS_BE ? \
        ( ( (c) & ~( 0xFF << 1*8 ) ) | ( ( (r) & 0xFF ) << 1*8 ) ) : \
        ( ( (c) & ~( 0xFF << 2*8 ) ) | ( ( (r) & 0xFF ) << 2*8 ) ) \
    )

/// Set the alpha component from a colour value
#define UIMG_COLOUR_SET_A(c, a) \
    ( \
        UIMG_IS_BE ? \
        ( ( (c) & ~( 0xFF << 0*8 ) ) | ( ( (a) & 0xFF ) << 0*8 ) ) : \
        ( ( (c) & ~( 0xFF << 3*8 ) ) | ( ( (a) & 0xFF ) << 3*8 ) ) \
    )

#define UIMG_COLOUR_TO_GREYSCALE(c) \
    ( \
        ( \
            29 * UIMG_COLOUR_GET_B(c) + \
            150 * UIMG_COLOUR_GET_G(c) + \
            77 * UIMG_COLOUR_GET_R(c) \
        ) >> 8 \
    )

static const uint32_t UIMG_COLOUR_EMPTY = UIMG_MAKE_COLOUR(0, 0, 0, 0);         ///< Blank colour
static const uint32_t UIMG_COLOUR_BLANK = UIMG_MAKE_COLOUR(0, 0, 0, 0);         ///< Blank colour
static const uint32_t UIMG_COLOUR_BLACK = UIMG_MAKE_COLOUR(0, 0, 0, 255);       ///< Black colour
static const uint32_t UIMG_COLOUR_WHITE = UIMG_MAKE_COLOUR(255, 255, 255, 255); ///< White colour
static const uint32_t UIMG_COLOUR_BLUE  = UIMG_MAKE_COLOUR(255, 0, 0, 255);     ///< Blue colour
static const uint32_t UIMG_COLOUR_GREEN = UIMG_MAKE_COLOUR(0, 255, 0, 255);     ///< Green colour
static const uint32_t UIMG_COLOUR_RED   = UIMG_MAKE_COLOUR(0, 0, 255, 255);     ///< Red colour

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
