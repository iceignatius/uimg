/**
 * @file
 * @brief       Image class
 * @author      王文佑
 * @date        2022/06/20
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_H_
#define _UIMG_H_

#include <stddef.h>
#include "uimgapi.h"
#include "uimgcolour.h"
#include "drvid.h"

#if defined(__cplusplus) && defined(UIMG_ENABLE_EXCEPTION)
#   include <stdexcept>
#endif

#ifdef __cplusplus
extern "C" {
#endif

UIMGAPI unsigned uimg_get_drv_id(void);

/**
 * The image object type
 */
typedef struct uimg uimg_t;

UIMGAPI uimg_t* uimg_create(int w, int h);
UIMGAPI uimg_t* uimg_clone(const uimg_t *src);
UIMGAPI uimg_t* uimg_shadow(const uimg_t *src);
UIMGAPI void uimg_release(uimg_t *img);

UIMGAPI int uimg_get_refcnt(const uimg_t *img);
UIMGAPI int uimg_is_valid(const uimg_t *img);
UIMGAPI int uimg_get_width(const uimg_t *img);
UIMGAPI int uimg_get_height(const uimg_t *img);

UIMGAPI uint32_t uimg_get_pixel(uimg_t *img, int x, int y);
UIMGAPI int uimg_set_pixel(uimg_t *img, int x, int y, uint32_t val);

UIMGAPI int uimg_begin_buf_access(const uimg_t *img);
UIMGAPI int uimg_end_buf_access(const uimg_t *img);
UIMGAPI uint32_t* uimg_get_row_buf(uimg_t *img, int y);
UIMGAPI const uint32_t* uimg_get_row_buf_c(const uimg_t *img, int y);

UIMGAPI void* uimg_get_drv_buf(uimg_t *img);

UIMGAPI int uimg_resize(uimg_t *img, int w, int h);
UIMGAPI int uimg_freebuf(uimg_t *img);
UIMGAPI int uimg_fill(uimg_t *img, uint32_t val);

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef __cplusplus

/**
 * C++ wrapper of ::uimg_t
 */
class Uimg
{
private:
    uimg_t *img;

public:
    static unsigned DriverId()
    {
        /**
         * Same as uimg_get_drv_id()
         */
        return uimg_get_drv_id();
    }

public:
    Uimg(int w = 0, int h = 0)
    {
        /**
         * Constructor
         *
         * @param w The initial image width.
         * @param h The initial image height.
         *
         * @remarks
         * On error occurred:
         * If the option UIMG_ENABLE_EXCEPTION is enabled, then
         * it will throw a std::bad_alloc exception; otherwise,
         * an invalid image object (buffer not ready) will be created.
         */
        img = uimg_create(w, h);
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::bad_alloc();
#endif
    }

    Uimg(int w, int h, uint32_t val)
    {
        /**
         * Constructor
         *
         * @param w     The initial image width.
         * @param h     The initial image height.
         * @param val   Filled with this initial colour.
         *
         * @remarks
         * On error occurred:
         * If the option UIMG_ENABLE_EXCEPTION is enabled, then
         * it will throw a std::bad_alloc exception; otherwise,
         * an invalid image object (buffer not ready) will be created.
         */
        img = uimg_create(w, h);
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::bad_alloc();
#endif

        if( img && uimg_fill(img, val) )
        {
            uimg_release(img);
            img = NULL;
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::runtime_error("Failed to fill colour!");
#endif
        }
    }

    Uimg(uimg_t *img) : img(img)
    {
        /**
         * Constructor
         *
         * @param img   The uimg_t type of image object to be assigned
         *              to the wrapper object,
         *              the wrapper will hold and using it,
         *              and will release it on destroy.
         */
    }

    Uimg(const Uimg &src)
    {
        /**
         * Constructor
         *
         * @remarks
         * On error occurred:
         * If the option UIMG_ENABLE_EXCEPTION is enabled, then
         * it will throw a std::bad_alloc exception; otherwise,
         * an invalid image object (buffer not ready) will be created.
         */
        img = uimg_shadow(src.img);
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::bad_alloc();
#endif
    }

#if __cplusplus >= 201103L
    Uimg(Uimg &&src)
    {
        /**
         * Constructor
         */
        img = src.img;
        src.img = NULL;
    }
#endif

    ~Uimg()
    {
        /**
         * Destructor
         */
        uimg_release(img);
    }

    Uimg& operator=(const Uimg &src)
    {
        /**
         * Assign operator
         *
         * @remarks
         * On error occurred:
         * If the option UIMG_ENABLE_EXCEPTION is enabled, then
         * it will throw a std::bad_alloc exception; otherwise,
         * an invalid image object (buffer not ready) will be created.
         */
        uimg_release(img);
        img = uimg_shadow(src.img);
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::bad_alloc();
#endif

        return *this;
    }

#if __cplusplus >= 201103L
    Uimg& operator=(Uimg &&src)
    {
        /**
         * Assign operator
         */
        uimg_release(img);
        img = src.img;
        src.img = NULL;

        return *this;
    }
#endif

private:
    int Monopoly()
    {
        if( !img || uimg_get_refcnt(img) <= 1 )
            return 0;

        uimg_t *tmp = uimg_clone(img);
        if(!tmp)
            return -1;

        uimg_release(img);
        img = tmp;
        return 0;
    }

public:
    const uimg_t* cptr() const
    {
        /**
         * Get ::uimg_t type of pointer
         */
        return img;
    }

    uimg_t* cptr()
    {
        /**
         * Get ::uimg_t type of pointer
         *
         * @return
         * The ::uimg_t type of pointer.
         * If error occurred and the option UIMG_ENABLE_EXCEPTION is enabled, then
         * it will throw a std::bad_alloc exception; otherwise,
         * NULL will be returned.
         */
        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return NULL;
#endif
        }
        else
        {
            return img;
        }
    }

    operator const uimg_t* () const
    {
        /**
         * The implecit ::uimg_t type pointer converter
         */
        return img;
    }

public:
    bool Valid() const
    {
        /**
         * Same as uimg_is_valid()
         */
        return uimg_is_valid(img);
    }

    int Width() const
    {
        /**
         * Same as uimg_get_width()
         */
        return uimg_get_width(img);
    }

    int Height() const
    {
        /**
         * Same as uimg_get_height()
         */
        return uimg_get_height(img);
    }

    uint32_t GetPixel(int x, int y) const
    {
        /**
         * Same as uimg_get_pixel()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * an exception will be thrown; otherwise,
         * the behaviour is undefined.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if( x < 0 || uimg_get_width(img) <= x ||
            y < 0 || uimg_get_height(img) <= y )
        {
            throw std::out_of_range(__func__);
        }
#endif

        return uimg_get_pixel(img, x, y);
    }

    int SetPixel(int x, int y, uint32_t val)
    {
        /**
         * Same as uimg_set_pixel()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * an exception will be thrown; otherwise,
         * the behaviour is undefined.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if( x < 0 || uimg_get_width(img) <= x ||
            y < 0 || uimg_get_height(img) <= y )
        {
            throw std::out_of_range(__func__);
        }
#endif

        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return -1;
#endif
        }

        return uimg_set_pixel(img, x, y, val);
    }

    int BeginBufAccess() const
    {
        /**
         * Same as uimg_begin_buf_access()
         */
        return uimg_begin_buf_access(img);
    }

    int EndBufAccess() const
    {
        /**
         * Same as uimg_end_buf_access()
         */
        return uimg_end_buf_access(img);
    }

    const uint32_t* RowBuffer(int y) const
    {
        /**
         * Same as uimg_get_row_buf_c()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw an exception instead of return NULL.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if( img && ( y < 0 || uimg_get_height(img) <= y ) )
            throw std::out_of_range(__func__);
#endif

        return uimg_get_row_buf_c(img, y);
    }

    uint32_t* RowBuffer(int y)
    {
        /**
         * Same as uimg_get_row_buf()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw an exception instead of return NULL.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if( img && ( y < 0 || uimg_get_height(img) <= y ) )
            throw std::out_of_range(__func__);
#endif

        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return NULL;
#endif
        }

        return uimg_get_row_buf(img, y);
    }

    void* DriverBuffer()
    {
        /**
         * Same as uimg_get_drv_buf()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw the std::bad_alloc exception
         * instead of return NULL.
         */
        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return NULL;
#endif
        }

        return uimg_get_drv_buf(img);
    }

public:
    int Resize(int w, int h)
    {
        /**
         * Same as uimg_resize()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw an exception instead of return an error code.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::invalid_argument(__func__);
        if( w < 0 || h < 0 )
            throw std::invalid_argument(__func__);
#endif

        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return -1;
#endif
        }

        int err = uimg_resize(img, w, h);
#ifdef UIMG_ENABLE_EXCEPTION
        if(err)
            throw std::bad_alloc();
#endif
        return err;
    }

    int FreeBuffer()
    {
        /**
         * Same as uimg_freebuf()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw an exception instead of return an error code.
         */
        if(!img)
        {
            return 0;
        }
        else if( uimg_get_refcnt(img) <= 1 )
        {
            int err = uimg_freebuf(img);
#ifdef UIMG_ENABLE_EXCEPTION
            if(err)
                throw std::runtime_error(__func__);
#endif
            return err;
        }
        else
        {
            uimg_release(img);
            img = uimg_create(0, 0);
#ifdef UIMG_ENABLE_EXCEPTION
            if(!img)
                throw std::bad_alloc();
#endif
            return img ? 0 : -1;
        }
    }

    int Fill(uint32_t val)
    {
        /**
         * Same as uimg_fill()
         *
         * @remarks
         * On error occurred and the option UIMG_ENABLE_EXCEPTION is enabled,
         * it will throw an exception instead of return an error code.
         */
#ifdef UIMG_ENABLE_EXCEPTION
        if(!img)
            throw std::invalid_argument(__func__);
#endif

        if(Monopoly())
        {
#ifdef UIMG_ENABLE_EXCEPTION
            throw std::bad_alloc();
#else
            return -1;
#endif
        }

        int err = uimg_fill(img, val);
#ifdef UIMG_ENABLE_EXCEPTION
        if(err)
            throw std::runtime_error(__func__);
#endif
        return err;
    }
};

#endif  // __cplusplus

#endif
