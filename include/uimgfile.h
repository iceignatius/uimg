/**
 * @file
 * @brief       Image file encoder and decoder helper
 * @details     Save and load image file with file format auto detected by
 *              the file name extension.
 * @author      王文佑
 * @date        2022/06/21
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_FILE_H_
#define _UIMG_FILE_H_

#include "uimgbmp.h"
#include "uimgjpg.h"
#include "uimgpng.h"
#include "uimggif_enc.h"
#include "uimggif_dec.h"
#include "uimgwebp.h"
#include "uimgheif_enc.h"
#include "uimgheif_dec.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Image file types (formats)
 */
enum uimgfile_type
{
    UIMGFILE_UNSUPPORTED = 0,   ///< Invalid/unsupported type
    UIMGFILE_BMP,               ///< BMP file
    UIMGFILE_PNG,               ///< PNG file
    UIMGFILE_JPEG,              ///< JPG/JPEG file
    UIMGFILE_GIF,               ///< GIF file
    UIMGFILE_WEBP,              ///< WebP file
    UIMGFILE_HEIF,              ///< HEIF/HEIC file
};

UIMGAPI int uimgfile_parse_type_by_name(const char *filename);
UIMGAPI int uimgfile_parse_type_by_signature(const void *data, size_t size);

UIMGAPI int uimgfile_save_file(const uimg_t *img, const char *filename, int flags);
UIMGAPI int uimgfile_load_file(uimg_t *img, const char *filename, int flags);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
