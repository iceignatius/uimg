/**
 * @file
 * @brief       Image buffer driver ID definition
 * @author      王文佑
 * @date        2022/07/12
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_DRIVER_ID_DEFINE_H_
#define _UIMG_DRIVER_ID_DEFINE_H_

#define UIMG_DRVID_SDL      0x53444C32  ///< Driver ID of SDL
#define UIMG_DRVID_RV1126   0x52563131  ///< Driver ID of RV1126
#define UIMG_DRVID_SOFT     0x534F4654  ///< Driver ID of Software

#endif
