/**
 * @file
 * @brief       GIF file decoder
 * @author      王文佑
 * @date        2023/11/23
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_GIF_DECODER_H_
#define _UIMG_GIF_DECODER_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/// GIF decoder object type
typedef struct uimggif_dec uimggif_dec_t;

UIMGAPI uimggif_dec_t* uimggif_dec_open_file(const char *filename);
UIMGAPI uimggif_dec_t* uimggif_dec_open_mem(
    const void *raw, size_t raw_size, size_t *read_size);
UIMGAPI void uimggif_dec_close(uimggif_dec_t *dec);

UIMGAPI int uimggif_dec_get_width(const uimggif_dec_t *dec);
UIMGAPI int uimggif_dec_get_height(const uimggif_dec_t *dec);
UIMGAPI int uimggif_dec_count_colour(const uimggif_dec_t *dec);
UIMGAPI int uimggif_dec_count_image(const uimggif_dec_t *dec);

UIMGAPI int uimggif_dec_get_image(
    uimggif_dec_t *dec, int idx, uimg_t *img, int *dura);

UIMGAPI int uimggif_hlpr_load_file(uimg_t *img, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
