/**
 * @file
 * @brief       GIF file encoder
 * @author      王文佑
 * @date        2023/11/23
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_GIF_ENCODER_H_
#define _UIMG_GIF_ENCODER_H_

#include "uimg.h"
#include "uimggif_palette.h"

#ifdef __cplusplus
extern "C" {
#endif

/// GIF encoder object type
typedef struct uimggif_enc uimggif_enc_t;

UIMGAPI uimggif_enc_t* uimggif_enc_open_file(const char *filename, int palette_id);
UIMGAPI uimggif_enc_t* uimggif_enc_open_mem(void *buf, size_t size, int palette_id);
UIMGAPI void uimggif_enc_close(uimggif_enc_t *enc);

UIMGAPI int uimggif_enc_get_width(const uimggif_enc_t *enc);
UIMGAPI int uimggif_enc_get_height(const uimggif_enc_t *enc);
UIMGAPI int uimggif_enc_count_colour(const uimggif_enc_t *enc);
UIMGAPI int uimggif_enc_count_image(const uimggif_enc_t *enc);

UIMGAPI int uimggif_enc_add_image(
    uimggif_enc_t *enc, const uimg_t *img, int dura);

UIMGAPI size_t uimggif_enc_write(uimggif_enc_t *enc);
UIMGAPI const void* uimggif_enc_get_rawdata(const uimggif_enc_t *enc);
UIMGAPI size_t uimggif_enc_get_rawsize(const uimggif_enc_t *enc);

UIMGAPI int uimggif_hlpr_save_file(
    const uimg_t *img, const char *filename, int palette_id);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
