/**
 * @file
 * @brief       WebP file encoder and decoder
 * @author      王文佑
 * @date        2023/11/23
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_WEBP_H_
#define _UIMG_WEBP_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Predefined WebP quality parameter
 */
enum uimgwebp_quality
{
    UIMGWEBP_QUALITY_WORST      = 10,   ///< Worst quality
    UIMGWEBP_QUALITY_LOW        = 75,   ///< Low quality
    UIMGWEBP_QUALITY_MEDIUM     = 85,   ///< Medium quality
    UIMGWEBP_QUALITY_HIGH       = 92,   ///< High quality
    UIMGWEBP_QUALITY_BEST       = 100,  ///< Best quality

    UIMGWEBP_QUALITY_LOSSLESS   = 0x80, ///< Lossless compression
};

UIMGAPI size_t uimgwebp_encode(const uimg_t *img, void *buf, size_t size, int flags);
UIMGAPI size_t uimgwebp_decode(uimg_t *img, const void *data, size_t size);

UIMGAPI int uimgwebp_save_file(const uimg_t *img, const char *filename, int flags);
UIMGAPI int uimgwebp_load_file(uimg_t *img, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
