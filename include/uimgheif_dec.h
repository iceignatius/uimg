/**
 * @file
 * @brief       HEIF/HEIC file decoder
 * @author      王文佑
 * @date        2023/11/23
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_HEIF_DECODER_H_
#define _UIMG_HEIF_DECODER_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/// HEIF decoder object type
typedef struct uimgheif_dec uimgheif_dec_t;

UIMGAPI uimgheif_dec_t* uimgheif_dec_open_file(const char *filename);
UIMGAPI uimgheif_dec_t* uimgheif_dec_open_mem(
    const void *raw, size_t raw_size, size_t *read_size);
UIMGAPI void uimgheif_dec_close(uimgheif_dec_t *dec);

UIMGAPI int uimgheif_dec_count_image(const uimgheif_dec_t *dec);
UIMGAPI int uimgheif_dec_get_primary_idx(const uimgheif_dec_t *dec);
UIMGAPI int uimgheif_dec_get_image(uimgheif_dec_t *dec, int idx, uimg_t *img);

UIMGAPI int uimgheif_hlpr_load_file(uimg_t *img, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
