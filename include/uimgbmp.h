/**
 * @file
 * @brief       BMP file encoder and decoder
 * @author      王文佑
 * @date        2022/06/21
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_BMP_H_
#define _UIMG_BMP_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Encode options
 */
enum uimgbmp_encopt
{
    UIMGBMP_ENC_DEFAULT = 0,    ///< Auto selection.
    UIMGBMP_ENC_BW      = 1,    ///< Black and white.
    UIMGBMP_ENC_GREY    = 8,    ///< Grey scale.
    UIMGBMP_ENC_RGB     = 24,   ///< 24-bits colourful.
    UIMGBMP_ENC_RGBA    = 32,   ///< 32-bits colourful
                                ///< (24-bits colour plus 8-bits alpha channel).
    UIMGBMP_ENC_COLOURMASK = 0x3F,

    UIMGBMP_ENC_TOPDOWN = 0x40, ///< Write rows in top-down sequence (default: bottom-up).
};

/**
 * Decode options
 */
enum uimgbmp_decopt
{
    UIMGBMP_DEC_DEFAULT         = 0,    ///< Default.
    UIMGBMP_DEC_IGNORE_ALPHA    = 1,    ///< Ignore alpha channel and
                                        ///< assume they all non-transparent.
};

UIMGAPI size_t uimgbmp_encode(const uimg_t *img, void *buf, size_t size, int flags);
UIMGAPI size_t uimgbmp_decode(uimg_t *img, const void *data, size_t size, int flags);

UIMGAPI int uimgbmp_save_file(const uimg_t *img, const char *filename, int flags);
UIMGAPI int uimgbmp_load_file(uimg_t *img, const char *filename, int flags);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
