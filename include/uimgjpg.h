/**
 * @file
 * @brief       JPEG file encoder and decoder
 * @author      王文佑
 * @date        2022/06/21
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_JPG_H_
#define _UIMG_JPG_H_

#include "uimg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Predefined JPEG quality parameter
 */
enum uimgjpg_quality
{
    UIMGJPG_QUALITY_WORST   = 10,   ///< Worst quality.
    UIMGJPG_QUALITY_LOW     = 75,   ///< Low quality.
    UIMGJPG_QUALITY_MEDIUM  = 85,   ///< Medium quality.
    UIMGJPG_QUALITY_HIGH    = 92,   ///< High quality.
    UIMGJPG_QUALITY_BEST    = 100,  ///< Best quality.
};

UIMGAPI size_t uimgjpg_encode(const uimg_t *img, void *buf, size_t size, int quality);
UIMGAPI size_t uimgjpg_decode(uimg_t *img, const void *data, size_t size);

UIMGAPI int uimgjpg_save_file(const uimg_t *img, const char *filename, int quality);
UIMGAPI int uimgjpg_load_file(uimg_t *img, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
