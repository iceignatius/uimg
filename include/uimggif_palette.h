/**
 * @file
 * @brief       GIF palette definition and manager
 * @author      王文佑
 * @date        2024/08/26
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_GIF_PALETTE_INTERFACE_H_
#define _UIMG_GIF_PALETTE_INTERFACE_H_

#include<stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * GIF colour palette
 */
enum uimggif_palette_id
{
    UIMGGIF_PALETTE_BW      = 'b',  ///< The black and white 2-colours palette
    UIMGGIF_PALETTE_VGA16   = 'l',  ///< The VGA 16-colours palette
    UIMGGIF_PALETTE_VGA256  = 'v',  ///< The VGA 256-colours palette
    UIMGGIF_PALETTE_FAST332 = 'f',  ///< The simplest 3-3-2-bits format 256-colours palette
    UIMGGIF_PALETTE_WEBSAFE = 'w',  ///< The Web-Safe 216-colours palette (transparent supported)
    UIMGGIF_PALETTE_GREY    = 'g',  ///< The grey scale in 256 levels

    UIMGGIF_PALETTE_CUSTOM_MIN = 0x80,
    UIMGGIF_PALETTE_CUSTOM_MAX = 0xFF,
};

struct uimggif_palette
{
    const struct GifColorType*(*get_table)(struct uimggif_palette *host);
    unsigned(*count_colours)(struct uimggif_palette *host);
    uint8_t(*calc_index)(struct uimggif_palette *host, uint32_t colour);
    int(*get_transparent)(struct uimggif_palette *host);
};

int uimggif_palette_acquire_id(struct uimggif_palette *palette);
int uimggif_palette_release_id(int id);

struct uimggif_palette* uimggif_palette_find(int id);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
