/**
 * @file
 * @brief       Definition of image blit type and parameters
 * @author      王文佑
 * @date        2022/06/24
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_BLIT_DEF_H_
#define _UIMG_BLIT_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Flip direction
 */
enum uimg_flip
{
    UIMG_FLIP_V = 1,    ///< Vertical
    UIMG_FLIP_H = 2,    ///< Horizontal
};

/**
 * Rotate angle
 */
enum uimg_rotate
{
    UIMG_ROTATE_0   = 0,    ///< 0 degree
    UIMG_ROTATE_90  = 90,   ///< 90 degree
    UIMG_ROTATE_180 = 180,  ///< 180 degree
    UIMG_ROTATE_270 = 270,  ///< 270 degree
};

/**
 * Scale mode
 */
enum uimg_scale_mode
{
    UIMG_SCALE_NONE = 0,    ///< No scale method supported or applied
    UIMG_SCALE_NEARBY,      ///< Nearby scaling method
    UIMG_SCALE_LINEAR,      ///< Linear scaling method
    UIMG_SCALE_SPLINE,      ///< Cubic spline scaling method
};

/**
 * Blend mode
 */
enum uimg_blend_mode
{
    UIMG_BLEND_OVERLAY = 0, ///< Pixel overlay
    UIMG_BLEND_ALPHA,       ///< Alpha blend
};

/**
 * Rectangle area
 */
struct uimg_rect
{
    int x;  ///< X position
    int y;  ///< Y position
    int w;  ///< Width
    int h;  ///< Height
};

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
