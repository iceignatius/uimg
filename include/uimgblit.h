/**
 * @file
 * @brief       Basic accelerated image process functions
 * @author      王文佑
 * @date        2022/06/24
 * @copyright   ZLib Licence
 */
#ifndef _UIMG_BLIT_H_
#define _UIMG_BLIT_H_

#include "uimg.h"
#include "uimgblitdef.h"

#ifdef __cplusplus
extern "C" {
#endif

int uimgblit_fill(uimg_t *img, uint32_t val, const struct uimg_rect *rect);
int uimgblit_flip(uimg_t *img, int dir);
int uimgblit_rotate(uimg_t *img, int ang);
int uimgblit_stretch(uimg_t *img, int w, int h, int scale_mode);
int uimgblit_blend(
    uimg_t *img,
    int dst_x,
    int dst_y,
    const uimg_t *src,
    const struct uimg_rect *src_rect,
    int blend_mode);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
