/**
 * @brief       Library interface
 * @author      王文佑
 * @date        2022/06/16
 * @copyright   ZLib Licence
 */
#ifndef _UIMGAPI_H_
#define _UIMGAPI_H_

#include "uimg_config.h"

#ifdef __cplusplus
    #define UIMGAPI_EXTC extern "C"
#else
    #define UIMGAPI_EXTC
#endif

#if defined(__unix__) && defined(__GNUC__)
    #define UIMGAPI_SHARED_EXPORT __attribute__((visibility("default")))
#elif defined(_WIN32)
    #ifdef UIMG_BUILDING_LIBRARY
        #define UIMGAPI_SHARED_EXPORT __declspec(dllexport)
    #else
        #define UIMGAPI_SHARED_EXPORT __declspec(dllimport)
    #endif
#else
    #define UIMGAPI_SHARED_EXPORT
#endif

#ifdef UIMG_BUILD_SHARED
    #define UIMGAPI UIMGAPI_EXTC UIMGAPI_SHARED_EXPORT
#else
    #define UIMGAPI UIMGAPI_EXTC
#endif

#endif
