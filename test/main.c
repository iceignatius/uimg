#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <limits.h>
#include <setjmp.h>
#include <cmocka.h>
#include "uimggif_pltgen.h"
#include "uimgfile.h"
#include "uimgblit.h"

#ifdef UIMG_USE_SDL
#   include <SDL2/SDL.h>
#endif

static
long load_compare_image(const char *filename, const uimg_t *t_img)
{
    /*
     * Return:
     * + Positive (including zero): Average value difference per pixel
     * + Negative: Error occurred or properties mismatch
     */
    uimg_t *f_img = NULL;

    long err = -1;
    do
    {
        if( !( f_img = uimg_create(0, 0) ) )
        {
            err = -3;
            break;
        }

        if(uimgfile_load_file(f_img, filename, 0))
        {
            err = -2;
            break;
        }

        if( uimg_get_width(f_img) != uimg_get_width(t_img) ||
            uimg_get_height(f_img) != uimg_get_height(t_img) )
        {
            err = -1;
            break;
        }

        uimg_begin_buf_access(f_img);
        uimg_begin_buf_access(t_img);

        long long diff = 0;
        int w = uimg_get_width(t_img);
        int h = uimg_get_height(t_img);
        for(int y = 0; y < h; ++y)
        {
            const uint32_t *f_row = uimg_get_row_buf_c(f_img, y);
            const uint32_t *t_row = uimg_get_row_buf_c(t_img, y);
            if( !f_row || !t_row )
            {
                diff = LLONG_MAX;
                break;
            }

            for(int x = 0; x < w; ++x)
            {
                diff += abs( (int) UIMG_COLOUR_GET_B(f_row[x]) - (int) UIMG_COLOUR_GET_B(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_G(f_row[x]) - (int) UIMG_COLOUR_GET_G(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_R(f_row[x]) - (int) UIMG_COLOUR_GET_R(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_A(f_row[x]) - (int) UIMG_COLOUR_GET_A(t_row[x]) );
            }
        }

        uimg_end_buf_access(t_img);
        uimg_end_buf_access(f_img);

        diff /= ( w * h * 4 );
        err = diff;

    } while(false);

    if(f_img)
        uimg_release(f_img);

    return err;
}

#ifdef UIMG_USE_SDL
static
void init_sdl(void **state)
{
    assert_int_equal(SDL_Init(SDL_INIT_VIDEO), 0);
}
#endif

#ifdef UIMG_USE_SDL
static
void quit_sdl(void **state)
{
    SDL_Quit();
}
#endif

#ifdef UIMG_ENABLE_BMP
static
void test_bmp_rw(void **state)
{
    uimg_t *colour_sample = uimg_create(3, 3);
    assert_non_null(colour_sample);
    assert_int_equal(uimg_set_pixel(colour_sample, 0, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 1, 0, UIMG_COLOUR_WHITE), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 2, 0, UIMG_COLOUR_GREEN), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 0, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 1, 1, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 2, 1, UIMG_COLOUR_GREEN), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 0, 2, UIMG_COLOUR_BLUE), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 1, 2, UIMG_COLOUR_BLUE), 0);
    assert_int_equal(uimg_set_pixel(colour_sample, 2, 2, UIMG_COLOUR_BLUE), 0);

    uimg_t *grey_sample = uimg_create(3, 3);
    assert_non_null(grey_sample);
    assert_int_equal(uimg_set_pixel(grey_sample, 0, 0, UIMG_MAKE_COLOUR(  0,   0,   0, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 1, 0, UIMG_MAKE_COLOUR(128, 128, 128, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 2, 0, UIMG_MAKE_COLOUR(  0,   0,   0, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 0, 1, UIMG_MAKE_COLOUR(127, 127, 127, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 1, 1, UIMG_MAKE_COLOUR(255, 255, 255, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 2, 1, UIMG_MAKE_COLOUR(127, 127, 127, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 0, 2, UIMG_MAKE_COLOUR(  0,   0,   0, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 1, 2, UIMG_MAKE_COLOUR(128, 128, 128, 255)), 0);
    assert_int_equal(uimg_set_pixel(grey_sample, 2, 2, UIMG_MAKE_COLOUR(  0,   0,   0, 255)), 0);

    uimg_t *bw_sample = uimg_create(3, 3);
    assert_non_null(bw_sample);
    assert_int_equal(uimg_set_pixel(bw_sample, 0, 0, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 1, 0, UIMG_COLOUR_WHITE), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 2, 0, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 0, 1, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 1, 1, UIMG_COLOUR_WHITE), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 2, 1, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 0, 2, UIMG_COLOUR_BLACK), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 1, 2, UIMG_COLOUR_WHITE), 0);
    assert_int_equal(uimg_set_pixel(bw_sample, 2, 2, UIMG_COLOUR_BLACK), 0);

    assert_int_equal(uimgbmp_save_file(colour_sample, "output/bmp_test32.bmp", UIMGBMP_ENC_RGBA), 0);
    assert_int_equal(uimgbmp_save_file(colour_sample, "output/bmp_test24.bmp", UIMGBMP_ENC_RGB), 0);
    assert_int_equal(uimgbmp_save_file(grey_sample, "output/bmp_test8.bmp", UIMGBMP_ENC_GREY), 0);
    assert_int_equal(uimgbmp_save_file(grey_sample, "output/bmp_test1.bmp", UIMGBMP_ENC_BW), 0);

    assert_int_equal(load_compare_image("samples/square_colour32.bmp", colour_sample), 0);
    assert_int_equal(load_compare_image("samples/square_colour24.bmp", colour_sample), 0);
    assert_int_equal(load_compare_image("samples/square_grey8.bmp", grey_sample), 0);
    assert_int_equal(load_compare_image("samples/square_grey1.bmp", bw_sample), 0);
    assert_int_equal(load_compare_image("output/bmp_test32.bmp", colour_sample), 0);
    assert_int_equal(load_compare_image("output/bmp_test24.bmp", colour_sample), 0);
    assert_int_equal(load_compare_image("output/bmp_test8.bmp", grey_sample), 0);
    assert_int_equal(load_compare_image("output/bmp_test1.bmp", bw_sample), 0);

    uimg_release(bw_sample);
    uimg_release(grey_sample);
    uimg_release(colour_sample);
}
#endif

#ifdef UIMG_ENABLE_JPEG
static
void test_jpeg_rw(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgjpg_load_file(img, "samples/pattern.jpg"), 0);
    assert_int_equal(load_compare_image("samples/pattern.bmp", img), 0);

#ifdef UIMG_ENABLE_JPEG_ENCODE
    assert_int_equal(uimgbmp_load_file(img, "samples/lenna.bmp", UIMGBMP_DEC_IGNORE_ALPHA), 0);
    assert_int_equal(load_compare_image("samples/lenna.bmp", img), 0);

    assert_int_equal(uimgjpg_save_file(img, "output/lenna.jpg", UIMGJPG_QUALITY_BEST), 0);
    assert_in_range(load_compare_image("output/lenna.jpg", img), 0, 4);
#endif

    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_PNG
static
void test_png_rw(void **state)
{
    // Small square test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/square.png"), 0);
        assert_int_equal(load_compare_image("samples/square_colour32.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(img, "output/square.png", 0), 0);
        assert_int_equal(load_compare_image("output/square.png", img), 0);

        uimg_release(img);
    }

    // Widely used format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);
        assert_int_equal(load_compare_image("samples/tux.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(img, "output/tux.png", 0), 0);
        assert_int_equal(load_compare_image("output/tux.png", img), 0);

        uimg_release(img);
    }

    // BGRA format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-bgra.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-bgra.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-bgra.png", UIMGPNG_BGRA),
            0);
        assert_int_equal(load_compare_image("output/daemon-bgra.png", img), 0);

        uimg_release(img);
    }

    // BGR format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-bgr.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-bgr.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-bgr.png", UIMGPNG_BGR),
            0);
        assert_int_equal(load_compare_image("output/daemon-bgr.png", img), 0);

        uimg_release(img);
    }

    // Grey-8 & alpha format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-grey8a.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-grey8a.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-grey8a.png", UIMGPNG_GREY8A),
            0);
        assert_int_equal(load_compare_image("output/daemon-grey8a.png", img), 0);

        uimg_release(img);
    }

    // Grey-8 format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-grey8.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-grey8.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-grey8.png", UIMGPNG_GREY8),
            0);
        assert_int_equal(load_compare_image("output/daemon-grey8.png", img), 0);

        uimg_release(img);
    }

    // Grey-16 format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-grey16.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-grey8.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-grey16.png", UIMGPNG_GREY16),
            0);
        assert_int_equal(load_compare_image("output/daemon-grey8.png", img), 0);

        uimg_release(img);
    }

    // Black & white format test
    {
        uimg_t *img = uimg_create(0, 0);
        assert_non_null(img);

        assert_int_equal(uimgpng_load_file(img, "samples/daemon-bw.png"), 0);
        assert_int_equal(load_compare_image("samples/daemon-bw.bmp", img), 0);

        assert_int_equal(uimgpng_save_file(
            img, "output/daemon-bw.png", UIMGPNG_BW),
            0);
        assert_int_equal(load_compare_image("output/daemon-bw.png", img), 0);

        uimg_release(img);
    }
}
#endif

#ifdef UIMG_ENABLE_GIF
static
void test_gif_write(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimggif_enc_t *enc =
        uimggif_enc_open_file("output/fire.gif", UIMGGIF_PALETTE_WEBSAFE);
    assert_non_null(enc);

    for(int i = 0; i < 33; ++i)
    {
        char filename[64];
        snprintf(filename, sizeof(filename), "samples/fire.gif.%02d.bmp", i);
        assert_int_equal(uimgfile_load_file(img, filename, 0), 0);
        assert_int_equal(uimggif_enc_add_image(enc, img, 50), 0);
    }

    assert_int_equal(uimggif_enc_get_width(enc), 30);
    assert_int_equal(uimggif_enc_get_height(enc), 60);
    assert_int_equal(uimggif_enc_count_colour(enc), 256);
    assert_int_equal(uimggif_enc_count_image(enc), 33);

    assert_int_not_equal(uimggif_enc_write(enc), 0);

    uimggif_enc_close(enc);
    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_GIF
static
void test_gif_read(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimggif_dec_t *dec = uimggif_dec_open_file("output/fire.gif");
    assert_non_null(dec);

    int width = uimggif_dec_get_width(dec);
    int height = uimggif_dec_get_height(dec);
    int colnum = uimggif_dec_count_colour(dec);
    int imgnum = uimggif_dec_count_image(dec);
    assert_int_equal(width, 30);
    assert_int_equal(height, 60);
    assert_int_equal(colnum, 256);
    assert_int_equal(imgnum, 33);

    for(int i = 0; i < imgnum; ++i)
    {
        int dura;
        assert_int_equal(uimggif_dec_get_image(dec, i, img, &dura), 0);
        assert_int_equal(dura, 50);

        char filename[64];
        snprintf(filename, sizeof(filename), "samples/fire.gif.%02d.bmp", i);
        assert_int_equal(load_compare_image(filename, img), 0);
    }

    uimggif_dec_close(dec);
    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_GIF) && defined(UIMG_ENABLE_PNG)
static
void test_gif_create_custom_palette(void **state)
{
    assert_non_null(state);
    assert_non_null(*state);
    uimggif_pltgen_t **shared_space = *state;
    assert_null(*shared_space);

    uimggif_pltgen_t *palette = uimggif_pltgen_create();
    assert_non_null(palette);
    *shared_space = palette;

    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    for(int i = 0; i < 8; ++i)
    {
        char filename[64];
        snprintf(filename, sizeof(filename), "samples/colourbar-%02d.png", i);
        assert_int_equal(uimgfile_load_file(img, filename, 0), 0);
        assert_int_equal(uimggif_pltgen_parse_image(palette, img), 0);
    }

    assert_int_equal(uimggif_pltgen_update_palette(palette), 0);

    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_GIF) && defined(UIMG_ENABLE_PNG)
static
void test_gif_write_custom_palette(void **state)
{
    assert_non_null(state);
    assert_non_null(*state);
    uimggif_pltgen_t **shared_space = *state;
    uimggif_pltgen_t *palette = *shared_space;
    assert_non_null(palette);

    int palette_id = uimggif_pltgen_get_id(palette);
    assert_in_range(palette_id, 0, 255);

    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimggif_enc_t *enc =
        uimggif_enc_open_file("output/colourbar.gif", palette_id);
    assert_non_null(enc);

    for(int i = 0; i < 8; ++i)
    {
        char filename[64];
        snprintf(filename, sizeof(filename), "samples/colourbar-%02d.png", i);
        assert_int_equal(uimgfile_load_file(img, filename, 0), 0);
        assert_int_equal(uimggif_enc_add_image(enc, img, 200), 0);
    }

    assert_int_equal(uimggif_enc_get_width(enc), 256);
    assert_int_equal(uimggif_enc_get_height(enc), 256);
    assert_int_equal(uimggif_enc_count_colour(enc), 256);
    assert_int_equal(uimggif_enc_count_image(enc), 8);

    assert_int_not_equal(uimggif_enc_write(enc), 0);

    uimggif_enc_close(enc);
    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_GIF) && defined(UIMG_ENABLE_PNG)
static
void test_gif_read_custom_palette(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimggif_dec_t *dec = uimggif_dec_open_file("output/colourbar.gif");
    assert_non_null(dec);

    int width = uimggif_dec_get_width(dec);
    int height = uimggif_dec_get_height(dec);
    int colnum = uimggif_dec_count_colour(dec);
    int imgnum = uimggif_dec_count_image(dec);
    assert_int_equal(width, 256);
    assert_int_equal(height, 256);
    assert_int_equal(colnum, 256);  // Ceil to the power series of 2
    assert_int_equal(imgnum, 8);

    for(int i = 0; i < imgnum; ++i)
    {
        int dura;
        assert_int_equal(uimggif_dec_get_image(dec, i, img, &dura), 0);
        assert_int_equal(dura, 200);

        char filename[64];
        snprintf(filename, sizeof(filename), "samples/colourbar-%02d.png", i);
        assert_int_equal(load_compare_image(filename, img), 0);
    }

    uimggif_dec_close(dec);
    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_GIF) && defined(UIMG_ENABLE_PNG)
static
void test_gif_release_custom_palette(void **state)
{
    assert_non_null(state);
    assert_non_null(*state);
    uimggif_pltgen_t **shared_space = *state;
    uimggif_pltgen_t *palette = *shared_space;
    assert_non_null(palette);

    uimggif_pltgen_release(palette);
    *shared_space = NULL;
}
#endif

#ifdef UIMG_ENABLE_WEBP
static
void test_webp_rw(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgwebp_load_file(img, "samples/daemon.webp"), 0);
    assert_int_equal(load_compare_image("samples/daemon-bgra.bmp", img), 0);

    assert_int_equal(
        uimgwebp_save_file(img, "output/daemon.webp", UIMGWEBP_QUALITY_LOSSLESS),
        0);
    assert_int_equal(load_compare_image("output/daemon.webp", img), 0);

    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_HEIF
static
void test_heif_rw(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgheif_hlpr_load_file(img, "samples/lenna.heic"), 0);
    assert_in_range(load_compare_image("samples/lenna.bmp", img), 0, 4);

    assert_int_equal(
        uimgheif_hlpr_save_file(img, "output/lenna.heic", UIMGHEIF_QUALITY_LOSSLESS),
        0);
    assert_in_range(load_compare_image("output/lenna.heic", img), 0, 4);

    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_BLITMOD
static
void test_fill(void **state)
{
    uimg_t *img = uimg_create(5, 5);
    assert_non_null(img);
    assert_int_equal(uimg_set_pixel(img, 0, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 1, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 2, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 3, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 4, 0, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 0, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 1, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 2, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 3, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 4, 1, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 0, 2, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 1, 2, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 2, 2, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 3, 2, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 4, 2, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 0, 3, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 1, 3, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 2, 3, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 3, 3, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 4, 3, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 0, 4, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 1, 4, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 2, 4, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 3, 4, UIMG_COLOUR_RED), 0);
    assert_int_equal(uimg_set_pixel(img, 4, 4, UIMG_COLOUR_RED), 0);

    assert_int_equal(uimgblit_fill(img, UIMG_COLOUR_BLUE, NULL), 0);

    assert_true( uimg_get_pixel(img, 0, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 4) == UIMG_COLOUR_BLUE );

    struct uimg_rect rect = { .x = 1, .y = 1, .w = 3, .h = 3 };
    assert_int_equal(uimgblit_fill(img, UIMG_COLOUR_GREEN, &rect), 0);

    assert_true( uimg_get_pixel(img, 0, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 0) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 1) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 2, 1) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 3, 1) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 4, 1) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 2) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 2, 2) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 3, 2) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 4, 2) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 3) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 2, 3) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 3, 3) == UIMG_COLOUR_GREEN );
    assert_true( uimg_get_pixel(img, 4, 3) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 0, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 1, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 2, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 3, 4) == UIMG_COLOUR_BLUE );
    assert_true( uimg_get_pixel(img, 4, 4) == UIMG_COLOUR_BLUE );

    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_BLITMOD
static
void test_flip(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_flip(img, UIMG_FLIP_V), 0);
    assert_int_equal(load_compare_image("samples/square_flip_v.bmp", img), 0);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_flip(img, UIMG_FLIP_H), 0);
    assert_int_equal(load_compare_image("samples/square_flip_h.bmp", img), 0);

    uimg_release(img);
}
#endif

#ifdef UIMG_ENABLE_BLITMOD
static
void test_rotate(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_rotate(img, UIMG_ROTATE_0), 0);
    assert_int_equal(load_compare_image("samples/square_rotate_0.bmp", img), 0);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_rotate(img, UIMG_ROTATE_90), 0);
    assert_int_equal(load_compare_image("samples/square_rotate_90.bmp", img), 0);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_rotate(img, UIMG_ROTATE_180), 0);
    assert_int_equal(load_compare_image("samples/square_rotate_180.bmp", img), 0);

    assert_int_equal(uimgbmp_load_file(img, "samples/square_colour24.bmp", 0), 0);
    assert_int_equal(uimgblit_rotate(img, UIMG_ROTATE_270), 0);
    assert_int_equal(load_compare_image("samples/square_rotate_270.bmp", img), 0);

    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG)
static
void test_stretch_nearby(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgbmp_load_file(img, "samples/lenna.bmp", 0), 0);
    assert_int_equal(uimgblit_stretch(
        img, uimg_get_width(img) * 3, uimg_get_height(img) * 3, UIMG_SCALE_NEARBY),
        0);
    assert_in_range(load_compare_image("samples/lenna_enlarge_nearby.png", img), 0, 8);

    assert_int_equal(uimgbmp_load_file(img, "samples/lenna.bmp", 0), 0);
    assert_int_equal(uimgblit_stretch(
        img, uimg_get_width(img) / 4, uimg_get_height(img) / 4, UIMG_SCALE_NEARBY),
        0);
    assert_in_range(load_compare_image("samples/lenna_shrink_nearby.png", img), 0, 8);

    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG) && !defined(UIMG_USE_SDL)
static
void test_stretch_linear(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    assert_int_equal(uimgbmp_load_file(img, "samples/lenna.bmp", 0), 0);
    assert_int_equal(uimgblit_stretch(
        img, uimg_get_width(img) * 3, uimg_get_height(img) * 3, UIMG_SCALE_LINEAR),
        0);
    assert_in_range(load_compare_image("samples/lenna_enlarge_linear.png", img), 0, 4);

    assert_int_equal(uimgbmp_load_file(img, "samples/lenna.bmp", 0), 0);
    assert_int_equal(uimgblit_stretch(
        img, uimg_get_width(img) / 4, uimg_get_height(img) / 4, UIMG_SCALE_LINEAR),
        0);
    assert_in_range(load_compare_image("samples/lenna_shrink_linear.png", img), 0, 4);

    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG)
static
void test_blend_overlay(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimg_t *cover = uimg_create(0, 0);
    assert_non_null(cover);

    assert_int_equal(uimgpng_load_file(cover, "samples/daemon.png"), 0);
    struct uimg_rect cover_rect = { .x = 82, .y = 8, .w = 268, .h = 228 };

    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);

    int x = ( uimg_get_width(img) - cover_rect.w ) / 2;
    int y = ( uimg_get_height(img) - cover_rect.h ) / 2;
    assert_int_equal(uimgblit_blend(img, x, y, cover, &cover_rect, UIMG_BLEND_OVERLAY), 0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_overlay.png", img), 0);

    uimg_release(cover);
    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG)
static
void test_blend_alpha(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimg_t *cover = uimg_create(0, 0);
    assert_non_null(cover);

    assert_int_equal(uimgpng_load_file(cover, "samples/daemon.png"), 0);
    struct uimg_rect cover_rect = { .x = 82, .y = 8, .w = 268, .h = 228 };

    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);

    int x = ( uimg_get_width(img) - cover_rect.w ) / 2;
    int y = ( uimg_get_height(img) - cover_rect.h ) / 2;
    assert_int_equal(uimgblit_blend(img, x, y, cover, &cover_rect, UIMG_BLEND_ALPHA), 0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_alpha.png", img), 0);

    uimg_release(cover);
    uimg_release(img);
}
#endif

#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG)
static
void test_blend_area(void **state)
{
    uimg_t *img = uimg_create(0, 0);
    assert_non_null(img);

    uimg_t *cover = uimg_create(0, 0);
    assert_non_null(cover);

    assert_int_equal(uimgpng_load_file(cover, "samples/daemon.png"), 0);
    struct uimg_rect cover_rect = { .x = 82, .y = 8, .w = 268, .h = 228 };

    int dst_x = - cover_rect.w / 2;
    int dst_y = - cover_rect.h / 2;
    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);
    assert_int_equal(uimgblit_blend(
        img, dst_x, dst_y, cover, &cover_rect, UIMG_BLEND_ALPHA),
        0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_topleft.png", img), 0);

    dst_x = uimg_get_width(img) - cover_rect.w / 2;
    dst_y = - cover_rect.h / 2;
    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);
    assert_int_equal(uimgblit_blend(
        img, dst_x, dst_y, cover, &cover_rect, UIMG_BLEND_ALPHA),
        0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_topright.png", img), 0);

    dst_x = - cover_rect.w / 2;
    dst_y = uimg_get_height(img) - cover_rect.h / 2;
    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);
    assert_int_equal(uimgblit_blend(
        img, dst_x, dst_y, cover, &cover_rect, UIMG_BLEND_ALPHA),
        0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_bottomleft.png", img), 0);

    dst_x = uimg_get_width(img) - cover_rect.w / 2;
    dst_y = uimg_get_height(img) - cover_rect.h / 2;
    assert_int_equal(uimgpng_load_file(img, "samples/tux.png"), 0);
    assert_int_equal(uimgblit_blend(
        img, dst_x, dst_y, cover, &cover_rect, UIMG_BLEND_ALPHA),
        0);
    assert_int_equal(load_compare_image("samples/tux_daemon_blend_bottomright.png", img), 0);

    uimg_release(cover);
    uimg_release(img);
}
#endif

int main(int argc, char *argv[])
{
#if defined(UIMG_ENABLE_GIF) && defined(UIMG_ENABLE_PNG)
    uimggif_pltgen_t *gif_pltgen = NULL;
#endif

    struct CMUnitTest tests[] =
    {
#ifdef UIMG_USE_SDL
        cmocka_unit_test(init_sdl),
#endif
#ifdef UIMG_ENABLE_BMP
        cmocka_unit_test(test_bmp_rw),
#endif
#ifdef UIMG_ENABLE_JPEG
        cmocka_unit_test(test_jpeg_rw),
#endif
#ifdef UIMG_ENABLE_PNG
        cmocka_unit_test(test_png_rw),
#endif
#ifdef UIMG_ENABLE_GIF
        cmocka_unit_test(test_gif_write),
        cmocka_unit_test(test_gif_read),
#   ifdef UIMG_ENABLE_PNG
        cmocka_unit_test_prestate(test_gif_create_custom_palette, &gif_pltgen),
        cmocka_unit_test_prestate(test_gif_write_custom_palette, &gif_pltgen),
        cmocka_unit_test_prestate(test_gif_read_custom_palette, &gif_pltgen),
        cmocka_unit_test_prestate(test_gif_release_custom_palette, &gif_pltgen),
#   endif
#endif
#ifdef UIMG_ENABLE_WEBP
        cmocka_unit_test(test_webp_rw),
#endif
#ifdef UIMG_ENABLE_HEIF
        cmocka_unit_test(test_heif_rw),
#endif
#ifdef UIMG_ENABLE_BLITMOD
        cmocka_unit_test(test_fill),
        cmocka_unit_test(test_flip),
        cmocka_unit_test(test_rotate),
#endif
#if defined(UIMG_ENABLE_BLITMOD) && defined(UIMG_ENABLE_PNG)
        cmocka_unit_test(test_stretch_nearby),
#   ifndef UIMG_USE_SDL // SDL_Surface does not support the linear scale method
        cmocka_unit_test(test_stretch_linear),
#   endif
        cmocka_unit_test(test_blend_overlay),
        cmocka_unit_test(test_blend_alpha),
        cmocka_unit_test(test_blend_area),
#endif
#ifdef UIMG_USE_SDL
        cmocka_unit_test(quit_sdl),
#endif
    };

    return cmocka_run_group_tests_name("UIMG test", tests, NULL, NULL);
}
